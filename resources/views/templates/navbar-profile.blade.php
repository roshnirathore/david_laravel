<li class="nav-item dropdown">
    <a class="nav-link dropdown-toggle text-muted waves-effect waves-dark"
       href="" data-toggle="dropdown"
       aria-haspopup="true"
       aria-expanded="false">
        <!-- <img src="{{url('/')}}/uploads/{{ Auth::user()->profile_image }}" alt="user" class="profile-pic"/> -->
        <img src="/vendor/wrappixel/material-pro/4.2.1/assets/images/users/1.jpg" alt="user" class="profile-pic"/>
    </a>
    <div class="dropdown-menu dropdown-menu-right scale-up">
        <ul class="dropdown-user">
            <li>
                <div class="dw-user-box">
                    <div class="u-img">
                        {{--Replace with User image here--}}
                        <!-- <img src="/vendor/wrappixel/material-pro/4.2.1/assets/images/users/1.jpg" alt="user" class="profile-pic"/> -->
                        <img src="{{url('/')}}/uploads/{{ Auth::user()->profile_image }} "alt="user" class="profile-pic"/>
                    </div>
                    <div class="u-text">
                        <h4>{{ Auth::user()->name }}</h4>
                    </div>
                </div>
            </li>
            <li role="separator" class="divider"></li>
            <li><a href="{{ route('profile.index') }}"><i class="ti-user"></i> My Profile</a></li>
            <li><a href="{{ route('profile.change_password') }}"><i class="ti-user"></i> Reset Password</a></li>
            <li><a href="/logout"><i class="fa fa-power-off"></i> Logout</a></li>
        </ul>
    </div>
</li>
