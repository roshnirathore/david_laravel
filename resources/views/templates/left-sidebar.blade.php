<!-- ============================================================== -->
<!-- Left Sidebar - style you can find in sidebar.scss  -->
<!-- ============================================================== -->
<aside class="left-sidebar">
    <!-- Sidebar scroll-->
    <div class="scroll-sidebar">
        <!-- User profile -->
        <!-- @ include('templates.sidebar-profile') -->
        <!-- End User profile text-->
        <!-- Sidebar navigation-->
        <nav class="sidebar-nav">
            <ul id="sidebarnav">

                <li>
                    <a class="waves-effect waves-dark" href="/">
                        <i class="mdi mdi-view-dashboard"></i>
                          <span class="hide-menu">Dashboard</span>
                    </a>
                </li>
                 <li>
                    <a class="" href="{{route('client.index')}}" aria-expanded="false">
                        <i class="mdi mdi-account-multiple"></i>
                        <span class="hide-menu">Clients</span>
                    </a>
                </li>
                <li>
                    <a class="" href="{{route('store.index')}}" aria-expanded="false">
                        <i class="mdi mdi-map-marker"></i>
                        <span class="hide-menu">Locations</span>
                    </a>
                </li>
                <li>
                    <a class="" href="{{route('storeuser.index')}}" aria-expanded="false">
                        <i class="mdi mdi-store"></i>
                        <span class="hide-menu">Store Users</span>
                    </a>
                </li>
                
                <li>
                    <a class="" href="#" aria-expanded="false">
                        <i class="mdi mdi-file-document"></i>
                        <span class="hide-menu">Reports</span>
                    </a>
                </li>

            </ul>
        </nav>
        <!-- End Sidebar navigation -->
    </div>
    <!-- End Sidebar scroll-->
    <!-- Bottom points-->
    {{--<div class="sidebar-footer">
        <!-- item--><a href="" class="link" data-toggle="tooltip" title="Settings"><i class="ti-settings"></i></a>
        <!-- item--><a href="" class="link" data-toggle="tooltip" title="Email"><i class="mdi mdi-gmail"></i></a>
        <!-- item--><a href="" class="link" data-toggle="tooltip" title="Logout"><i class="mdi mdi-power"></i></a>
    </div>
    --}}
    <!-- End Bottom points-->
</aside>
<!-- ============================================================== -->
<!-- End Left Sidebar - style you can find in sidebar.scss  -->
<!-- ============================================================== -->