@extends('layouts.main') 
@section('body-classes','')
@section('template-css')
   <link rel="stylesheet" href="https://use.typekit.net/jhw8gfh.css">
   <link href="{{ mix('/css/material/style.css') }}" rel="stylesheet">
   <link href="{{ mix('/css/colors/blue.css') }}" id="theme" rel="stylesheet">
   <link href="{{ mix('/css/custom.css') }}" rel="stylesheet">
@endsection

@section('template-custom-js')
    <script src="/vendor/wrappixel/material-pro/4.2.1/material/js/custom.min.js"></script>

    <!-- multisecet dropdown -->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-3-typeahead/4.0.2/bootstrap3-typeahead.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/0.9.13/js/bootstrap-multiselect.js"></script>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/0.9.13/css/bootstrap-multiselect.css" />
<script src="https://canvasjs.com/assets/script/canvasjs.min.js"></script>
@endsection

@section('layout-content')

    @include('templates.topbar')

    <!-- @ include('templates.left-sidebar') --> 
    <!-- Get Feedback report data -->
 @php 
    $data = App\Http\Controllers\UtilityController::index();
@endphp



<script type="text/javascript">
var pie_data = <?php echo json_encode($data);?>;
var today_report_data = pie_data.today_report_data;
var days7_report_data = pie_data.days7_report_data;
var days30_report_data = pie_data.days30_report_data;
var days365_report_data = pie_data.days365_report_data;
</script>
<script type="text/javascript">

</script>
    <div class="page-wrapper">
 
        <div class="container-fluid test">
            <div class="row">
                <div class="col-sm-12">
                    <div class="progress-row mt-15">
                        <div class="logo"> <a href="{{route ('client.home')}}">
                            <img src="/images/focus-track-logo.png"
                             alt="homepage"
                             class="dark-logo"/> </a>
                        </div>
                        <div class="current-date">  
                            @if (Auth::user()->roles[0]->slug == 'admin')
                                <p>CURRENT AS OF : {{ (new \App\Helpers\FeedbackHelper)->admin() }}</p> 
                            @elseif (Auth::user()->roles[0]->slug == 'client')
                                <p>CURRENT AS OF : {{ (new \App\Helpers\FeedbackHelper)->user() }}</p> 
                            @endif

                        </div> 
                        <div class="chart-tracking">
                            <div class = "remove_text" id="chartContainerToday" style="height: 100px; width: auto;"></div>
                            <p>TODAY</p>
                        </div>

                        <div class="chart-tracking">
                            <div class = "remove_text" id="chartContainer7Days" style="height: 100px; width: auto;"></div>
                            <p>7 DAYS</p>
                        </div>
                        <div class="chart-tracking">
                            
                             <div class = "remove_text" id="chartContainer30Days" style="height: 100px; width: auto;"></div>
                             <p>30 DAYS</p>
                        </div>
                        <div class="chart-tracking">
                             <div class = "remove_text" id="chartContainer365Days" style="height: 100px; width: auto;"></div>
                             <p>365 DAYS</p>
                            <script type="text/javascript">
                                //Current day's data
                                if (today_report_data.length > 0 ) {
                                    var todaylike = 0;
                                   var todayunlike = 0;
                                    for (var i = today_report_data.length - 1; i >= 0; i--) {
                                        var fbValue = today_report_data[i]['name'];
                                        if(fbValue == 1){
                                          todaylike++
                                        }else{
                                          todayunlike++
                                        }  
                                    }
                                  //  console.log('today=>'+like+' '+unlike+);
                                }else{
                                    todaylike = 100;
                                    todayunlike = 0;
                                }
                                
                                //previous 7 day's data
                                if (days7_report_data.length > 0 ) {
                                    var days7like = 0;
                                   var days7unlike = 0;
                                    for (var i = days7_report_data.length - 1; i >= 0; i--) {
                                        var fbValue = days7_report_data[i]['name'];
                                        if(fbValue == 1){
                                          days7like++
                                        }else{
                                          days7unlike++
                                        }  
                                    }
                                    //console.log('7day=>'+like+' '+unlike+);
                                }else{
                                    days7like = 100;
                                    days7unlike = 0;
                                }

                                //previous 1 month data
                                 if (days30_report_data.length > 0 ) {
                                    var days30like = 0;
                                   var days30unlike = 0;
                                    for (var i = days30_report_data.length - 1; i >= 0; i--) {
                                        var fbValue = days30_report_data[i]['name'];
                                        if(fbValue == 1){
                                          days30like++
                                        }else{
                                          days30unlike++
                                        }  
                                    }
                                }else{
                                    days30like = 100;
                                    days30unlike = 0;
                                }

                                //prevoius 1 year's data
                                if (days365_report_data.length > 0 ) {
                                    var days365like = 0;
                                   var days365unlike = 0;
                                    for (var i = days365_report_data.length - 1; i >= 0; i--) {
                                        var fbValue = days365_report_data[i]['name'];
                                        if(fbValue == 1){
                                          days365like++
                                        }else{
                                          days365unlike++
                                        }  
                                    }
                                }else{
                                    days365like = 100;
                                    days365unlike = 0;
                                }


                        window.onload = function () {
                                 
                                 //Current day's chart
                                 var likePertoday =parseInt((todaylike/(todayunlike+todaylike)*100));
                                var chartToday = new CanvasJS.Chart("chartContainerToday", {
                                    
                                    legend:{
                                        cursor: "pointer"
                                    },
                                     subtitles: [{
                                        text: likePertoday+"%",
                                        verticalAlign: "center",
                                        fontSize: 18,
                                        fontColor:"#8dc642",
                                        dockInsidePlotArea: true
                                    }],

                                    data: [{
                                        type: "doughnut",
                                        radius:  "90%",
                                        indexLabelPlacement: "inside",
                                        //indexLabel: "{name} #percent%",
                                        dataPoints: [
                                            { y: todayunlike, name: "", color: "red" },
                                            { y: todaylike, name: "", color: "#8dc642" }
                                        ]
                                    }]
                                });
                                chartToday.render();

                                //previous 7 day's chart
                                var likePer7 = parseInt((days7like/(days7unlike+days7like)*100));
                                var chart7Days = new CanvasJS.Chart("chartContainer7Days", {
                                    
                                    legend:{
                                        cursor: "pointer"
                                    },
                                    subtitles: [{
                                        text: likePer7+"%",
                                        verticalAlign: "center",
                                        fontSize: 18,
                                        fontColor:"#8dc642",
                                        dockInsidePlotArea: true
                                    }],
                                    data: [{
                                        type: "doughnut",
                                        radius:  "90%",
                                        indexLabelPlacement: "inside",
                                        
                                        dataPoints: [
                                            { y: days7unlike, name: "", color: "red" },
                                            { y: days7like, name: "", color: "#8dc642" }
                                        ]
                                    }]
                                });
                                chart7Days.render();

                                //prevoius 1 month's chart
                                 var likePer30 =parseInt((days30like/(days30unlike+days30like)*100));
                                var chart30Days = new CanvasJS.Chart("chartContainer30Days", {
                                    
                                    legend:{
                                        cursor: "pointer"
                                    },
                                    subtitles: [{
                                        text: likePer30+"%",
                                        verticalAlign: "center",
                                        fontSize: 18,
                                        fontColor:"#8dc642",
                                        dockInsidePlotArea: true
                                    }],

                                    data: [{
                                        type: "doughnut",
                                        radius:  "90%",
                                        indexLabelPlacement: "inside",
                                        //toolTipContent: "<b>{name}</b>: (#percent%)",
                                       // indexLabel: "{name} #percent%",
                                        dataPoints: [
                                            { y: days30unlike, name: "", color: "red" },
                                            { y: days30like, name: "", color: "#8dc642" }
                                        ]
                                    }]
                                });
                                chart30Days.render();

                                
                                //prevoius 1 year's chart
                                var likePer365 =parseInt((days365like/(days365unlike+days365like)*100));
                                var chart365Days = new CanvasJS.Chart("chartContainer365Days", {
                                    
                                    legend:{
                                        cursor: "pointer"
                                    },
                                    subtitles: [{
                                        text: likePer365+"%",
                                        verticalAlign: "center",
                                        fontSize: 18,
                                        fontColor:"#8dc642",
                                        dockInsidePlotArea: true
                                    }],
                                    data: [{
                                        type: "doughnut",
                                        radius:  "90%",
                                        indexLabelPlacement: "inside",
                                        //toolTipContent: "<b>{name}</b>: (#percent%)",
                                        //indexLabel: "{name} #percent%",
                                        dataPoints: [
                                            { y: days365unlike, name: "", color: "red" },
                                            { y: days365like, name: "", color: "#8dc642" }
                                        ]
                                    }]
                                });
                                chart365Days.render();

                            }
                            </script>
                        </div>
                        <div class="thumbs">
                            <div class="thumbs-up-green">
                                <img src="/images/thumbs-up-small.png"
                             alt="Thumbs Up">
                            </div>
                            <div class="thumbs-down-red">
                                <img src="/images/thumbs-down-small.png"
                             alt="Thumbs Down">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
             @include('common.errors')
            @include('common.success')
            @yield('content')
        </div>
    </div>
    <style type="text/css">
        /*.page-wrapper{
            margin-left: 0px;
            background-color: #fff;
        }
        .progress-row {
            display: flex;
            width: 60%;
        }

        .progress-row .logo {
            width: 200px;
            max-width: 100%;
        }
        .progress-row .current-date, .progress-row .chart-tracking, .progress-row .thumbs {
            flex: 1;
            font-size: 22px;
            text-align: center;
            color: #57585a;
        }*/
        .remove_text {
            position: relative;
        }

        .remove_text:after {
            content: "";
            display: block;
            position: absolute;
            z-index: 1;
            width: 100%;
            height: 12px;
            background: #fff;
            bottom: 0px;
        }
    </style>
    <footer>
    <div class="footer-section">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12">
                    <p>© Digital Biz Farm LLC. All rights reserved.</p>
                </div>
            </div>
        </div>
    </div>
</footer>
@endsection



