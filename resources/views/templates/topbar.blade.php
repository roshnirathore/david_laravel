<!-- ============================================================== -->
<!-- Topbar header - style you can find in pages.scss -->
<!-- ============================================================== -->
<header class="topbar">
    <nav class="navbar top-navbar navbar-expand-md navbar-light">
        <!-- ============================================================== -->
        <!-- Logo -->
        <!-- ============================================================== -->
        <div class="top-header">      
        </div>
        <div class="header-main navbar-nav">
            <ul class="dash-nav"> 
                <li class="nav-item"><a href="{{route ('profile.index')}}"><i class="mdi mdi-account"></i>{{ Auth::user()->first_name }}</a></li>
                <li class="nav-item dropdown">
                    <a href="#">ACCOUNT</a>
                    <!-- <div class="dropdown-content">
                        <ul>
                            <li>TEST</li>
                            <li>TEST1</li>
                            <li>TEST2</li>
                            <li>TEST3</li>
                        </ul>
                    </div> -->
                    <div class="dropdown-menu">
                        <ul class="dropdown-user">
                            <li>
                             @if (Auth::user()->roles[0]->slug == 'admin')
                                <a href="{{ route('client.index') }}"><i class="ti-user"></i>Clients</a> 
                            @elseif (Auth::user()->roles[0]->slug == 'client')
                                <a href="{{ route('client_store.index') }}"><i class="fa fa-university"></i>Stores</a>
                            @else
                                No Users :(
                            @endif
                            </li>
                            <li class="sub-dropdown ">
                                <a href="#"><i class="fa fa-info-circle"></i>Account Details</a>
                                <div class="sub-dropdown-menu">
                                    <ul>
                                        <li>
                                            <a href="{{ route('profile.index') }}"><i class="fa fa-user"></i>Profile</a>
                                        </li>
                                        <li>
                                            <a href="{{ route('password.index') }}"><i class="fa fa-key"></i>Change Password</a>
                                        </li>
                                    </ul>
                                </div>
                            </li>
                            <li>
                                @if (Auth::user()->roles[0]->slug == 'admin')
                                    <a href="{{ route('app.index') }}"><i class="fa fa-futbol"></i>App</a>
                                @endif
                            </li>
                        </ul>
                    </div>
                </li>
                <li class="nav-item"><a href="{{ route('support.index') }}">SUPPORT</a></li>
                <li class="nav-item"><a href="/logout"><i class="fa fa-power-off"></i>SIGN OUT</a></li>
            </ul>
        </div>
    </nav> 
</header>
<style type="text/css">
    .sub-dropdown {
        position: relative;
    }
    .header-main ul.dash-nav li.nav-item.dropdown .dropdown-menu ul.dropdown-user {
        width: 160px;
    }
    .sub-dropdown-menu {
        position: absolute;
        top: 8px;
        right: -191px;
        background: #015aaa;
        z-index: 9;
        width: 190px;
        border-radius: 3px;
        transition: 0.4s ease-in-out;
        visibility: hidden;
    }
    .sub-dropdown:hover .sub-dropdown-menu {
        visibility: visible;
    }
    .sub-dropdown-menu ul {
        padding: 0px;
    }
</style>
<!-- ============================================================== -->
<!-- End Topbar header -->
<!-- ============================================================== -->
