<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- Favicon icon -->
    <link rel="icon" type="image/png" sizes="16x16" href="/images/focus-track-logo-icon.png">
    {{--<link rel="icon" type="image/png" sizes="16x16" href="/images/focus-track-logo-icon.png">--}}
    <title>{{ config('app.name', 'Laravel') }}</title>


    @stack('before-styles')

    <!-- Bootstrap Core CSS -->
    <link href="/vendor/wrappixel/material-pro/4.2.1/assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="/vendor/wrappixel/material-pro/4.2.1/assets/plugins/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css" rel="stylesheet">
    <!-- Page plugins css -->
    <link href="/vendor/wrappixel/material-pro/4.2.1/assets/plugins/clockpicker/dist/jquery-clockpicker.min.css" rel="stylesheet">
    <!-- Color picker plugins css -->
    <link href="/vendor/wrappixel/material-pro/4.2.1/assets/plugins/jquery-asColorPicker-master/css/asColorPicker.css" rel="stylesheet">
    <!-- Date picker plugins css -->
    <link href="/vendor/wrappixel/material-pro/4.2.1/assets/plugins/bootstrap-datepicker/bootstrap-datepicker.min.css" rel="stylesheet" type="text/css" />
    <!-- Daterange picker plugins css -->
    <link href="/vendor/wrappixel/material-pro/4.2.1/assets/plugins/timepicker/bootstrap-timepicker.min.css" rel="stylesheet">
    <link href="/vendor/wrappixel/material-pro/4.2.1/assets/plugins/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">
    <!-- Template CSS -->

    @section('template-css')
    {{--Defaults to Material and Blue--}}

    {{-- ### Choose only the one you want ### --}} 
    <link href="/css/material/style.css" rel="stylesheet">
    {{--<link href="/css/dark/style.css" rel="stylesheet">--}}
    {{--<link href="/css/minisidebar/style.css" rel="stylesheet">--}}
    {{--<link href="/css/horizontal/style.css" rel="stylesheet">--}}
    {{--<link href="/css/material-rtl/style.css" rel="stylesheet">--}}

    <!-- You can change the theme colors from here -->
    <link href="/css/colors/blue.css" id="theme" rel="stylesheet">
    @show

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="/vendor/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="/vendor/respondjs/1.4.2/respond.min.js"></script>
    <![endif]-->

    @stack('after-styles')

</head>

<body class=" @yield('body-classes') card-no-border ">
<!-- ============================================================== -->
<!-- Preloader - style you can find in spinners.css -->
<!-- ============================================================== -->
<div class="preloader">
    <svg class="circular" viewBox="25 25 50 50">
        <circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" /> </svg>
</div>

<div id="main-wrapper">
@yield('layout-content')
</div>

@stack('before-scripts')

<!-- ============================================================== -->
<!-- All Jquery -->
<!-- ============================================================== -->
@section('jquery')
{{--    If not using jQuery from NPM and webpack build, don't override this section,    --}}
{{--    or user @parent inside when you do it, to include this jquery script            --}}


<script src="/vendor/wrappixel/material-pro/4.2.1/assets/plugins/jquery/jquery.min.js"></script>
<script src="/vendor/wrappixel/material-pro/4.2.1/assets/plugins/quickpaginate/jquery.quickpaginate.packed.js"></script>
@show
<!-- Bootstrap tether Core JavaScript -->
<script src="/vendor/wrappixel/material-pro/4.2.1/assets/plugins/popper/popper.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.2/sweetalert.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="/vendor/wrappixel/material-pro/4.2.1/assets/plugins/bootstrap/js/bootstrap.min.js"></script>
<!-- slimscrollbar scrollbar JavaScript -->
<script src="/vendor/wrappixel/material-pro/4.2.1/material/js/jquery.slimscroll.js"></script>
<!--Wave Effects -->
<script src="/vendor/wrappixel/material-pro/4.2.1/material/js/waves.js"></script>
<!--Menu sidebar -->
<script src="/vendor/wrappixel/material-pro/4.2.1/material/js/sidebarmenu.js"></script>
<!--stickey kit -->
<script src="/vendor/wrappixel/material-pro/4.2.1/assets/plugins/sticky-kit-master/dist/sticky-kit.min.js"></script>
<script src="/vendor/wrappixel/material-pro/4.2.1/assets/plugins/sparkline/jquery.sparkline.min.js"></script>
<!--Custom JavaScript -->
@section('template-custom-js')
    {{--Defaults to Material --}}

    {{-- ### Choose only the one you want ### --}}
    <script src="/vendor/wrappixel/material-pro/4.2.1/material/js/custom.min.js"></script>
    {{--<script src="/vendor/wrappixel/material-pro/4.2.1/dark/js/custom.min.js"></script>--}}
    {{--<script src="/vendor/wrappixel/material-pro/4.2.1/minisidebar/js/custom.min.js"></script>--}}
    {{--<script src="/vendor/wrappixel/material-pro/4.2.1/horizontal/js/custom.min.js"></script>--}}
    {{--<script src="/vendor/wrappixel/material-pro/4.2.1/material-rtl/js/custom.min.js"></script>--}}
@show
<!-- ============================================================== -->
    <script src="/vendor/wrappixel/material-pro/4.2.1/assets/plugins/moment/moment.js"></script>
    <script src="/vendor/wrappixel/material-pro/4.2.1/assets/plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js"></script>
    <!-- Clock Plugin JavaScript -->
    <script src="/vendor/wrappixel/material-pro/4.2.1/assets/plugins/clockpicker/dist/jquery-clockpicker.min.js"></script>
    <!-- Color Picker Plugin JavaScript -->
    <script src="/vendor/wrappixel/material-pro/4.2.1/assets/plugins/jquery-asColorPicker-master/libs/jquery-asColor.js"></script>
    <script src="/vendor/wrappixel/material-pro/4.2.1/assets/plugins/jquery-asColorPicker-master/dist/jquery-asColorPicker.min.js"></script>
    <!-- Date Picker Plugin JavaScript -->
    <script src="/vendor/wrappixel/material-pro/4.2.1/assets/plugins/bootstrap-datepicker/bootstrap-datepicker.min.js"></script>
    <!-- Date range Plugin JavaScript -->
    <script src="/vendor/wrappixel/material-pro/4.2.1/assets/plugins/timepicker/bootstrap-timepicker.min.js"></script>
    <script src="/vendor/wrappixel/material-pro/4.2.1/assets/plugins/bootstrap-daterangepicker/daterangepicker.js"></script>
 
    <script>
       // MAterial Date picker    
        $('#from_date').bootstrapMaterialDatePicker({ format : 'MM-DD-YYYY',weekStart : 0, time: false });
        $('#to_date').bootstrapMaterialDatePicker({ format : 'MM-DD-YYYY',weekStart : 0, time: false });
        $('#from_time').bootstrapMaterialDatePicker({ format : 'HH:mm', time: true, date: false });
        $('#to_time').bootstrapMaterialDatePicker({ format : 'HH:mm', time: true, date: false });
                          
        $('.clockpicker').clockpicker({
            donetext: 'Done',
        }).find('input').change(function() {
             console.log(this.value);
            });

        /*if (/mobile/i.test(navigator.userAgent)) {
            $('input').prop('readOnly', true);
        }*/
                       
        $('.input-daterange-timepicker').daterangepicker({
            timePicker: true,
            format: 'MM/DD/YYYY h:mm A',
            timePickerIncrement: 30,
            timePicker12Hour: true,
            timePickerSeconds: false,
            buttonClasses: ['btn', 'btn-sm'],
            applyClass: 'btn-danger',
            cancelClass: 'btn-inverse'
        }); 
                    
    </script>

<!-- Style switcher -->
<!-- ============================================================== -->
<script src="/vendor/wrappixel/material-pro/4.2.1/assets/plugins/styleswitcher/jQuery.style.switcher.js"></script>


@stack('after-scripts')

{{--ATTENTION:--}}
{{----}}
{{----}}
{{----}}

{{--This code is only for the live running demo, without the proper config key, it **DOES NOT** track anything--}}

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id={{ config('app.google_analytics') }}"></script>
<script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', '{{ config('app.google_analytics') }}');
</script>

{{----}}
{{----}}
{{----}}


<script src="/vendor/wrappixel/material-pro/4.2.1/assets/plugins/quickpaginate/jquery.quickpaginate.packed.js"></script>

</body>

</html>
