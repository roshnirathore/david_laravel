@extends('layouts.main')

@section('template-css')
   <link rel="stylesheet" href="https://use.typekit.net/jhw8gfh.css">
   <link href="{{ mix('/css/material/style.css') }}" rel="stylesheet">
   <link href="{{ mix('/css/colors/blue.css') }}" id="theme" rel="stylesheet">
   <link href="{{ mix('/css/custom.css') }}" rel="stylesheet">
@endsection

@section('layout-content')  
<section id="wrapper">
    <div class="login-register">
        <div class="top-header">
            
        </div>
        <div class="logo">
            <img src="/images/focus-track-logo.png"
             alt="homepage"
             class="dark-logo"/>
        </div>
        <div class="login-box card">

                <div class="card-body">
                    <div class="reset-form">
                        <h3 class="box-title m-b-20">Reset Password</h3>
                        <form method="POST" action="http://dashboard.focustrax.com/password/reset" class="form-horizontal form-material">
                        @csrf

                        <input type="hidden" name="token" value="{{ $token }}">

                        <div class="form-group">

                            <div class="col-xs-12">
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ $email ?? old('email') }}" required autocomplete="email" placeholder="Email" autofocus>

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group">
                           
                            <div class="col-xs-12">
                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password" placeholder="Password">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group">

                            <div class="col-xs-12">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password" placeholder="Confirm Password">
                            </div>
                        </div>

                        <div class="form-group text-center m-t-20">
                            <div class="col-xs-12">
                                <button class="btn btn-info btn-lg btn-block text-uppercase waves-effect waves-light" type="submit"> {{ __('Reset Password') }}</button>
                            </div>
                       </div> 
                    </form>
                    </div>
                </div>
         </div>
    </div>
</section>
            
@endsection
