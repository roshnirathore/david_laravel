@extends('templates.main',['pageTitle'=>'Clients','rootPage'=>''])


@section('content')
<div class="row">
     <!-- column -->
    <div class="col-12">
        <div class="card">
            <div class="card-body">
				<div class="container">
					<div class="accordion-container faq-section">
					  <h2>Frequently Asked Questions</h2>
					  <div class="set">
						    <a href="#">
						      Lorem Ipsum 1
						      <i class="fa fa-plus"></i>
						    </a>
						    <div class="content">
						      <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</p>
						    </div>
					  </div>
					  <div class="set">
						    <a href="#">
						      Lorem Ipsum 2
						      <i class="fa fa-plus"></i>
						    </a>
						    <div class="content">
						      <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</p>
						    </div>
					  </div>
					  <div class="set">
						    <a href="#">
						      Lorem Ipsum 3
						      <i class="fa fa-plus"></i>
						    </a>
						    <div class="content">
						      <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</p>
						    </div>
					  </div>
					  <div class="set">
						    <a href="#">
						      Lorem Ipsum 4
						      <i class="fa fa-plus"></i> 
						    </a>
						    <div class="content">
						      <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</p>
						    </div>
					  </div>
					</div>
				</div>
            </div>
            <div class="return_page">
                <p>Go To:</p>
                 <a href="{{route ('client.home')}}">Home</a>
            </div>
        </div>
    </div>
</div>
@endsection
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script type="text/javascript">
	$(document).ready(function() {
	  $(".set > a").on("click", function() {
	    if ($(this).hasClass("active")) {
	      $(this).removeClass("active");
	      $(this)
	        .siblings(".content")
	        .slideUp(200);
	      $(".set > a i")
	        .removeClass("fa-minus")
	        .addClass("fa-plus");
	    } else {
	      $(".set > a i")
	        .removeClass("fa-minus")
	        .addClass("fa-plus");
	      $(this)
	        .find("i")
	        .removeClass("fa-plus")
	        .addClass("fa-minus");
	      $(".set > a").removeClass("active");
	      $(this).addClass("active");
	      $(".content").slideUp(200);
	      $(this)
	        .siblings(".content")
	        .slideDown(200);
	    }
	  });
	});
</script>
