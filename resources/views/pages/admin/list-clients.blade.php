@extends('templates.main',['pageTitle'=>'Clients','rootPage'=>''])
@section('content')
<style type="text/css">
    #search-input {
  width: 100%;
  font-size: 16px;
  padding: 12px 30px;
  border: 1px solid #ccc;
  margin-bottom: 15px;
}
</style>
<div class="row">

     <!-- column -->
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-10">
                        <h4 class="card-title">List of Clients</h4>
                        <h6 class="card-subtitle">
                            @if (count($clients) === 1)
                                <code>{{ count($clients) }}</code> User total
                            @elseif (count($clients) > 1)
                                <code>{{ count($clients) }}</code> Total Users
                            @else
                                No Users :(
                            @endif</h6>                       
                    </div>
                    <div class="col-2">
                        <a href="{{ route('client.create') }}" class="btn btn-info btn-rounded" color="info"> Create </a>
                    </div>
                    
                </div>
                
                <div class="table-responsive">
                    <input type="text" id="search" name="search" placeholder="Search client" title="Search" style="margin-bottom: 20px; text-align: center;">                 
                    <table class="table color-bordered-table info-bordered-table" id="client_list">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Company Name </th>
                            <th># Stores</th>
                            <th># Install</th>
                            <th>Status</th>
                            <th>Created</th>
                            <th>Updated</th>
                            <th class="text-nowrap">Action</th>
                        </tr>
                        </thead>
                        <tbody> 

                             @foreach ($clients as $client)
                             <tr id="<?php echo $client->id ?>">
                                <td>{{$loop->iteration}}</td>
                                <td>{{ ucfirst(trans($client->first_name)) }} {{$client->last_name}}</td>
                                <td>{{$client->email}}</td>
                                <td>{{$client->company_name}}</td>
                                <td><a href="{{ route('store.index',[$client->id]) }}">{{count($client->stores)}}</a> </td>
                                
                                <td>{{count($client->installs)}}</td>
                                <td>{{$client->activated == 1 ? 'Active' : 'Suspended'}}
                                </td>
                                <td>{{$client->created_at->format('m-d-Y H:i')}}</td>
                                <td>{{$client->updated_at->format('m-d-Y H:i')}}</td>

                                <td class="text-nowrap">
                                    <a href="{{ route('client.edit',[$client->id]) }}" data-toggle="tooltip" data-original-title="Edit" class="btn btn-secondary btn-circle"> <i class="fa fa-pencil-alt"></i> </a>
                                    <a href="{{ route('client.delete',[$client->id]) }}" data-toggle="tooltip" data-original-title="Delete" class="btn btn-danger btn-circle" onclick="comfrimDelet(event,this)">  <i class="fa fa-times"></i> </a>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                     {{ $clients->links() }}
                </div>
            </div>
            <div class="return_page">
                <p>Go To:</p>
                 <a href="{{route ('client.home')}}">Home</a>
            </div>
        </div>
    </div>
</div>
@endsection
<script type="text/javascript">
  function comfrimDelet(e,element) {
    e.preventDefault();
    swal({
      title: "Are you sure?",
      text: "Once deleted, you will not be able to recover this Client!",
      icon: "warning",
      buttons: true,
      dangerMode: true,
    })
    .then((willDelete) => {
      if (willDelete) {     
          window.location.href = $(element).attr('href');
      }
    })
  }
</script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
<script type="text/javascript">
  $(document).ready(function(){
    $("#search").on('keyup', function(){
      $value = $(this).val();
      $.ajax({
        type:'get',
        url:'{{URL::to('search')}}',
        data:{'search':$value},
        success:function(clients){
          $('tbody').html(clients);
        }
      }) 
    })
  });
  
  /*$(document).ready(function () {
    $.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
    });

    $('body').on('click', '#userStatus', function(){
      var userStatus = $(this).val();
      $.ajax({
        type:'POST',
        url:'{{URL::to('userStatus')}}',
        data:{'userStatus':userStatus},
        success:function(clients){
          //$('tbody').html(clients);
          console.log('working',clients);
        }
      }) 
    })
  });*/

</script>