@extends('templates.main',['pageTitle'=>'Clients','rootPage'=>'']) 
@section('content')
<div class="row">

     <!-- column -->
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-10">
                        <h4 class="card-title">List of Apps</h4>
                        <h6 class="card-subtitle">
                            @if (count($apps) === 1)
                                <code>{{ count($apps) }}</code> App total
                            @elseif (count($apps) > 1)
                                <code>{{ count($apps) }}</code> Total App
                            @else
                                No App :(
                            @endif
                        </h6>                       
                    </div>
                    <div class="col-2">
                        <a href="{{ route('app.create') }}" class="btn btn-info btn-rounded" color="primary">Create</a>
                    </div>
                    
                </div>

                <div class="table-responsive">
                    <table class="table color-bordered-table info-bordered-table">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Version</th>
                            <th>Description</th>
                            <th>file</th>
                            <th>Is Live</th>
                            <th>Modified By</th>
                            <th class="text-nowrap">Action</th>
                        </tr>
                        </thead> 
                        
                    <tbody> 

                             @foreach ($apps as $app)
                             <tr id="<?php echo $app->id ?>">
                                <td>{{$loop->iteration}}</td>
                                <td>{{$app->version}}</td>
                                <td>{{$app->description}}</td>
                                <td>{{$app->file}}</td>
                                <td>{{$app->is_live}}</td>
                                <td>@if(Auth::user()->id == $app->user->id)
                                        Me
                                    @else
                                        {{$app->user->first_name}}
                                    @endif
                                </td>
                                

                                <td class="text-nowrap">
                                    <a href="{{ route('app.edit',[$app->id]) }}" data-toggle="tooltip" data-original-title="Edit" class="btn btn-secondary btn-circle"> <i class="fa fa-pencil-alt"></i> </a>
                                    <a href="{{ route('app.delete',[$app->id]) }}" data-toggle="tooltip" data-original-title="Delete" class="btn btn-danger btn-circle" onclick="comfrimDelet(event,this)">  <i class="fa fa-times"></i> </a>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                     {{ $apps->links() }}
                </div>
            </div>
            <div class="return_page">
                <p>Go To:</p>
                 <a href="{{route ('client.home')}}">Home</a>
                 <a href="{{route ('client.index')}}">Clients</a>
            </div>
        </div>
    </div>
</div>
@endsection
<script type="text/javascript">
  function comfrimDelet(e,element) { 
    e.preventDefault();
    swal({
      title: "Are you sure?",
      text: "Once deleted, you will not be able to recover this App!",
      icon: "warning",
      buttons: true,
      dangerMode: true,
    })
    .then((willDelete) => {
      if (willDelete) {     
          window.location.href = $(element).attr('href');
      }
    })
  }
</script>
