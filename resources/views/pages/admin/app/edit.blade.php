@extends('templates.main',['pageTitle'=>'Edit','rootPage'=>'Client'])
@section('content') 

<div class="row">
   
    
    <div class="col-lg-12">
        <div class="card card-outline-info">
            <div class="card-header aaaa">
                <h4 class="m-b-0 text-white"><strong>Edit App:</strong> {{ $app->version }}</h4>
            </div>
            <div class="card-body card-body-form">
                <form class="form-horizontal form-material" method="POST" action="{{  route('app.update',[$app->id]) }}" enctype="multipart/form-data" >
                @csrf
                <div class="form-group m-t-20">
                    <div class="col-xs-12">
                         <label>Version </label> <span class="color-red">*</span>
                        <input type="number" class="form-control{{ $errors->has('version') ? ' is-invalid' : '' }}" name="version" value="{{ $app->version }}" readonly="readonly">
                    </div>
                </div>

                <div class="form-group m-t-20">
                    <div class="col-xs-12">
                        <label>Description</label>
                        <input type="text" class="form-control{{ $errors->has('description') ? ' is-invalid' : '' }}" name="description" value="{{ $app->description }}" >
                    </div>
                </div>            
                <div class="form-group ">
                    <div class="col-xs-12">
                         <label>APK File </label> <span class="color-red">*</span>
                        <input type="file" class="form-control{{ $errors->has('file') ? ' is-invalid' : '' }}" name="uploaded_file" value="{{ $app->file }}" >
                    </div>
                </div>

                <div class="form-group ">
                    <div class="col-xs-12 apps_radio_btn">
                        <label class="apps_radio_label">Is live</label>
                        <input id="true" name="radio" type="radio" value="true"{{ $app->is_live == 'true' ? 'checked' : ''}} >True
                        <input id="false" name="radio" type="radio" value="false"{{ $app->is_live == 'false' ? 'checked' : ''}}>False   
                    </div>
                </div>


                <div class="form-group storetext-center m-t-20">
                    <div class="col-xs-12">
                        <button class="btn btn-info btn-lg btn-block text-uppercase waves-effect waves-light" type="submit">  {{ __('Update') }}</button>
                    </div>
                </div>
            </form>
            <div class="return_page">
                <p>Go To:</p>
                 <a href="{{ route('app.index') }}">Back</a>
            </div>
            </div>
        </div>
    </div>
</div>
@endsection