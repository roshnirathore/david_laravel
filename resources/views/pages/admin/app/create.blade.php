@extends('templates.main',['pageTitle'=>'Create','rootPage'=>'Clients'])


@section('content')

<div class="row"> 
    
    <div class="col-lg-12">
        <div class="card card-outline-info">
            <div class="card-header">
                <h4 class="m-b-0 text-white">Create New App</h4>
            </div>
            <div class="card-body card-body-form">
                <form class="form-horizontal form-material" method="POST" action="{{ route('app.save') }}" enctype="multipart/form-data">
                @csrf
                <div class="form-group m-t-20">
                    <div class="col-xs-12">
                        <label>Version </label> <span class="color-red">*</span>
                        <input id="version" type="number" class="form-control{{ $errors->has('version') ? ' is-invalid' : '' }}" name="version" value="{{ old('version') }}" required autofocus>
                    </div>
                </div>
                <div class="form-group m-t-20">
                    <div class="col-xs-12">
                        <label>Description</label>
                        <textarea  type="text" class="form-control{{ $errors->has('description') ? ' is-invalid' : '' }}" name="description" value="{{ old('description') }}" ></textarea>
                    </div>
                </div>            
                <div class="form-group ">
                    <div class="col-xs-12">
                        <label>APK File </label> <span class="color-red">*</span>
                        <input id="file" type="file" class="form-control{{ $errors->has('file') ? ' is-invalid' : '' }}" name="uploaded_file" value="{{ old('file') }}" required>
                    </div>
                </div>

                <div class="form-group ">
                    <div class="col-xs-12 apps_radio_btn">
                        <label class="apps_radio_label">Is live</label>
                        <input id="true" name="radio" type="radio" checked value="true">True
                        <input id="false" name="radio" type="radio" value="false">False   
                    </div>
                </div>

                <div class="form-group text-center m-t-20">
                    <div class="col-xs-12">
                        <button class="btn btn-info btn-lg btn-block text-uppercase waves-effect waves-light" type="submit">  {{ __('Create') }}</button>
                    </div>
                </div>
            </form>
            <div class="return_page">
                <p>Go To:</p>
                <a href="{{ route('app.index') }}">APP List</a>
            </div>
            </div>
        </div>
    </div>
</div>
@endsection