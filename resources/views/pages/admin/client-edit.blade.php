@extends('templates.main',['pageTitle'=>'Edit','rootPage'=>'Client'])


@section('content')
<div class="row">
    @if ($errors->any())
      <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
            @endforeach
        </ul>
      </div><br /> 
    @endif
    
    <div class="col-lg-12">
        <div class="card card-outline-info">
            <div class="card-header aaaa">               
                <h4 class="m-b-0 text-white"><strong>Edit Client:</strong> {{ $client->name }}</h4>
                
                <!-- <a href="{{route('client.index')}}" class="btn btn-info btn-xs pull-right">
                  <i class="fa fa-fw fa-mail-reply" aria-hidden="true"></i>
                  <span class="hidden-xs">Back to </span>Clients
                </a> -->
            </div>
            <div class="card-body card-body-form">
                <form class="form-horizontal form-material client-form" method="POST" action="{{ route('client.update',[$client->id]) }}">
                @csrf
                <div class="form-group m-t-20">
                    <div class="col-xs-12">
                        <label>First Name </label> <span class="color-red">*</span>
                        <input id="first_name" type="text" class="form-control{{ $errors->has('first_name') ? ' is-invalid' : '' }}" name="first_name" value="{{$client->first_name}}" required autofocus>
                    </div>
                </div>
                <div class="form-group m-t-20">
                    <div class="col-xs-12">
                        <label>Last Name </label> <span class="color-red">*</span>
                        <input id="last_name" type="text" class="form-control{{ $errors->has('last_name') ? ' is-invalid' : '' }}" name="last_name" value="{{$client->last_name}}" required autofocus>
                    </div>
                </div>      
                <div class="form-group m-t-20">
                    <div class="col-xs-12">
                        <label>Password </label> <span class="color-red">*</span>
                       <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>
                    </div>
                </div>        
                <div class="form-group ">
                    <div class="col-xs-12">
                        <label>Company Name </label> <span class="color-red">*</span>
                        <input id="company_name" type="text" class="form-control{{ $errors->has('company_name') ? ' is-invalid' : '' }}" name="company_name" value="{{$client->company_name}}" required>
                    </div>
                </div>

                <div class="form-group ">
                    <div class="col-xs-12">
                        <label>Billing Address Line 1 </label> <span class="color-red">*</span>
                        <input id="address_line_1" type="text" class="form-control{{ $errors->has('address_line_1') ? ' is-invalid' : '' }}" name="address_line_1" value="{{ $client->address_line_1 }}" required>
                    </div>
                </div>

                <div class="form-group ">
                    <div class="col-xs-12">
                        <label>Billing Address Line 2</label>
                        <input id="address_line_2" type="text" class="form-control{{ $errors->has('address_line_2') ? ' is-invalid' : '' }}" name="address_line_2" value="{{ $client->address_line_2 }}">
                    </div>
                </div>

                <div class="form-group ">
                    <div class="col-xs-12">
                        <label>City </label> <span class="color-red">*</span>
                        <input id="city" type="text" class="form-control{{ $errors->has('city') ? ' is-invalid' : '' }}" name="city" value="{{ $client->city }}" required>
                    </div>
                </div>

                <div class="form-group ">
                    <div class="col-xs-12">
                        <label>State </label> <span class="color-red">*</span>
                        <input id="state" type="text" class="form-control{{ $errors->has('state') ? ' is-invalid' : '' }}" name="state" value="{{ $client->state }}" required>
                    </div>
                </div>

                <div class="form-group ">
                    <div class="col-xs-12">
                        <label>Country </label> <span class="color-red">*</span>
                        <input id="country" type="text" class="form-control{{ $errors->has('country') ? ' is-invalid' : '' }}" name="country" value="{{ $client->country }}" required>
                    </div>
                </div>

                <div class="form-group ">
                    <div class="col-xs-12">
                        <label>Zip Code </label> <span class="color-red">*</span>
                        <input id="zip_code" type="text" class="form-control{{ $errors->has('zip_code') ? ' is-invalid' : '' }}" name="zip_code" value="{{ $client->zip_code }}" required>
                    </div>
                </div> 
                <div class="form-group ">
                    <div class="col-xs-12">
                        <label>Phone Number </label> <span class="color-red">*</span>
                        <input id="phone_no" type="text" class="form-control{{ $errors->has('phone_no') ? ' is-invalid' : '' }}" name="phone_no" value="{{ $client->phone_no }}" required>
                    </div>
                </div> 
                <div class="form-group ">
                    <div class="col-xs-12">
                        <label>Phone Number Extension</label>
                        <input id="phone_number_extension" type="text" class="form-control" name="phone_number_extension" value="{{ $client->phone_number_extension }}">
                    </div>
                </div>

                <div class="form-group ">
                    <div class="col-xs-12">
                        <label> Alternate Phone or Mobile Number </label>
                        <input id="alter_phone_no" type="text" class="form-control{{ $errors->has('alter_phone_no') ? ' is-invalid' : '' }}" name="alter_phone_no" value="{{ $client->alter_phone_no }}">
                    </div>
                </div>
                <div class="form-group ">
                    <div class="col-xs-12">
                        <label>Timezone </label> <span class="color-red">*</span>
                        <select name="timezone" class="form-control">
                            <option value="">Select Timezone</option>
                            @foreach ($timezone_list as $row) 
                            <option value="{{$row['zone']}}" {{ $client->timezone == $row['zone'] ? 'selected' : ''}}>{{$row['show_zone']}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>

                <div class="form-group ">
                    <div class="col-xs-12">
                        <label>Status</label>
                        <select name="user_status" class="form-control">
                            <option value="">Select User Status</option>
                            <option value="1"  {{$client->activated == 1 ? 'selected' : ''}} >Active</option>
                                  <option value="0"  {{$client->activated == 0 ? 'selected' : ''}}  >Suspended</option>
                        </select>

                    </div>
                </div>
                
                <div class="form-group text-center m-t-20">
                    <div class="col-xs-12">
                        <button class="btn btn-info btn-lg btn-block text-uppercase waves-effect waves-light" type="submit">  {{ __('Update') }}</button>
                    </div>
                </div>
            </form>
            <div class="return_page">
                <p>Go To:</p>
                 <a href="{{route ('client.index')}}">Back</a>
            </div>
            </div>
        </div>
    </div>
                </div>
@endsection