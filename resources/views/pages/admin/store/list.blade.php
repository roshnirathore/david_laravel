@extends('templates.main',['pageTitle'=>'Clients','rootPage'=>''])
@section('content')
<div class="row">

     <!-- column -->
    <div class="col-12">
        <div class="card">
            <div class="card-body">               
              <div class="row">
                <div class="col-10">
                  <h4 class="card-title">List of Stores</h4>
                  <h6 class="card-subtitle">
                    @if (count($stores) === 1)
                      <code>{{ count($stores) }}</code> Store total
                    @elseif (count($stores) > 1)
                      <code>{{ count($stores) }}</code> Total Store
                    @else
                      No Store :(
                    @endif
                  </h6>
                </div>
                <div class="col-2"> 
                  <a href="{{ route('store.create',[$client_id]) }}" class="btn btn-info btn-rounded" color="info"> Create </a>  
                </div>
              </div>

                <div class="table-responsive">
                    <table class="table color-bordered-table info-bordered-table">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Name</th>
                            <th># Units</th>
                            <th>Created</th>
                            <th>Updated</th>
                            <th>Modified By</th>
                            <th class="text-nowrap">Action</th>
                        </tr>
                        </thead>
                        <tbody>
                             @foreach ($stores as $store)
                             
                             <tr>
                                <td>{{$loop->iteration}}</td>
                                <td> {{ ucfirst(trans($store->name)) }}</td>
                                <td><a href="{{ route('unit.index',[$client_id,$store->id]) }}">{{count($store->units)}}</a></td>
                                <td>{{$store->created_at->format('m-d-Y H:i')}}</td>
                                <td>{{$store->updated_at->format('m-d-Y H:i')}}</td>
                                <td>@if(Auth::user()->id == $store->user->id)
                                  Me
                                  @else
                                      {{$store->user->first_name}}
                                  @endif</td>

                                 <td class="text-nowrap">
                                    <a href="{{ route('store.edit',[$client_id,$store->id]) }}" data-toggle="tooltip" data-original-title="Edit" class="btn btn-secondary btn-circle"> <i class="fa fa-pencil-alt"></i> </a>
                                    <a href="{{ route('store.delete',[$client_id,$store->id]) }}" data-toggle="tooltip" data-original-title="Delete" class="btn btn-danger btn-circle" onclick="comfrimDelet(event,this)"> <i class="fa fa-times"></i> </a>
                                </td>
                            </tr>
                              @endforeach
                        </tbody>
                    </table>
                    {{ $stores->links() }}
                </div>
            </div>
            <div class="return_page">
                <p>Go To:</p>
                 <a href="{{route ('client.home')}}">Home</a>
                 <a href="{{route ('client.index')}}">Clients</a>
            </div>
        </div>
    </div>
</div>
@endsection
<script type="text/javascript">
  function comfrimDelet(e,element) { 
    e.preventDefault();
    swal({
      title: "Are you sure?",
      text: "Once deleted, you will not be able to recover this store!",
      icon: "warning",
      buttons: true,
      dangerMode: true,
    })
    .then((willDelete) => {
      if (willDelete) {     
          window.location.href = $(element).attr('href');
      }
    })
  }
</script>