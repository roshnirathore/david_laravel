@extends('templates.main',['pageTitle'=>'Create','rootPage'=>'Clients'])

 
@section('content')
<div class="row"> 
    
    <div class="col-lg-12">
        <div class="card card-outline-info">
            <div class="card-header">
                <h4 class="m-b-0 text-white">Create New Client</h4>
            </div>
            <div class="card-body card-body-form">
                <form class="form-horizontal form-material client-form" method="POST" action="{{ route('client.save') }}">
                @csrf 
                <div class="form-group m-t-20">
                    <div class="col-xs-12">
                        <label>First Name </label> <span class="color-red">*</span>
                        <input id="first_name" type="text" class="form-control{{ $errors->has('first_name') ? ' is-invalid' : '' }}" name="first_name" value="{{ old('first_name') }}" required autofocus>
                    </div>
                </div>
                <div class="form-group m-t-20">
                    <div class="col-xs-12">
                        <label>Last Name </label> <span class="color-red">*</span>
                        <input id="last_name" type="text" class="form-control{{ $errors->has('last_name') ? ' is-invalid' : '' }}" name="last_name" value="{{ old('last_name') }}" required autofocus>
                    </div>
                </div>            
                <div class="form-group ">
                    <div class="col-xs-12">
                        <label>Email </label> <span class="color-red">*</span>
                        <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required>
                    </div>
                </div>
                <div class="form-group ">
                    <div class="col-xs-12">
                        <label>Company Name </label> <span class="color-red">*</span>
                        <input id="company_name" type="text" class="form-control{{ $errors->has('company_name') ? ' is-invalid' : '' }}" name="company_name" value="{{ old('company_name') }}" required>
                    </div>
                </div>
                <div class="form-group ">
                    <div class="col-xs-12">
                        <label>Password </label> <span class="color-red">*</span>
                        <input id="password" type="password" class="form-control" name="password" required>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-xs-12">
                        <label>Confirm Password </label> <span class="color-red">*</span>
                        <input id="password-confirm"  type="password" class="form-control" name="password_confirmation" required>
                    </div>
                </div>
                <div class="form-group ">
                    <div class="col-xs-12">
                         <label>Billing Address Line 1 </label> <span class="color-red">*</span>
                        <input id="address_line_1"  type="text" class="form-control{{ $errors->has('address_line_1') ? ' is-invalid' : '' }}" name="address_line_1" value="{{ old('address_line_1') }}" required>
                    </div>
                </div>

                <div class="form-group ">
                    <div class="col-xs-12">
                        <label>Billing Address Line 2</label>
                        <input id="address_line_2" type="text" class="form-control{{ $errors->has('address_line_2') ? ' is-invalid' : '' }}" name="address_line_2" value="{{ old('address_line_2') }}">
                    </div>
                </div>

                <div class="form-group ">
                    <div class="col-xs-12">
                        <label>City </label> <span class="color-red">*</span>
                        <input id="city" type="text" class="form-control{{ $errors->has('city') ? ' is-invalid' : '' }}" name="city" value="{{ old('city') }}" required>
                    </div>
                </div>

                <div class="form-group ">
                    <div class="col-xs-12">
                        <label>State </label> <span class="color-red">*</span>
                        <input id="state" type="text" class="form-control{{ $errors->has('state') ? ' is-invalid' : '' }}" name="state" value="{{ old('state') }}" required>
                    </div>
                </div>

                <div class="form-group ">
                    <div class="col-xs-12">
                        <label>Country </label> <span class="color-red">*</span>
                        <input id="country" type="text" class="form-control{{ $errors->has('country') ? ' is-invalid' : '' }}" name="country" value="USA" required>
                    </div>
                </div>

                <div class="form-group ">
                    <div class="col-xs-12">
                        <label>Zip Code </label> <span class="color-red">*</span>
                        <input id="zip_code" type="text" class="form-control{{ $errors->has('zip_code') ? ' is-invalid' : '' }}" name="zip_code" value="{{ old('zip_code') }}" required>
                    </div>
                </div>
                <div class="form-group ">
                    <div class="col-xs-12">
                        <label>Phone Number </label> <span class="color-red">*</span>
                        <input id="phone_no" type="text" class="form-control{{ $errors->has('phone_no') ? ' is-invalid' : '' }}" name="phone_no" value="{{ old('phone_no') }}" required>
                    </div>
                </div>

                <div class="form-group ">
                    <div class="col-xs-12">
                        <label>Phone Number Extension</label> 
                        <input id="phone_number_extension" type="text" class="form-control{{ $errors->has('phone_number_extension') ? ' is-invalid' : '' }}" name="phone_number_extension" value="{{ old('phone_number_extension') }}">
                    </div>
                </div>

                <div class="form-group ">
                    <div class="col-xs-12">
                         <label>Alternate Phone or Mobile Number </label>
                        <input id="alter_phone_no" type="text" class="form-control{{ $errors->has('alter_phone_no') ? ' is-invalid' : '' }}" name="alter_phone_no" value="{{ old('alter_phone_no') }}">
                    </div>
                </div>
                
                 <div class="form-group ">
                    <div class="col-xs-12"> 
                        <label>TimeZone </label> <span class="color-red">*</span>
                        <select name="timezone" class="form-control">
                            <option value="">Select Timezone</option>
                            @foreach ($timezone_list as $row)
                            <option value="{{$row['zone']}}">{{$row['show_zone']}}</option>
                            @endforeach
                        </select> 
                    </div>
                </div>
                
                <div class="form-group text-center m-t-20">
                    <div class="col-xs-12">
                        <button class="btn btn-info btn-lg btn-block text-uppercase waves-effect waves-light" type="submit">  {{ __('Create') }}</button>
                    </div>
                </div>
            </form>
            <div class="form-group m-b-0">
                <div class="col-sm-12 text-center">
                    <p>Go To: <a href="{{ route('client.index') }}" class="text-info m-l-5">Clients</a></p>
                </div> 
            </div>
            </div>
        </div>
    </div>
</div>
@endsection