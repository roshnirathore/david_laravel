@extends('templates.main',['pageTitle'=>'Create','rootPage'=>'Units'])

@section('content')

<div class="row">
    <div class="col-lg-12">
        <div class="card card-outline-info">
            <div class="card-header">
                <h4 class="m-b-0 text-white">Create New Unit</h4>
            </div>
            <div class="card-body card-body-form">
                <form class="form-horizontal form-material" method="POST" action="{{ route('unit.save',[$client_id,$store_id]) }}">
                @csrf
                <div class="form-group m-t-20">
                    <div class="col-xs-12">
                        <label>Unit Name </label> <span class="color-red">*</span>
                        <input type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old('name') }}" required autofocus>
                    </div>
                </div>   
               
                <div class="form-group ">
                    <div class="col-xs-12">
                        <label>Timezone </label> <span class="color-red">*</span>
                        <select name="timezone" class="form-control">
                            <option value="">Select Timezone</option>
                            @foreach ($timezone as $row)
                            <option value="{{$row['zone']}}"  {{ $user_timezone == $row['zone'] ? 'selected' : ''}}>{{$row['show_zone']}}</option>
                            @endforeach 
                        </select>
                    </div>
                </div>

                <div class="form-group text-center m-t-20">
                    <div class="col-xs-12">
                        <button class="btn btn-info btn-lg btn-block text-uppercase waves-effect waves-light" type="submit">  {{ __('Create') }}</button>
                    </div>
                </div>
                <div class="form-group m-b-0">
                </div>
            </form>
            <div class="return_page">
                <p>Go To:</p>
                <a href="{{ route('unit.index',[$client_id,$store_id]) }}">Back</a>
            </div>
            </div>
        </div>
    </div>
</div>
@endsection