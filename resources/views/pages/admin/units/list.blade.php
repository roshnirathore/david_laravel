@extends('templates.main',['pageTitle'=>'Clients','rootPage'=>'']) 
@section('content')
<div class="row">

     <!-- column -->
    <div class="col-12">
        <div class="card">
            <div class="card-body">
               

                <div class="row">
                    <div class="col-10">
                        <!-- <h4 class="card-title">List of {{$store->name}} Units</h4> -->
                        <h4 class="card-title">List of Units</h4>
                        <h6 class="card-subtitle">
                            @if (count($units) === 1)
                                <code>{{ count($units) }}</code> Unit total
                            @elseif (count($units) > 1)
                                <code>{{ count($units) }}</code> Total Unit
                            @else
                                No Unit :(
                            @endif
                        </h6>                       
                    </div>
                    <div class="col-2">
                        <a href="{{ route('unit.create',[$client_id, $store]) }}" class="btn btn-info btn-rounded" color="primary">Create</a>
                    </div>
                    
                </div>

                <div class="table-responsive">
                    <table class="table color-bordered-table info-bordered-table">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Name</th>
                            <th>Created</th>
                            <th>Updated</th>
                            <th>Modified By</th>
                            <th class="text-nowrap">Action</th>
                        </tr>
                        </thead> 
                        <tbody>
                                @foreach ($units as $unit)
                                    <tr>
                                        <td>{{$loop->iteration}}</td>
                                        <td>{{ ucfirst(trans($unit->name)) }}</td> 
                                        <td>{{$unit->created_at->format('m-d-Y H:i')}}</td>
                                        <td>{{$unit->updated_at->format('m-d-Y H:i')}}</td>
                                        <td>@if(Auth::user()->id == $unit->user->id)
                                          Me
                                          @else
                                              {{$unit->user->first_name}}
                                          @endif</td>

                                        <td class="text-nowrap">
                                            <a href="{{ route('unit.edit',[$client_id,$unit->store_id, $unit->id]) }}" data-toggle="tooltip" data-original-title="Edit" class="btn btn-secondary btn-circle"> <i class="fa fa-pencil-alt"></i> </a>
                                            <a href="{{ route('unit.delete',[$client_id,$unit->store_id, $unit->id]) }}" data-toggle="tooltip" data-original-title="Delete" class="btn btn-danger btn-circle" onclick="comfrimDelete(event,this)"> <i class="fa fa-times"></i> </a>
                                            <a href="{{ route('feedback.delete',[$client_id,$unit->store_id, $unit->id]) }}" data-toggle="tooltip" data-original-title="Delete Feedback" class="btn btn-danger" onclick="comfrimDeleteFeedback(event,this)">Delete Feedback</a>
                                        </td>
                                    </tr>
                                @endforeach                      
                        
                        </tbody>
                    </table>
                    {{ $units->links() }}
                </div>
            </div>
            <div class="return_page">
                <p>Go To:</p>
                 <a href="{{route ('client.home')}}">Home</a>
                 <a href="{{route ('client.index')}}">Clients</a>
                <a href="{{route ('store.index',[$client_id])}}">Stores</a>
            </div>
        </div>
    </div>
</div>
@endsection
<script type="text/javascript">
  function comfrimDelete(e,element) { 
    e.preventDefault();
    swal({
      title: "Are you sure?",
      text: "Once deleted, you will not be able to recover this unit!",
      icon: "warning",
      buttons: true,
      dangerMode: true,
    })
    .then((willDelete) => {
      if (willDelete) {     
          window.location.href = $(element).attr('href');
      }
    })
  }

  /*function comfrimDeleteFeedback(e,element) { 
    e.preventDefault();
    swal({
      title: "Are you sure?",
      text: "Once deleted, you will not be able to recover this unit's feedback!",
      icon: "warning",
      buttons: true,
      dangerMode: true,
    })
    .then((willDelete) => {
      if (willDelete) {     
          window.location.href = $(element).attr('href');
      }
    })
  }*/
</script>