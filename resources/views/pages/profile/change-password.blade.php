@extends('templates.main',['pageTitle'=>'Password Reset','rootPage'=>'Profile'])


@section('content')
<div class="row">    
    <div class="col-lg-12">
        <div class="card card-outline-info">
            <div class="card-header aaaa">               
                <h4 class="m-b-0 text-white"><strong>Password Reset:</strong></h4>
            </div>
            <div class="card-body">
                <form class="form-horizontal form-material" method="POST" action="{{ route('password.update')}}">
                @csrf 
                <div class="form-group">
                            <label for="old_password" class="form-label">{{ __('Old Password') }} </label> <span class="color-red">*</span>
                            <input id="old_password" type="password" class="form-control{{ $errors->has('old_password') ? ' is-invalid' : '' }}" name="old_password" max-length="255" required autofocus>

                           <!--  @if ($errors->has('old_password'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('old_password') }}</strong>
                                </span>
                            @endif -->
                        </div>

                        <div class="form-group">
                            <label for="new_password" class="form-label">{{ __('New Password') }} </label> <span class="color-red">*</span>
                            <input id="new_password" type="password" class="form-control{{ $errors->has('new_password') ? ' is-invalid' : '' }}" name="new_password" max-length="255" required>

                           <!--  @if ($errors->has('new_password'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('new_password') }}</strong>
                                </span>
                            @endif -->
                        </div>

                        <div class="form-group">
                            <label for="confirm_new_password" class="form-label">{{ __('Confirm New Password') }} </label> <span class="color-red">*</span>
                            <input id="confirm_new_password" type="password" class="form-control{{ $errors->has('confirm_new_password') ? ' is-invalid' : '' }}" name="confirm_new_password" max-length="255" required>

                            <!-- @if ($errors->has('confirm_new_password'))
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $errors->first('confirm_new_password') }}</strong>
                                </span>
                            @endif -->
                        </div>
                
                <div class="form-group text-center m-t-20">
                    <div class="col-xs-12">
                        <button class="btn btn-info btn-lg btn-block text-uppercase waves-effect waves-light" type="submit">  {{ __('Update') }}</button>
                    </div>
                </div>
            </form>
            <div class="return_page">
                <p>Go To:</p>
                 <a href="{{route ('client.home')}}">Home</a>
            </div>
            </div>
        </div>
    </div>
                </div>
@endsection