@extends('templates.main',['pageTitle'=>'Edit','rootPage'=>'Profile'])
 

@section('content')
<div class="row">    
    <div class="col-lg-12">
        <div class="card card-outline-info">
            <div class="card-header aaaa">               
                <h4 class="m-b-0 text-white"><strong>Edit Profile:</strong></h4>
            </div>
            <div class="card-body">
                
                <form  class="form-horizontal form-material client-form" method="POST" action="{{ route('profile.update') }}" enctype="multipart/form-data">
                @csrf
                <div class="form-group m-t-20"> 
                    <div class="col-xs-12">
                        <label for="old_password" class="form-label">{{ __('First Name') }} </label> <span class="color-red">*</span>
                        <input id="first_name" placeholder="First Name" type="text" class="form-control{{ $errors->has('first_name') ? ' is-invalid' : '' }}" name="first_name" value="{{$first_name}}" required autofocus>
                    </div> 
                </div>
                <div class="form-group m-t-20">
                    <div class="col-xs-12">
                        <label for="old_password" class="form-label">{{ __('Last Name') }} </label> <span class="color-red">*</span>
                        <input id="last_name" placeholder="Last Name" type="text" class="form-control{{ $errors->has('last_name') ? ' is-invalid' : '' }}" name="last_name" value="{{$last_name}}" required autofocus>
                    </div>
                </div>           
                <div class="form-group ">
                    <div class="col-xs-12">
                        <label for="old_password" class="form-label">{{ __('Email') }} </label> <span class="color-red">*</span>
                        <input id="email" placeholder="Email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{$email}}" required readonly="readonly">
                    </div>
                </div>

                <div class="form-group ">
                    <div class="col-xs-12">
                        <label>Company Name </label> <span class="color-red">*</span>
                        <input id="company_name" type="text" class="form-control{{ $errors->has('company_name') ? ' is-invalid' : '' }}" name="company_name" value="{{$company_name}}" required>
                    </div>
                </div>

                <div class="form-group m-t-20">
                    <div class="col-xs-12">
                        <label for="old_password" class="form-label">{{ __('Billing Address Line 1') }} </label> <span class="color-red">*</span>
                        <input id="address_line_1" type="text" class="form-control{{ $errors->has('address_line_1') ? ' is-invalid' : '' }}" name="address_line_1" value="{{$address_line_1}}" >
                    </div>
                </div>

                <div class="form-group m-t-20">
                    <div class="col-xs-12">
                        <label for="old_password" class="form-label">{{ __('Billing Address Line 2') }}</label>
                        <input id="address_line_2"  type="text" class="form-control{{ $errors->has('address_line_2') ? ' is-invalid' : '' }}" name="address_line_2" value="{{$address_line_2}}" >
                    </div>
                </div>      

                <div class="form-group m-t-20">
                    <div class="col-xs-12">
                        <label for="old_password" class="form-label">{{ __('City') }} </label> <span class="color-red">*</span>
                        <input id="city" type="text" class="form-control{{ $errors->has('city') ? ' is-invalid' : '' }}" name="city" value="{{$city}}" >
                    </div>
                </div> 

                <div class="form-group m-t-20">
                    <div class="col-xs-12">
                        <label for="old_password" class="form-label">{{ __('State') }} </label> <span class="color-red">*</span>
                        <input id="state" type="text" class="form-control{{ $errors->has('state') ? ' is-invalid' : '' }}" name="state" value="{{$state}}" >
                    </div>
                </div> 
                
                <div class="form-group m-t-20">
                    <div class="col-xs-12">
                        <label for="old_password" class="form-label">{{ __('Country') }} </label> <span class="color-red">*</span>
                        <input id="country" type="text" class="form-control{{ $errors->has('country') ? ' is-invalid' : '' }}" name="country" value="{{$country}}" >
                    </div>
                </div>

                <div class="form-group m-t-20">
                    <div class="col-xs-12">
                        <label for="old_password" class="form-label">{{ __('Zip Code') }} </label> <span class="color-red">*</span>
                        <input id="zip_code" type="text" class="form-control{{ $errors->has('zip_code') ? ' is-invalid' : '' }}" name="zip_code" value="{{$zip_code}}" >
                    </div>
                </div>

                <div class="form-group m-t-20">
                    <div class="col-xs-12">
                        <label for="old_password" class="form-label">{{ __('Phone Number') }} </label> <span class="color-red">*</span>
                        <input id="phone_no" type="text" class="form-control{{ $errors->has('phone_no') ? ' is-invalid' : '' }}" name="phone_no" value="{{$phone_no}}" >
                    </div>
                </div>
                <div class="form-group ">
                    <div class="col-xs-12">
                        <label>Phone Number Extension</label>
                        <input id="phone_number_extension" type="text" class="form-control" name="phone_number_extension" value="{{ $phone_number_extension }}">
                    </div>
                </div>
                <div class="form-group m-t-20">
                    <div class="col-xs-12">
                        <label for="old_password" class="form-label">{{ __('Alternate Phone or Mobile Number') }}</label>
                        <input id="alter_phone_no" type="text" class="form-control{{ $errors->has('alter_phone_no') ? ' is-invalid' : '' }}" name="alter_phone_no" value="{{$alter_phone_no}}" >
                    </div>
                </div>

                <div class="form-group m-t-20">
                    <div class="col-xs-12">
                        <label>Timezone </label> <span class="color-red">*</span>
                        <select name="timezone" class="form-control">
                            <option value="">Select Timezone</option>
                            @foreach ($timezone_list as $row) 
                            <option value="{{$row['zone']}}" {{ $user_timezone == $row['zone'] ? 'selected' : ''}}>{{$row['show_zone']}}</option>
                            @endforeach
                        </select>
                    </div>
                </div>

                <div class="form-group text-center m-t-20">
                    <div class="col-xs-12">
                        <button class="btn btn-info btn-lg btn-block text-uppercase waves-effect waves-light" type="submit">  {{ __('Update') }}</button>
                    </div>
                </div>
            </form>
            <div class="return_page">
                <p>Go To:</p>
                 <a href="{{route ('client.home')}}">Home</a>
            </div>
            </div>
        </div>
    </div>
                </div>
@endsection