@extends('templates.main',['pageTitle'=>'Edit','rootPage'=>'Client'])


@section('content')
<div class="row">

    
    <div class="col-lg-12">
        <div class="card card-outline-info">
            <div class="card-header aaaa">               
                <h4 class="m-b-0 text-white"><strong>Editing User:</strong> {{ $user->name }}</h4>
                <!-- <a href="{{route('storeuser.index')}}" class="btn btn-info btn-xs pull-right">
                  <i class="fa fa-fw fa-mail-reply" aria-hidden="true"></i>
                  <span class="hidden-xs">Back to </span>Store User
                </a> -->
            </div>
            <div class="card-body">
                <form class="form-horizontal form-material" method="POST" action="{{ route('storeuser.update',[$user->id]) }}">
                @csrf
                <div class="form-group m-t-20">
                    <div class="col-xs-12">
                        <input id="first_name" placeholder="First Name" type="text" class="form-control{{ $errors->has('first_name') ? ' is-invalid' : '' }}" name="first_name" value="{{$user->first_name}}" required autofocus>
                    </div>
                </div>
                <div class="form-group m-t-20">
                    <div class="col-xs-12">
                        <input id="last_name" placeholder="Last Name" type="text" class="form-control{{ $errors->has('last_name') ? ' is-invalid' : '' }}" name="last_name" value="{{$user->last_name}}" required autofocus>
                    </div>
                </div>            
                <div class="form-group ">
                    <div class="col-xs-12">
                        <input id="email" placeholder="Email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{$user->email}}" required readonly="readonly">
                    </div>
                </div>
                
                <div class="form-group text-center m-t-20">
                    <div class="col-xs-12">
                        <button class="btn btn-info btn-lg btn-block text-uppercase waves-effect waves-light" type="submit">  {{ __('Update') }}</button>
                    </div>
                </div>
                <!-- <div class="form-group m-b-0">
                    <div class="col-sm-12 text-center">
                        <p>Return to User list: <a href="{{ route('storeuser.index') }}" class="text-info m-l-5"><b>Store Users</b></a></p>
                    </div>
                </div> -->
            </form>
            </div>
        </div>
    </div>
                </div>
@endsection