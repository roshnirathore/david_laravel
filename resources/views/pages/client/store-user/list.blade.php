@extends('templates.main',['pageTitle'=>'Clients','rootPage'=>''])


@section('content')
<div class="row">

     <!-- column -->
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-10">
                        <h4 class="card-title">List of Store User</h4>
                        <h6 class="card-subtitle">
                            @if (count($storeUsers) === 1)
                                <code>{{ count($storeUsers) }}</code> user total
                            @elseif (count($storeUsers) > 1)
                                <code>{{ count($storeUsers) }}</code> total user
                            @else
                                No Store :(
                            @endif
                        </h6>
                    </div>
                    <div class="col-2">
                        <a href="{{ route('storeuser.create') }}" class="btn btn-info btn-rounded" color="info"> Create </a> 
                    </div>
                </div>
                
                    
                <div class="table-responsive">
                    <table class="table color-bordered-table info-bordered-table">
                        <thead>
                        <tr > 
                            <th>#</th>
                            <th>Name</th>
                            <th>Email</th>
                            <th>Created</th>
                            <th>Updated</th>
                            <th class="text-nowrap">Action</th>
                        </tr>
                        </thead>
                        <tbody>
                             @foreach ($storeUsers as $su)
                             <tr>
                                <td>{{$loop->iteration}}</td>
                                <td>{{$su->first_name}} {{$su->last_name}}</td>
                                <td>{{$su->email}}</td>
                                <td>{{$su->created_at}}</td>
                                <td>{{$su->updated_at}}</td>

                                 <td class="text-nowrap">
                                    <a href="{{ route('storeuser.edit',[$su->id]) }}" data-toggle="tooltip" data-original-title="Edit" class="btn btn-secondary btn-circle"> <i class="fa fa-pencil-alt"></i> </a>
                                    <a href="{{ route('storeuser.delete',[$su->id]) }}" data-toggle="tooltip" data-original-title="Delete" class="btn btn-danger btn-circle" onclick="comfrimDelet(event,this)"> <i class="fa fa-times"></i> </a>
                                    
                                </td>
                            </tr>
                              @endforeach
                        </tbody>
                    </table>
                    {{ $storeUsers->links() }}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
<script type="text/javascript">
  function comfrimDelet(e,element) {
    e.preventDefault();
    swal({
      title: "Are you sure?",
      text: "Once deleted, you will not be able to recover this file!",
      icon: "warning",
      buttons: true,
      dangerMode: true,
    })
    .then((willDelete) => {
      if (willDelete) {     
          window.location.href = $(element).attr('href');
      }
    })
  }
</script>