@extends('templates.main',['pageTitle'=>'Create','rootPage'=>'Store User'])


@section('content')
<div class="row"> 
    
    <div class="col-lg-12">
        <div class="card card-outline-info">
            <div class="card-header">
                <h4 class="m-b-0 text-white">Create New Store User</h4>
            </div>
            <div class="card-body">
                <form class="form-horizontal form-material" method="POST" action="{{ route('storeuser.save') }}">
                @csrf
                <div class="form-group m-t-20">
                    <div class="col-xs-12">
                        <input id="first_name" placeholder="First Name" type="text" class="form-control{{ $errors->has('first_name') ? ' is-invalid' : '' }}" name="first_name" value="{{ old('first_name') }}" required autofocus>
                    </div>
                </div>
                <div class="form-group m-t-20">
                    <div class="col-xs-12">
                        <input id="last_name" placeholder="Last Name" type="text" class="form-control{{ $errors->has('last_name') ? ' is-invalid' : '' }}" name="last_name" value="{{ old('last_name') }}" required autofocus>
                    </div>
                </div>            
                <div class="form-group ">
                    <div class="col-xs-12">
                        <input id="email" placeholder="Email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required>
                    </div>
                </div>
                <div class="form-group ">
                    <div class="col-xs-12">
                        <input id="password" placeholder="Password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-xs-12">
                        <input id="password-confirm" placeholder="Confirm Password" type="password" class="form-control" name="password_confirmation" required>
                    </div>
                </div>
                <div class="form-group text-center m-t-20">
                    <div class="col-xs-12">
                        <button class="btn btn-info btn-lg btn-block text-uppercase waves-effect waves-light" type="submit">  {{ __('Create') }}</button>
                    </div>
                </div>
                <!-- <div class="form-group m-b-0">
                    <div class="col-sm-12 text-center">
                        <p>Return to User list: <a href="{{ route('storeuser.index') }}" class="text-info m-l-5"><b>Store ghjftygUsers</b></a></p>
                    </div>
                </div> -->
            </form>
            </div>
        </div>
    </div>
                </div>
@endsection