@extends('templates.main',['pageTitle'=>'Clients','rootPage'=>''])
@section('content')
<div class="row">

     <!-- column -->
    <div class="col-12">
        <div class="card">
            <div class="card-body">               
              <div class="row">
                <div class="col-10">
                  <h4 class="card-title">List of Stores</h4>
                  <h6 class="card-subtitle">
                    @if (count($stores) === 1)
                      <code>{{ count($stores) }}</code> Store total
                    @elseif (count($stores) > 1)
                      <code>{{ count($stores) }}</code> Total Store
                    @else
                      No Store :(
                    @endif
                  </h6>
                </div>
              </div>

                <div class="table-responsive">
                    <table class="table color-bordered-table info-bordered-table">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Name</th>
                            <th># Units</th>
                            <th>Created</th>
                            <th>Updated</th>
                            <th>Modified By</th>
                            <th class="text-nowrap">Action</th>
                        </tr>
                        </thead>
                        <tbody>
                             @foreach ($stores as $store)
                             
                             <tr>
                                <td>{{$loop->iteration}}</td>
                                <td> {{ ucfirst(trans($store->name)) }}</td>
                                <td><a href="{{route('client_unit.index',[$store->id])}}">{{count($store->units)}}</a></td>
                                <td>{{$store->created_at->format('m-d-Y H:i')}}</td>
                                <td>{{$store->updated_at->format('m-d-Y H:i')}}</td>
                                <td>@if(Auth::user()->id == $store->user->id)
                                  Me
                                  @else
                                      {{$store->user->first_name}}
                                  @endif</td>
                                <td class="text-nowrap">
                                  <a href="{{ route('client_store.edit',[$store->id]) }}" data-toggle="tooltip" data-original-title="Edit" class="btn btn-secondary btn-circle"> <i class="fa fa-pencil-alt"></i> </a>
                                </td>
                            </tr>
                              @endforeach
                        </tbody>
                    </table>
                    {{ $stores->links() }}
                </div>
            </div>
            <div class="return_page">
                <p>Go To:</p>
                 <a href="{{route ('client.home')}}">Home</a>
            </div> 
        </div>
    </div>
</div>
@endsection
