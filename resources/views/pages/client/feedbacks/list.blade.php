@extends('templates.main',['pageTitle'=>'Clients','rootPage'=>'']) 
@section('content')

<script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
<!-- <script src="/vendor/wrappixel/material-pro/4.2.1/assets/plugins/custom-js/custom.js"></script> -->
<div class="row">
     <!-- column -->
    <div class="col-12"> 
        <div class="card">
            <div class="card-body">  
                <!-- row -->  
                <div class="row">
                    <div class="col-2">       
                        <form method="POST" action="" name="filter_form">
                            {{ csrf_field() }}
                            <div class="create-report">
                              <h4 class="card-title">CREATE REPORT</h4>
                              @if(isset($users))
                               @if (Auth::user()->roles[0]->slug == 'admin')
                               <h5>Company</h5>
                              <select  name="users[]" id="user_id" multiple class="form-control" >
                                
                                  @foreach ($users as $user)
                                    @if(Auth::user()->id != $user->id)
                                      <option value="{{$user->id}}"  
                                       @if(isset($saved_users))
                                         @foreach ($saved_users as $saved_user)
                                            {{$saved_user == $user->id ? 'selected' : ''}}
                                           @endforeach
                                          @elseif(isset($selectedUserPriority)) 
                                            @foreach ($selectedUserPriority as $selectUser)
                                               {{$selectUser == $user->id ? 'selected' : ''}}
                                             @endforeach
                                          @endif > 
                                        {{ ucfirst(trans($user->company_name)) }}</option> 
                                    @endif
                                  @endforeach  
                                @endif
                                @endif
                              </select>

                              <h5>Store</h5>
                              <select  name="stores[]" id="store_id" multiple class="form-control" >
                                  @foreach ($stores as $store)
                                      <option value="{{$store->id}}"
                                        @if(isset($saved_store_id))
                                         @foreach ($saved_store_id as $saved_store)
                                            {{$saved_store == $store->id ? 'selected' : ''}}
                                           @endforeach
                                          @elseif(isset($selectedstorePriority)) 
                                            @foreach ($selectedstorePriority as $selectstore)
                                               {{$selectstore == $store->id ? 'selected' : ''}}
                                             @endforeach
                                          @endif

                                      >{{ ucfirst(trans($store->name)) }}</option>
                                  @endforeach
                              </select>
                            </div>
                            <div class="date-picker pt-15">
                              <h5>Date Range</h5>
                              <div class="date-range">
                                <span class="date-from">
                                  <input type="text" name="from_date" id="from_date" class="form-control" placeholder="From Date" 
                                  value="@if(isset($saved_from_date)){{$saved_from_date}}@else{{ request()->input('from_date') }}@endif">
                                  <i class="fa fa-calendar" onClick="$('#from_date').focus();" style="cursor: pointer; margin-left: 5px"></i> 
                                  <span></span>
                                </span> 
                                
                                <span class="date-to">
                                  <input type="text" class="form-control" name="to_date" id="to_date" placeholder="To Date"  value="@if(isset($saved_to_date)){{$saved_to_date}}@else{{ request()->input('to_date') }}@endif">
                                  <i class="fa fa-calendar" onclick="$('#to_date').focus();" style="cursor: pointer; margin-left: 5px"></i>
                                </span>
                              </div>
                            </div>
                            <div class="time-picker-section pt-15">
                              <h5>Time Range</h5>
                              <div class="time-picker">
                                <span class="time-from">
                                  <input type="text" class="form-control" name="from_time" id="from_time" placeholder="From Time"  value="@if(isset($saved_from_time)){{$saved_from_time}}@else{{ request()->input('from_time') }}@endif">
                                  <i class='far fa-clock' onClick="$('#from_time').focus();" style="cursor: pointer; margin-left: 5px"></i>
                                </span>
                                <span class="time-to">
                                  <input type="text" placeholder="To Time"  class="form-control" name="to_time" id="to_time" value="@if(isset($saved_to_time)){{$saved_to_time}}@else{{ request()->input('to_time') }}@endif">
                                  <i class='far fa-clock' onClick="$('#to_time').focus();" style="cursor: pointer; margin-left: 5px"></i>
                                </span>
                              </div>
                            </div>
                            
                            <div class="day-week pt-15">
                                <label>Day of Week</label>  
                                <div class="input-group"> 
                                  <div class="custom-check">
                                      <label class="custom-control custom-radio normal-radio rad">
                                        <input name="weekDays[]" type="checkbox" class="custom-control-input" value="Sunday" <?php 
                                          if(isset($saved_weekDays)){
                                            foreach ($saved_weekDays as $saved_weekDay){  
                                              if($saved_weekDay == 'Sunday'){ echo 'checked'; }
                                              else{ echo ''; }
                                            }
                                          }
                                          elseif(isset($selectedDay)){
                                            foreach ($selectedDay as $weekDay){  
                                              if($weekDay == 'Sunday'){ echo 'checked'; }
                                              else{ echo ''; }
                                            }
                                          } ?> 
                                        >
                                        <i class=" om-control-description">S</i>
                                      </label> 

                                      <label class="custom-control custom-radio normal-radio rad">
                                        <input name="weekDays[]" type="checkbox" class="custom-control-input" value="Monday" <?php 
                                          if(isset($saved_weekDays)){
                                            foreach ($saved_weekDays as $saved_weekDay){  
                                              if($saved_weekDay == 'Monday'){ echo 'checked'; }
                                              else{ echo ''; }
                                            }
                                          }
                                          elseif(isset($selectedDay)){
                                            foreach ($selectedDay as $weekDay){ 
                                              if($weekDay == 'Monday'){ echo 'checked'; }
                                              else{ echo ''; }
                                            }
                                          } ?> 
                                        >
                                        <i class=" om-control-description">M</i>
                                      </label>

                                      <label class="custom-control custom-radio normal-radio rad">
                                        <input name="weekDays[]" type="checkbox" class="custom-control-input" value="Tuesday" <?php 
                                          if(isset($saved_weekDays)){
                                            foreach ($saved_weekDays as $saved_weekDay){  
                                              if($saved_weekDay == 'Tuesday'){ echo 'checked'; }
                                              else{ echo ''; }
                                            }
                                          }
                                          elseif(isset($selectedDay)){
                                            foreach ($selectedDay as $weekDay){ if($weekDay == 'Tuesday'){ echo 'checked'; }
                                              else{ echo ''; }
                                            }
                                          } ?> 
                                        >
                                        <i class=" om-control-description">T</i>
                                      </label>

                                      <label class="custom-control custom-radio normal-radio rad">
                                        <input name="weekDays[]" type="checkbox" class="custom-control-input" value="Wednesday"  <?php 
                                          if(isset($saved_weekDays)){
                                            foreach ($saved_weekDays as $saved_weekDay){  
                                              if($saved_weekDay == 'Wednesday'){ echo 'checked'; }
                                              else{ echo ''; }
                                            }
                                          }
                                          elseif(isset($selectedDay)){
                                            foreach ($selectedDay as $weekDay){ 
                                             if($weekDay == 'Wednesday'){ echo 'checked'; }
                                              else{ echo ''; }
                                            }
                                          } ?> 
                                        >
                                        <i class=" om-control-description">W</i>
                                      </label>

                                      <label class="custom-control custom-radio normal-radio rad">
                                        <input name="weekDays[]" type="checkbox" class="custom-control-input" value="Thursday" <?php 
                                          if(isset($saved_weekDays)){
                                            foreach ($saved_weekDays as $saved_weekDay){  
                                              if($saved_weekDay == 'Thursday'){ echo 'checked'; }
                                              else{ echo ''; }
                                            }
                                          }
                                          elseif(isset($selectedDay)){
                                            foreach ($selectedDay as $weekDay){
                                              if($weekDay == 'Thursday'){ echo 'checked'; }
                                              else{ echo ''; }
                                            }
                                          } ?> 
                                        >
                                        <i class=" om-control-description">Th</i>
                                      </label>

                                      <label class="custom-control custom-radio normal-radio rad">
                                        <input name="weekDays[]" type="checkbox" class="custom-control-input" value="Friday" <?php 
                                          if(isset($saved_weekDays)){
                                            foreach ($saved_weekDays as $saved_weekDay){  
                                              if($saved_weekDay == 'Friday'){ echo 'checked'; }
                                              else{ echo ''; }
                                            }
                                          }
                                          elseif(isset($selectedDay)){
                                            foreach ($selectedDay as $weekDay){ 
                                              if($weekDay == 'Friday'){ echo 'checked'; }
                                              else{ echo ''; }
                                            }
                                          }  ?> 
                                        >
                                        <i class=" om-control-description">F</i>
                                      </label>

                                      <label class="custom-control custom-radio normal-radio rad">
                                        <input name="weekDays[]" type="checkbox" class="custom-control-input" value="Saturday" <?php 
                                          if(isset($saved_weekDays)){
                                            foreach ($saved_weekDays as $saved_weekDay){  
                                              if($saved_weekDay == 'Saturday'){ echo 'checked'; }
                                              else{ echo ''; }
                                            }
                                          }
                                          elseif(isset($selectedDay)){
                                            foreach ($selectedDay as $weekDay){if($weekDay == 'Saturday'){ echo 'checked'; }
                                              else{ echo ''; }
                                            }
                                          } ?> 
                                        >
                                        <i class=" om-control-description">S</i>
                                    </label>
                                  </div>
                                </div> 
                            </div> 
                            <div class="date-picker pt-15">
                             <h5>TimeZone</h5>
                               <select name="timezone" class="form-control">
                                <option value="">Select Timezone</option>
                                @foreach ($timezone as $row)
                                  <!-- <option value="{{$row['zone']}}" <?php //if(isset($selectTimezone)){if($selectTimezone == $row['zone']) echo 'selected';} elseif(isset($saved_time_zone)){if($saved_time_zone == $row['zone']) echo 'selected';} ?> >({{$row['GMT_difference']. ' ) '.$row['zone']}}</option> -->

                                 <option value="{{$row['zone']}}" <?php if(isset($selectTimezone)){if($selectTimezone == $row['zone']) echo 'selected';} elseif(isset($saved_time_zone)){if($saved_time_zone == $row['zone']) echo 'selected';} ?> >{{$row['show_zone']}}</option>
                                @endforeach
                              </select>
                            </div>
                            <div class="report-filter-btn">
                              <button class="" type="submit" name="fiter_report_btn">  {{ __('Filter Report') }}</button> 
                            </div>
                        </form>
                        <!-- <form action="{{ route('client.home') }}">
                          <div class="report-filter-btn">
                              <button class="" type="submit" name="fiter_refresh_btn">  {{ __('Filter Refresh') }}</button>

                            </div>
                        </form> -->
                        <div class="fiter_refresh_div">
                          <a href="{{route ('client.home')}}" class="fiter_refresh_btn">Clear Filters</a> 
                        </div>
                          
                        
                        <div class="create-report">
                          <h4 class="card-title">SAVED REPORT</h4>
                          <ul class="paginationList" id="user_report"></ul>
                          <div id="pagination-container" style="cursor: pointer;">
                          </div>  
                        </div>


                    </div>

                    <div class="col-10">
                        <div class="report-box-container ">
                          <!-- Update report button -->
                          @if(isset($update_btn_set)) 
                           <button type="button" class="btn btn-info btn-lg"
                           data-toggle="modal" data-target="#reportModal" id="update_report">Update This Report</button>    
                          <!-- save report button -->
                          @elseif(isset($json_fiter_param))
                            @if(count($report_data))
                              <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#reportModal" id="save_report">Save This Report</button> 
                            @endif
                          @endif
                          <div id="reportModal" class="modal fade" role="dialog">
                            <div class="modal-dialog">

                              <!-- Modal content-->
                              <div class="modal-content">
                                <div class="modal-header">
                                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                                  <h4 class="modal-title"></h4>
                                </div>
                                <div class="modal-body">
                                   <form method="POST" action="{{ route('client_report.save') }}">
                                  <!--  <form> -->
                                    @csrf
                                      <div class="report-name pt-15">
                                        <div> 
                                          <input type="hidden" name="report_id" value="@if(isset($update_btn_set)){{$update_btn_set}} @endif">
                                          <input type="hidden" name="filter_param[]" value="@if(isset($json_fiter_param)){{$json_fiter_param}} @elseif(isset($fiter_param)){{$fiter_param}}@endif">
                                          
                                        </div>
                                            <label>Report Name</label>
                                            <input type="text" name="report_name" id="report_name" value="@if(isset($report_name)){{$report_name}} @endif">
                                        </div>
                                        <div class="report-filter-btn">
                                            <button type="submit">  {{ __('Save') }}</button>
                                        </div>
                                    </form>
                                </div>
                                <div class="modal-footer">
                                  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                </div>
                              </div>
                            </div>
                          </div>
                            <!-- Chart Report -->
                            @if(isset($delete_btn_set)) 
                              <div class="delete-report">
                                <a href="{{ route('report.delete',[$delete_report_id]) }}" data-toggle="tooltip" data-original-title="Delete Report" class="btn btn-danger" onclick="comfrimDelet(event,this)">DELETE REPORT</a>
                              </div>
                             @endif
                            <script src="https://canvasjs.com/assets/script/canvasjs.min.js"></script>
                            <div style="position: relative;display: block;">
                              <div class = "remove_text" id="chartContainer" style="height: 360px; width: 90%;" ></div>
                              <div id="not_data">Data is not Available</div>
                              @if(count($report_data))
                              <div class="body_thumbs">
                                <div class="thumbs-up-green">
                                    <img src="/images/thumbs-up-small.png"
                                 alt="Thumbs Up">
                                </div>
                                <div class="thumbs-down-red">
                                    <img src="/images/thumbs-down-small.png"
                                 alt="Thumbs Down">
                                </div>
                              </div>
                              @endif
                            </div>
                          <script type="text/javascript">
                          $(document).ready(function(){

                            var weekDays  = <?php if( $weekDays == "false") echo $weekDays; else  echo json_encode($weekDays); ?>;
                            console.log(weekDays);
                            var weekDay_len;
                            if (weekDays) {
                             // console.log(weekDays);
                               weekDay_len = weekDays.length;
                             }else{
                              weekDay_len=0;
                             }

                            var title_report_name  = '<?php if($report_name) echo $report_name; else  echo $report_name; ?>';

                              var graph_data = {!! $report_data->toJson() !!};
                             var check_data = graph_data.length;
                            if(check_data == 0){

                              $("#chartContainer").hide();
                              $("#not_data").show();
                            }else{

                              $("#not_data").hide();
                              var likeArray = <?php echo $likeArray; ?>;
                              var unLikeArray = <?php echo $unLikeArray; ?>;
                              var dateArray = <?php echo $dateArray; ?>;

                             var likeFinalData = new Array();
                             var unlikeFinalData = new Array();
                             var wdCheck  = new Array();
                          for (var i = dateArray.length - 1; i >= 0; i--) {
                              
                              var likeData = {  y: likeArray[i] , label: dateArray[i]};
                                likeFinalData.push(likeData);
                            
                              var unlikeData = {  y: unLikeArray[i] , label: dateArray[i]};
                               unlikeFinalData.push(unlikeData);
                          }

                          var chart = new CanvasJS.Chart("chartContainer",{
                            zoomEnabled: true,
                            animationEnabled: true,
                            exportFileName: "Feedback Report",
                            exportEnabled: true,
                            title:{ text: title_report_name },
                            axisX: {
                              labelAutoFit: true
                            },
                             axisY:{
                                interval: "{total}",
                                labelAutoFit: true
                              },
                              dataPointMaxWidth: 80,
                            data: [
                                    {
                                      type: "stackedColumn",
                                      indexLabel: "{y}",
                                      color: "red",
                                      dataPoints: unlikeFinalData
                                    }, 
                                     
                                    {
                                      type: "stackedColumn",
                                      indexLabel: "{y}",
                                      color: "#8dc642",
                                      dataPoints: likeFinalData
                                    }, 
                                    {
                                      //type: "scatter",  
                                      type: "bubble",  
                                      color: "transparent",
                                      indexLabelFontWeight: "bold",
                                      indexLabelFontColor: "#8dc642",
                                      indexLabel: "{y} {successper}",
                                      toolTipContent: null,
                                      dataPoints: []
                                    }
                                  ]
                          });
                           //Show total number like and unlike 
                          for(var i = 0; i < chart.options.data[0].dataPoints.length; i++) {
                            
                            var likeTotal = 0;
                            var unlikeTotal = 0;
                            if (chart.options.data[0].dataPoints[i] == undefined) {
                                unlikeTotal = 0;
                            }else{
                                unlikeTotal = chart.options.data[0].dataPoints[i].y;
                            }

                            if (chart.options.data[1].dataPoints[i] == undefined) {
                                likeTotal = 0;
                            }else{
                                likeTotal = chart.options.data[1].dataPoints[i].y;
                            }
                            var likePer = parseInt((likeTotal/(unlikeTotal+likeTotal)*100));
                             
                          likePer = "("+likePer+"%)";
                          console.log(likePer);
                            var total = likeTotal + unlikeTotal;
                            chart.options.data[2].dataPoints.push({
                              label: chart.options.data[0].dataPoints[i].label, 
                              y: total,
                              successper:likePer,
                            });
                          }
                        //  
                          chart.render();
                        }

                        
                          });
                        </script>
                        <!-- Multiselect dropdown list -->
                        <script type="text/javascript">
                          $(document).ready(function(){
                            $('#user_id').multiselect({
                              nonSelectedText: 'Select Company',
                              enableFiltering: true,
                              includeSelectAllOption: true,
                              enableCaseInsensitiveFiltering: true
                            });

                           $('#store_id').multiselect({
                            nonSelectedText: 'Select Store',
                            enableFiltering: true,
                            includeSelectAllOption: true,
                            enableCaseInsensitiveFiltering: true
                           });

                            // Get Report list from report table by ajax 
                            $.ajaxSetup({
                              headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                              }
                            });
                            //get stores list by client id
                            $('#user_id').change(function(){ 
                              var userId = $(this).val();
                              if(userId){
                                $.ajax({
                                  url:'/reports',
                                  type:'GET',
                                  data: {
                                        "client_id": userId
                                       },
                                  success:function(data){
                                  var store_list = data.success;
                                  var store_option = "";
                                  $(store_list).each(function(index,item){
                                    var store_name = item.name;
                                    var store_id = item.id;

                                    store_option += "<option value ='"+store_id+"'>"+store_name+"</option>";
                                  });

                                  $('#store_id').html(store_option).multiselect("destroy").multiselect({
                                      nonSelectedText: 'Select Store',
                                      enableFiltering: true,
                                      includeSelectAllOption: true,
                                      enableCaseInsensitiveFiltering: true
                                    });

                                  },
                                 error : function(res){
                                  console.log(res);
                                 }
                                });
                              }
                            });
                                //Get Saved Report list
                                $.ajax({
                                  url: '/reports/report_list', 
                                  type: "GET",
                                  success: function(data){
                                    var report_data = data.success;
                                  var route = window.location.origin;
                                  $(report_data).each(function (index, item) {
                                    var report_name = item.report_name;
                                    var id = item.id;

                                    var div_data="<li class='listItem'><a href ="+route+"/report/"+id+">"+report_name+"</a></li>";
                                    $(div_data).appendTo('#user_report');
                                  }); 
                                  paginationReinitialize(); //report list pagination 
                                 
                                  }
                                });
                          });
                        </script>
                        <!-- c -->
                        </div>
                    </div>
                </div>
                <div class="col-12">
                  <div class="return_page">
                    <p>Go To:</p>
                    <a href="{{route ('client.home')}}">Home</a>
                  </div>
                </div>
              </div>  
            </div>
        </div> 
    </div>
</div>
<script type="text/javascript">
  function comfrimDelet(e,element) {
    e.preventDefault();
    swal({
      title: "Are you sure?",
      text: "Once deleted, you will not be able to recover this Report!",
      icon: "warning",
      buttons: true,
      dangerMode: true,
    })
    .then((willDelete) => {
      if (willDelete) {     
          window.location.href = $(element).attr('href');
      }
    })
  }
</script>
@endsection 


