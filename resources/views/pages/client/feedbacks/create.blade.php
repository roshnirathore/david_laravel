@extends('templates.main',['pageTitle'=>'Create','rootPage'=>'Feedbacks'])

@section('content')
<div class="row"> 
    <div class="col-lg-12">
        <div class="card card-outline-info">
            <div class="card-header">
                <h4 class="m-b-0 text-white">Your Feedback</h4>
            </div>
            <div class="card-body card-body-form">
                <form class="form-horizontal form-material" method="POST" action="{{ route('client_feedback.save_feedback') }}">
                @csrf
                <div class="form-group row">
                    <label for="feedback" class="col-sm-3 control-label ">Feedback</label>
                    <div class="col-sm-9">
                        <div class="input-group"> 
                            <div class="form-check">
                                <label class="custom-control custom-radio normal-radio rad">
                                    <input id="like" name="radio" type="radio" checked class="custom-control-input" value="1">
                                    <i class="mdi mdi-thumb-up-outline custom-control-description"></i>
                               </label>
                                <label class="custom-control custom-radio normal-radio rad">
                                    <input id="unlike" name="radio" type="radio" class="custom-control-input" value="0">
                                    <i class="mdi mdi-thumb-down-outline custom-control-description"></i>
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
               
                
                <div class="form-group text-center m-t-20">
                    <div class="col-xs-12">
                        <button class="btn btn-info btn-lg btn-block text-uppercase waves-effect waves-light" type="submit">  {{ __('Create') }}</button>
                    </div>
                </div>
                <div class="form-group m-b-0">
                </div>
            </form>
            </div>
        </div>
    </div>
</div>
@endsection
<style type="text/css">
    
</style>