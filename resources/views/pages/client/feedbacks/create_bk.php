@extends('templates.main',['pageTitle'=>'Create','rootPage'=>'Feedbacks'])

@section('content')
<div class="row">
    <div class="col-lg-12">
        <div class="card card-outline-info">
            <div class="card-header">
                <h4 class="m-b-0 text-white">Your Feedback</h4>
            </div>
            <div class="card-body">
                <form class="form-horizontal form-material" method="POST" action="{{ route('feedback.save',[$store_id,$unit_id]) }}">
                @csrf
                <div class="form-group row">
                    <label for="feedback" class="col-sm-3 control-label ">Feedback</label>
                    <div class="col-sm-9">
                        <div class="input-group">
                            <div class="form-check">
                                <label class="custom-control custom-radio normal-radio rad">
                                    <input id="like" name="radio" type="radio" checked class="custom-control-input" value="1">
                                    <i class="mdi mdi-thumb-up-outline custom-control-description"></i>
                               </label>
                                <label class="custom-control custom-radio normal-radio rad">
                                    <input id="unlike" name="radio" type="radio" class="custom-control-input" value="0">
                                    <i class="mdi mdi-thumb-down-outline custom-control-description"></i>
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
               
                
                <div class="form-group text-center m-t-20">
                    <div class="col-xs-12">
                        <button class="btn btn-info btn-lg btn-block text-uppercase waves-effect waves-light" type="submit">  {{ __('Create') }}</button>
                    </div>
                </div>
                <div class="form-group m-b-0">
                </div>
            </form>
            </div>
        </div>
    </div>
</div>
@endsection
<style type="text/css">
    .normal-radio input[type=radio] {
    opacity: 1;
    position: static;
}
.rad,
.ckb{
  cursor: pointer;
  user-select: none;
  -webkit-user-select: none;
  -webkit-touch-callout: none;
}
.rad > input,
.ckb > input{ /* HIDE ORG RADIO & CHECKBOX */
  visibility: hidden;
  position: absolute;
}
/* RADIO & CHECKBOX STYLES */
.rad > i,
.ckb > i{     /* DEFAULT <i> STYLE */
  display: inline-block;
  vertical-align: middle;
  transition: 0.2s;
}
/* CHECKBOX OVERWRITE STYLES */
.ckb > i {
  width: 25px;
  border-radius: 3px;
}
/*.rad:hover > i{ 
  box-shadow: inset 0 0 0 3px #fff;
  background: gray;
}*/
.rad > input:checked + i{ /* (RADIO CHECKED) <i> STYLE */
  box-shadow: none;
  background: #f1eeea;
}
.rad i {
    font-size: 16px;
    padding: 15px;
    border: solid 1px #ddd;
    font-style: normal;
}
label.rad {
    display: block;
    float: left;
    width: 50%;
    margin: 0px 0px 0px -1px;
    vertical-align: middle;
}
.rad i {
    padding: 4px;
    border-radius: 0;
    background: #f5ece8;
    border: solid 1px #3688e5;
    transition: all 0.4s ease-in-out;
    -webkit-transition: all 0.4s ease-in-out;
    -moz-transition: all 0.4s ease-in-out;
    margin: 0px;
    position: relative;
    opacity: 1;
    display: block;
    text-align: center;
    height: auto;
    font-size: 30px;
    color: #3688e5;
}

.rad > input:checked + i {
    background: #3688e5;
    opacity: 1;
    color: #fff;
} 
</style>