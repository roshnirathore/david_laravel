<?php
namespace App\Helpers;

use Auth;
use App\Models\User;
use App\Models\Feedback;

class FeedbackHelper{

	public function admin(){
		$user_timezone =  Auth::user()->timezone; 
	    $latest_fb= Feedback::orderBy('global_feedback_date', 'DESC')->first();

	    if($latest_fb){
	    	$fb_date = date("Y-m-d H:i:s", strtotime($latest_fb->global_feedback_date));
	    	$last_syn =$this->user_timezone($user_timezone,$fb_date);
	    	return $last_syn;
	    }else{
	    	$date = $this->utc_date();
	    	$last_syn =$this->user_timezone($user_timezone,$date);
            return $last_syn;
	    	
	    }
	}
	public function user(){
		$client_id =  Auth::user()->id; 
		$user_timezone =  Auth::user()->timezone; 
		//$latest_fb = Feedback::where('client_id',$client_id)->latest()->first();
		$latest_fb = Feedback::where('client_id',$client_id)->orderBy('global_feedback_date', 'DESC')->first();
		
	    if($latest_fb){
	    	$fb_date = date("Y-m-d H:i:s", strtotime($latest_fb->global_feedback_date));
	    	$last_syn =$this->user_timezone($user_timezone,$fb_date);
	    	return $last_syn;
	    }else{
	    	
	    	$date = $this->utc_date();
    		$last_syn =$this->user_timezone($user_timezone,$date);
            return $last_syn;
	    	
	    }
	}	

	public function user_timezone($timezone ='',$date=''){
        $utc_date = \DateTime::createFromFormat(
              'Y-m-d H:i:s',
              $date,
              new \DateTimeZone('UTC')
          );

          $acst_date = clone $utc_date; 
          $acst_date->setTimeZone(new \DateTimeZone($timezone));
          $global_feedback_date = $acst_date->format('m-d-Y H:i a');
          return $global_feedback_date;
	}

	public function utc_date(){
		$date = date("d-m-Y H:i:s"); 
	    $utc_date = \DateTime::createFromFormat(
              'd-m-Y H:i:s',
            $date,
            new \DateTimeZone(date_default_timezone_get()) //Pacific/Honolulu
        );

        $acst_date = clone $utc_date; 
        $acst_date->setTimeZone(new \DateTimeZone('UTC'));
        $date = $acst_date->format('Y-m-d H:i:s');
        return $date;
	}
}