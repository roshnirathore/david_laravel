<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class Support extends Mailable
{
    use Queueable, SerializesModels;
      public $email_content;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($email_content="")
    {
         $this->email_content = $email_content;
    }

    /** 
     * Build the message.
     *
     * @return $this 
     */
    public function build()
    {
 
       // return $this->from($this->email_content['email'], $this->email_content['name'])->subject($this->email_content['subject'])->view('emails.support');
      //  return $this->from('support@digitalbizfarm.com','FocusTrax')->subject('FocusTrax Client Support Request')->view('emails.support');
        return $this->from('support@focustrax.com','FocusTrax')->subject($this->email_content['subject'])->view('emails.support');
    }
}
