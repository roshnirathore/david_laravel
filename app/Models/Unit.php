<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Unit extends Model
{
    protected $fillable = [
        'name',
        'store_id',
        'client_id',
        'added_by',
        'modified_by',
        'timezone'

     ];
    public function user()
    {
        return $this->belongsTo('App\Models\User', 'modified_by');
    }

    
    /*public function unit_list()
    {
     return $this->hasMany('App\Models\Store','store_id ,name,added_by ,modified_by, created_at,updated_at,id');
    }*/

    public function unitApi() 
    {
        return $this->belongsTo('App\Models\Store','store_id');
    }
}

