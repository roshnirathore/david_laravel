<?php

namespace App\Models;

use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{   

    use HasApiTokens,Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'first_name',
        'last_name',
        'email',
        'password',
        'activated',
        'token',
        'signup_ip_address',
        'signup_confirmation_ip_address',
        'signup_sm_ip_address',
        'admin_ip_address',
        'updated_ip_address',
        'deleted_ip_address',
        'profile_image',
        'company_name',
        'address_line_1',
        'address_line_2',
        'city',
        'state',
        'country',
        'zip_code',
        'phone_no',
        'alter_phone_no',
        'timezone',
        'phone_number_extension',
        ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    
    public function hasRole(String $roleName)
    {
        return $this->roles()->where('name', $roleName)->exists(); 
    }

    public function roles()
    {
        return $this->belongsToMany('App\Models\Role');
    }
    public function addRole(String $roleName)
    {
        $role = Role::where('name', $roleName)->first();
        //print_r($role);
        if ($role) $this->roles()->save($role);
    }

    public function stores()
    {
        return $this->hasMany('App\Models\Store','client_id');
    }

    //Get modified user name
    public function units()
    {
     return $this->hasMany('App\Models\Unit','first_name,id');
    }
    public function apps()
    {
     return $this->hasMany('App\Models\App','first_name,id');
    }

    public function modified_user()
    {
      return $this->hasMany('App\Models\store','first_name,id');
    }
    public function installs()
    {
        return $this->hasMany('App\Models\Installation','client_id');
    }
}
