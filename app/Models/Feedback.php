<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Feedback extends Model
{
     protected $fillable = [
        'name',
        'store_id',
        'unit_id',
        'client_id',
        'feedback_date',
        'global_feedback_date'

     ];
     protected $table = 'feedbacks';

    
}
