<?php 

namespace App\Models;

use Illuminate\Database\Eloquent\Relations\Pivot;

class UserRole extends Pivot
{
	protected $fillable = [
        'role_id',
        'user_id',
     ];
     protected $table = 'role_user';
   
}
