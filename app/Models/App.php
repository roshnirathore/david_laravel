<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class App extends Model
{ 
    protected $fillable = [
        'version',
		'description',
		'file',
		'is_live',
		'added_by',
		'modified_by'

     ];
     protected $table = 'apps';

     public function user()
    {
        return $this->belongsTo('App\Models\User', 'modified_by');
    }
}
