<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Store extends Model
{
     /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'client_id',
        'added_by',
        'modified_by',

     ];

    public function hasOwner(String $roleName)
    {
        return $this->owner()->where('name', $roleName)->exists();
    }

    public function owner()
    {
        return $this->belongsTo('App\Models\User');
    }

    public function assignedUser()
    {
        return $this->belongsTo('App\Models\User','client_id');
    }
    /*public function units()
    {
        return $this->hasMany('App\Models\Units','store_id');
    }*/

    public function user()
    {
        return $this->belongsTo('App\Models\User','modified_by'); 
    }


    public function units() 
    {
        return $this->hasMany('App\Models\Unit','store_id');
    }

}
