<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    public const ADMIN = 'Admin';
    public const CLIENT = 'Client';
    public const STORE = 'Store';

    public function users()
    {
        return $this->belongsToMany('App\Models\User')->using('App\Models\UserRole');
    }
} 
