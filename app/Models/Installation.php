<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Installation extends Model
{
    protected $fillable = [
        'device_id',
		'client_id',
		'app_version'

     ];
     protected $table = 'installations';

     public function assignedUser()
    {
        return $this->belongsTo('App\Models\User','client_id'); 
    }
}
