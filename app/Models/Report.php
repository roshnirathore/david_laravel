<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Report extends Model
{
    protected $fillable = [
        'report_name',
		'filter_param',
		'added_by'

     ];
     protected $table = 'reports';
}
