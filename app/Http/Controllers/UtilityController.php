<?php

namespace App\Http\Controllers;

use Auth;
use Session;
use Carbon\Carbon;
use App\Traits\CaptureIpTrait;
use App\Models\Role;
use App\Models\User;
use App\Models\Store;
use App\Models\UserRole;
use App\Models\Feedback;
use Illuminate\Http\Request;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;

class UtilityController extends Controller
{
	public static function index(){

	  $client_id = Auth::user()->id;
	  //check user role
      $user_role_id = UserRole::where('user_id',$client_id)->pluck('role_id');
      $role = Role::where('id',$user_role_id)->pluck('slug');
      $user_role = $role[0];

      if($user_role =='client'){

          $today_report_data = Feedback::where('client_id', $client_id)->whereDate('feedback_date','=', Carbon::now())->get();

          $days7_report_data = Feedback::where('client_id', $client_id)->whereDate('feedback_date','>', Carbon::now()->subDays(7))->get();

          $days30_report_data = Feedback::where('client_id', $client_id)->whereDate('feedback_date','>', Carbon::now()->subDays(30))->get();

          $days365_report_data = Feedback::where('client_id', $client_id)->whereDate('feedback_date','>', Carbon::now()->subDays(365))->get();

          //$stores = Store::where('client_id', $client_id)->get();

          $data = array( 
          	'today_report_data'=> $today_report_data,
          	'days7_report_data'=> $days7_report_data,
          	'days30_report_data'=> $days30_report_data,
          	'days365_report_data'=> $days365_report_data,
          		//'stores'=> $stores,
          );
          
      }else{
          $today_report_data = Feedback::whereDate('feedback_date','=', Carbon::now())->get();

          $days7_report_data = Feedback::whereDate('feedback_date','>', Carbon::now()->subDays(7))->get();

          $days30_report_data = Feedback::whereDate('feedback_date','>', Carbon::now()->subDays(30))->get();

          $days365_report_data = Feedback::whereDate('feedback_date','>', Carbon::now()->subDays(365))->get();

          //$stores = Store::where('client_id', $client_id)->get();

          $data = array(
          	'today_report_data'=> $today_report_data,
          	'days7_report_data'=> $days7_report_data,
          	'days30_report_data'=> $days30_report_data,
          	'days365_report_data'=> $days365_report_data,
          		//'stores'=> $stores,
          );
      }
      return $data;
	}

 	public function listPHPInfo(){
 	phpinfo();
 }
}
