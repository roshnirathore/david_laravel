<?php

namespace App\Http\Controllers;

use Auth;
use Session;
use App\Traits\CaptureIpTrait;
use App\Models\Store;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;

class StoreManagementController extends Controller
{
      /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($client_id) 
    {
       // print_r($client_id);
       // $stores = Store::paginate(20);
        $users = User::findOrFail($client_id);
        $stores = Store::with('user')->where('client_id',$client_id)->paginate(20);
        

        return View('pages.admin.store.list', compact('stores', 'users','client_id'));
    }

      /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($client_id)
    {
        $users = User::all();

        $data = [
            'users' => $users,
            'client_id' => $client_id,
        ];

        return view('pages.admin.store.create')->with($data);
    }

     /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function save(Request $request,$client_id)
    {
       $validator = Validator::make($request->all(),
            [
                'name'            => 'required',
            ],
            [
                'name.required' => "Name is required",
            ]
        )->validate();

       
        $store = Store::create([
                'name'          => $request->input('name'),
                'client_id' => $client_id,
                'added_by'        => Auth::user()->id,
                'modified_by'        => Auth::user()->id,
            ]);            
        $store->save();
        //return redirect('stores')->with('success', "Store has been added!");     
        return redirect('client/'.$client_id.'/stores')->with('message', "Store has been added!");   
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($client_id, $id)
    {
        $store = Store::where('client_id',$client_id)->findOrFail($id);
        $users = User::all();
        $data = [
            'users'        => $users,
            'store'        => $store,
            'client_id'    => $client_id
        ];
        return view('pages.admin.store.edit')->with($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int                      $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $client_id, $id)
    {
        $store = Store::find($id);
        $validator = Validator::make($request->all(),  
            [
                'name'            => 'required'
            ],
            [
                'name.required' => "Name is required"
            ])->validate();

        $store->name = $request->input('name');
        $store->client_id = $client_id;
        $store->modified_by = Auth::user()->id;
        $store->save();
       // return back()->with('success',"Store has been updated");
        return redirect('client/'.$client_id.'/stores')->with('success', "$store->name Store has been updated");
        
    }

      /**
     * Delete the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function delete($client_id,$id) 
    {
        $store = Store::findOrFail($id);

        if ($store->added_by == Auth::user()->id)
        {
            Store::destroy($store->id);
            return redirect()->back()->with('success', "$store->name has been deleted");
        }
        return back()->with('error',"You are not authorize to delete this store");  
    }

}
