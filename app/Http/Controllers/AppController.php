<?php 

namespace App\Http\Controllers;

use Auth;
use Session;
use App\Traits\CaptureIpTrait;
use App\Models\Role;
use App\Models\User;
use App\Models\Store;
use App\Models\App;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;

class AppController extends Controller
{
    public function index(){
    	$apps = App::with('user')->paginate(20);
    	return view('pages.admin.app.list',compact('apps')); 
    }

    public function create(){
         
    	return view('pages.admin.app.create',compact('timezone'));
    }

   	public function save(Request $request){

        $validator = $request->validate( 
            [
               'version'  => 'required|unique:apps',
               // 'uploaded_file'     => 'mimes:apk,application/vnd.android.package-archive'
                'uploaded_file'     => 'mimes:zip',
            ],
            [
                'uploaded_file.required' => "File upload required",
                'uploaded_file.mimes' => "Only apk file is allow",
                'version.required' => "App Version is required",
            ]
        );
  
   		//Image upload
        $file = $request->file('uploaded_file');

        if($file){
            $extension = $file->getClientOriginalExtension();
            $fileName  = $file->getFilename().'.'.$extension;
            Storage::disk('public')->put($fileName ,  File::get($file));
        }else{
            $fileName = '';
        }


   		 $app = App::create([  
            'version'     => $request->input('version'),
            'description' => $request->input('description'),
            'file'        => $fileName,
            'is_live'     => $request->input('radio'),
            'added_by'    => Auth::user()->id,
            'modified_by' => Auth::user()->id,
          ]);
        
        $app->save();
        
        if($request->input('radio') == 'true'){
            $app = App::where('id', '!=' ,$app->id)->update(['is_live' => 'false']);    
        }

        return redirect('apps')->with('success', "App has been added!");
   	}

   	public function edit($id)
    { 
        $app = App::findOrFail($id);
        $data = [
            'app' => $app,
            //'filepath'=>storage_path('uploads').'/'
            'filepath'=> 'dashboard.focustrax.com/uploads/',
        ];
        return view('pages.admin.app.edit')->with($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int                      $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
         $app = App::findOrFail($id);
         $file = $request->file('uploaded_file');

        if($file){
            $extension = $file->getClientOriginalExtension();
            $fileName  = $file->getFilename().'.'.$extension;
            Storage::disk('public')->put($fileName ,  File::get($file));
            $app->file = $fileName;
        }
   
        $app->version = $request->input('version');
        $app->description = $request->input('description');
        $app->is_live = $request->input('radio');
        $app->modified_by = Auth::user()->id;

        $app->save();
        return redirect('apps')->with('success', "App has been updated");
        
    }

    /**
     * Delete the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        $app = App::findOrFail($id);
        App::destroy($app->id);
        return redirect('apps')->with('success', 'App Deleted correctly!!!'); 
    }
}
