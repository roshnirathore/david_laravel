<?php

namespace App\Http\Controllers;
use Auth;
use App\Models\Role;
use Illuminate\Http\Request;

class UserController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user();
        if ($user->hasRole(Role::ADMIN)) {
            return view('pages.admin.home');
        }
        if ($user->hasRole(Role::CLIENT)) {
            return view('pages.client.home');
        }
        if ($user->hasRole(Role::STORE)) {
            return view('pages.store.home');
        }
    }
}
