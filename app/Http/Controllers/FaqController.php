<?php

namespace App\Http\Controllers;

use Auth;
use Session;
use Illuminate\Http\Request;

class FaqController extends Controller
{
	public function __construct()
  {
    $this->middleware('auth');
  } 

    public function index(){
    	return View('faq');
    }
}
