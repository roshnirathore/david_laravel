<?php

namespace App\Http\Controllers;

use Auth;
use Session;
use DataTables;
use App\Traits\CaptureIpTrait;
use App\Models\Role;
use App\Models\User;
use App\Models\Store;
use App\Models\Installation;
use Illuminate\Http\Request;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
 


class ClientManagementController extends Controller 
{ 
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() 
    {
      
        $clients = User::whereHas('roles', function($role) {
            $role->where('name', '=', Role::CLIENT);
        })->paginate(20);

        $roles = Role::all();
        return View('pages.admin.list-clients', compact('clients', 'roles'));
    }

      /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $roles = Role::all();
        $data = [
            'roles' => $roles,
        ];

        return view('pages.admin.client-create')->with($data);
    }

     /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function save(Request $request)
    { 
        $validator = Validator::make($request->all(),
            [
                'first_name'            => 'required',
                'last_name'             => 'required',
                'email'                 => 'required|max:255|unique:users|regex:/(.*)\./i|email',
                'password'              => 'required|min:6|max:20|confirmed',
                'password_confirmation' => 'required|same:password',
            ],
            [
                'first_name.required' => "First name is required",
                'last_name.required'  => "Last name is required",
                'email.required'      => "Email is required",
                'email.email'         => "Please enter a valid email address",
                'password.required'   => "Please enter a password",
                'password.min'        => "minimum password length is  6",
                'password.max'        => "maximum password length is 20"
            ]
        )->validate();

       
        $ipAddress = new CaptureIpTrait();
        $client = User::create([
                'name'             => $request->input('email'),
                'first_name'       => $request->input('first_name'),
                'last_name'        => $request->input('last_name'),
                'email'            => $request->input('email'),
                'password'         => Hash::make($request->input('password')),
                'admin_ip_address' => $ipAddress->getClientIp(),
                'activated'        => 1,
                'token'            => str_random(64)
            ]);            
        $client->save();
        $client->addRole(Role::CLIENT); 
        return redirect('clients')->with('success', "Client has been added!");        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $client = User::findOrFail($id);
        $data = [
            'client'        => $client
        ];
        return view('pages.admin.client-edit')->with($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int                      $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $currentUser = Auth::user();
        $user = User::find($id);
        $ipAddress = new CaptureIpTrait();
        $validator = Validator::make($request->all(), [
                'first_name'     => 'required|max:255',
                'last_name'     => 'required|max:255',

            ])->validate();

        $user->first_name = $request->input('first_name');
        $user->last_name = $request->input('last_name');
        $user->password = Hash::make($request->input('password'));
        $user->updated_ip_address = $ipAddress->getClientIp();
        $user->save();
        // Session::push('success', 'Client has been updated');
       // return back()->with('success',"Client has been updated");
        return redirect('clients')->with('success', "Client has been updated");
        
    }

    /**
     * Delete the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        $client = User::findOrFail($id);
        if (!$client->hasRole(Role::ADMIN))
        {
            User::destroy($client->id);
            return redirect()->back()->with('success', "$client->first_name has been deleted");
        }
        return back()->with('error',"You can not delete admin user");
    }
 
    public function search(Request $request){

        if($request->ajax()){
            $output='';
            $clients = User::whereHas('roles', function($role) { $role->where('name', '=', Role::CLIENT); })->where('email', 'LIKE','%'.$request->search.'%')->orWhere('first_name', 'LIKE','%'.$request->search.'%')->where('last_name', 'LIKE','%'.$request->search.'%')->get();

             $client = $clients->toArray();
            if ($client) {
                $iteration = 1;

                foreach ($client as $key => $value) {
                    $created_at = date("m-d-Y H:i", strtotime($value['created_at']));
                    $updated_at = date("m-d-Y H:i", strtotime($value['updated_at']));
                   $output.='<tr>'.
                   '<td>'.$iteration++.'</td>'.
                    '<td>'.$value['first_name'].' '.$value['last_name'].'</td>'.
                    '<td>'.$value['email'].'</td>'.
                    '<td><a href="client/5/stores">'.count($value['stores']).'</td>'.
                    '<td>3</td>'.
                    '<td>'.$created_at.'</td>'.
                    '<td>'.$updated_at.'</td>'.
                    '<td class="text-nowrap"><a href="clients/edit/'.$value['id'].'" data-toggle="tooltip" data-original-title="Edit" class="btn btn-secondary btn-circle"> <i class="fa fa-pencil-alt"></i> </a>
                    <a href="/clients/delete/'.$value['id'].'" data-toggle="tooltip" data-original-title="Delete" class="btn btn-danger btn-circle" onclick="comfrimDelet(event,this)">  <i class="fa fa-times"></i> </a>
                    </td>'.
                    '</tr>';

                }
                return Response($output);
            }
            //echo json_encode($clients);
        }
    }
}


 