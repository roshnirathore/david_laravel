<?php

namespace App\Http\Controllers;

use Auth;
use Session;
use DataTables;
use App\Traits\CaptureIpTrait;
use App\Models\Role;
use App\Models\User;
use App\Models\Store;
use App\Models\Installation;
use Illuminate\Http\Request;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
 


class ClientManagementController extends Controller 
{  
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request) 
    {
         
        $clients = User::whereHas('roles', function($role) {
            $role->where('name', '=', Role::CLIENT);
        })->paginate(20);
        $roles = Role::all();
        return View('pages.admin.list-clients', compact('clients', 'roles'));
    }

      /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $roles = Role::all();
        $timezone_list = $this->timezone_list(); 
        $data = [
            'roles' => $roles,
            'timezone_list' => $timezone_list,
        ];

        return view('pages.admin.client-create')->with($data);
    }

     /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function save(Request $request)
    {  
        $validator = Validator::make($request->all(),
            [
                'first_name'            => 'required',
                'last_name'             => 'required',
                'company_name'          => 'required',
                'email'                 => 'required|max:255|unique:users|regex:/(.*)\./i|email',
                'password'              => 'required|min:6|max:20|same:password_confirmation',
                'password_confirmation' => 'required',
                'address_line_1'        => 'required',
                'city'                  => 'required',
                'state'                 => 'required',
                'country'               => 'required',
                'zip_code'              => 'required',
                'phone_no'              => 'required',
                'timezone'              => 'required',
            ],
            [
                'first_name.required'               => "First name is required",
                'last_name.required'                => "Last name is required",
                'company_name.required'             => "Comapny name is required",
                'email.required'                    => "Email is required",
                'email.email'                       => "Please enter a valid email address",
                'password.required'                 => "Please enter a password",
                'password.min'                      => "minimum password length is  6",
                'password.max'                      => "maximum password length is 20",
                'address_line_1.required'           => "Address is required",
                'city.required'                     => "City is required",
                'state.required'                    => "State is required",
                'country.required'                  => "Country is required",
                'zip_code.required'                 => "Zip Code is required",
                'phone_no.required'                 => "Phone Number is required",
                'timezone.required'                 => "Timezone is required",
            ]
        )->validate();

        if (filter_var( $request->input('email'), FILTER_VALIDATE_EMAIL)) {
            $ipAddress = new CaptureIpTrait();
            $client = User::create([
                    'name'             => $request->input('email'),
                    'first_name'       => $request->input('first_name'),
                    'last_name'        => $request->input('last_name'),
                    'email'            => $request->input('email'),
                    'password'         => Hash::make($request->input('password')),
                    'company_name'     => $request->input('company_name'),
                    'address_line_1'   => $request->input('address_line_1'),
                    'address_line_2'   => $request->input('address_line_2'),
                    'city'             => $request->input('city'),
                    'state'            => $request->input('state'),
                    'country'          => $request->input('country'),
                    'zip_code'         => $request->input('zip_code'),
                    'phone_no'         => $request->input('phone_no'),
                    'phone_number_extension'=> $request->input('phone_number_extension'),
                    'alter_phone_no'   => $request->input('alter_phone_no'),
                    'timezone'         => $request->input('timezone'),
                    'admin_ip_address' => $ipAddress->getClientIp(),
                    'activated'        => 1,
                    'token'            => str_random(64),
                ]);            
            $client->save();
            $client->addRole(Role::CLIENT); 
        return redirect('clients')->with('success', "Client has been added!");   
        }else{
            return redirect('clients/create')->with('error', "Email address is not valid!"); 

        }
              
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $client = User::findOrFail($id);
        $timezone_list = $this->timezone_list(); 
        $data = [
            'client'        => $client,
            'timezone_list'        => $timezone_list,
        ];
        return view('pages.admin.client-edit')->with($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int                      $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $currentUser = Auth::user();
        $user = User::find($id);
        $ipAddress = new CaptureIpTrait();
        $validator = Validator::make($request->all(), [
                'first_name'            => 'required|max:255',
                'last_name'             => 'required|max:255',
                'company_name'          => 'required',
                'address_line_1'        => 'required',
                'city'                  => 'required',
                'state'                 => 'required',
                'country'               => 'required',
                'zip_code'              => 'required',
                'phone_no'              => 'required',
                'timezone'              => 'required',

            ])->validate();

        $user->first_name         = $request->input('first_name');
        $user->last_name          = $request->input('last_name');
        $user->company_name       = $request->input('company_name');
        $user->password           = Hash::make($request->input('password'));
        $user->updated_ip_address = $ipAddress->getClientIp();
        $user->address_line_1     = $request->input('address_line_1');
        $user->address_line_2     = $request->input('address_line_2');
        $user->city               = $request->input('city');
        $user->state              = $request->input('state');
        $user->country            = $request->input('country');
        $user->zip_code           = $request->input('zip_code');
        $user->phone_no           = $request->input('phone_no');
        $user->phone_number_extension= $request->input('phone_number_extension');
        $user->alter_phone_no     = $request->input('alter_phone_no');
        $user->timezone           = $request->input('timezone');
        $user->activated           = $request->input('user_status');
        $user->save();
        // Session::push('success', 'Client has been updated');
       // return back()->with('success',"Client has been updated");
        return redirect('clients')->with('success', "Client has been updated");
        
    }

    /**
     * Delete the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        $client = User::findOrFail($id);
        if (!$client->hasRole(Role::ADMIN))
        {
            User::destroy($client->id);
            return redirect()->back()->with('success', "$client->first_name has been deleted"); 
        }
        return back()->with('error',"You can not delete admin user");
    }
    
    public function search(Request $request){

        if($request->ajax()){
            $output='';
            $clients = User::whereHas('roles', function($role) { $role->where('name', '=', Role::CLIENT); })->Where('first_name', 'LIKE','%'.$request->search.'%')->orwhere('email', 'LIKE','%'.$request->search.'%')->orwhere('last_name', 'LIKE','%'.$request->search.'%')->get();

             $client = $clients->toArray();
            if ($client) {
                $iteration = 1;

                foreach ($client as $key => $value) {
                     $role = User::findOrFail($value['id']);
                     if (!$role->hasRole(Role::ADMIN)) {
                        if ($value['activated']) {
                           $status = 'Active';
                        }else{
                             $status = 'Suspended';
                        }

                        $stores = Store::where('client_id',$value['id'])->count();
                        $install = Installation::where('client_id',$value['id'])->count();
                        $created_at = date("m-d-Y H:i", strtotime($value['created_at']));
                        $updated_at = date("m-d-Y H:i", strtotime($value['updated_at']));
                       $output.='<tr>'.
                       '<td>'.$iteration++.'</td>'.
                        '<td>'.$value['first_name'].' '.$value['last_name'].'</td>'.
                        '<td>'.$value['email'].'</td>'.
                        '<td>'.$value['company_name'].'</td>'.
                        '<td><a href="client/'.$value['id'].'/stores">'.$stores.'</td>'.
                        '<td>'.$install.'</td>'.
                        '<td>'.$status.'</td>'.
                        '<td>'.$created_at.'</td>'.
                        '<td>'.$updated_at.'</td>'.
                        '<td class="text-nowrap"><a href="clients/edit/'.$value['id'].'" data-toggle="tooltip" data-original-title="Edit" class="btn btn-secondary btn-circle"> <i class="fa fa-pencil-alt"></i> </a>
                        <a href="/clients/delete/'.$value['id'].'" data-toggle="tooltip" data-original-title="Delete" class="btn btn-danger btn-circle" onclick="comfrimDelet(event,this)">  <i class="fa fa-times"></i> </a>
                        </td>'.
                        '</tr>';
                    }
                }
                return Response($output);

            }
            //echo json_encode($clients); 
        }
    }

    public function timezone_list(){
       $timezone = array(); 
         $timestamp = time();
            foreach(timezone_identifiers_list(\DateTimeZone::PER_COUNTRY, 'US') as $key => $t) {
                date_default_timezone_set($t);
                if($t == 'America/New_York' || $t == 'America/Chicago' || $t == 'America/Denver' || $t == 'America/Phoenix' || $t == 'America/Los_Angeles'){
                  $timezone[$key]['zone'] = $t;
                  if($t == 'America/New_York'){
                    $show_zone = substr_replace("America/New_York","EST New York",0);
                  }else if($t == 'America/Chicago'){
                    $show_zone = substr_replace("America/Chicago","CST Chicago",0);
                  }else if($t == 'America/Denver'){
                    $show_zone = substr_replace("America/Denver","MST Denver",0);
                  }else if($t == 'America/Phoenix'){
                    $show_zone = substr_replace("America/Phoenix","MST Phoenix",0);
                  }else if($t == 'America/Los_Angeles'){
                    $show_zone = substr_replace("America/Los_Angeles","PST Los Angeles",0);
                  }
                  $timezone[$key]['show_zone'] = $show_zone;
                  $timezone[$key]['GMT_difference'] =  date('P', $timestamp);
                }
            }
         $timezone = collect($timezone)->sortBy('GMT_difference');
         return $timezone; 
    }


}


 