<?php

namespace App\Http\Controllers\Client;

use Auth;
use Session;
use Carbon\Carbon;
use App\Traits\CaptureIpTrait;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Pagination\Paginator;
use App\Models\User;
use App\Models\Store;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;


class StoreController extends Controller
{
   public function index(){
 
   	$client_id =  Auth::user()->id; 
    
    $stores = Store::with('user')->where('client_id', $client_id)->paginate(20); 

    return View('pages.client.store.list', compact('stores','client_id'));
   }

   public function edit($store_id){
    $client_id =  Auth::user()->id;
   	$store = Store::where('client_id', $client_id)->findOrFail($store_id);
   	$data = [
            'store'        => $store
        ];
        return view('pages.client.store.edit')->with($data);
   }

   public function update(Request $request, $store_id){

   		$store = Store::find($store_id);
   		$Validator = Validator::make($request->all(),
   			[
   				'name' => 'required'
   			],
   			[
   				'name.required' => 'Store Name is required'
   			])->validate();

   		$store->name = $request->input('name');
   		$store->modified_by = Auth::user()->id;
   		$store->save();

   		return redirect('/stores')->with('success', "$store->name Store has been updated");
   }
}
