<?php

namespace App\Http\Controllers\Client;

use Auth;
use Session;
use App\Traits\CaptureIpTrait;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Pagination\Paginator;
use App\Models\User;
use App\Models\Store;
use App\Models\Feedback;
use App\Models\Role;
use App\Models\UserRole;
use App\Models\Report;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;

class FeedbackController extends Controller
{
    public function index(){
      $client_id =  Auth::user()->id;
      $user_role = $this->role_report(); //check user role

      if($user_role =='client'){
          $report_data = Feedback::where('client_id', $client_id)->get();
          $stores = Store::where('client_id', $client_id)->get();
      }else{
          $report_data = Feedback::all();
          $stores = Store::all();
      }

      
      return View('pages.client.feedbacks.list',compact('stores','report_data'));
    }

    public function filter_data(Request $request){ 

       $client_id =  Auth::user()->id;
      //Get Data from Feedback table
      $store_id = $request->input('stores');
      $from_date = $request->input('from_date');
      $to_date  = $request->input('to_date');
      $from_time = $request->input('from_time');
      $to_time = $request->input('to_time');
      
      $fiter_param = array('store_id'=> $store_id,
                          'from_date'=>$from_date,
                          'to_date'=>$to_date,
                          'from_time'=>$from_time,
                          'to_time'=>$to_time
                        );
      $json_fiter_param = json_encode($fiter_param);
      $user_role = $this->role_report(); //check user role

      //check into databse  by filter_param
    
      if($user_role =='client'){ 

        $query = Feedback::where('client_id', $client_id);

          if ($store_id != NULL) {
            $query->whereIn('store_id', $store_id);
          } 
          if ($from_date != NULL) {
             $from_date = date("Y-m-d", strtotime($from_date));
             $query->whereDate('feedback_date','>=', $from_date);
            
          } 
          if ($to_date != NULL) {
            $to_date  = date("Y-m-d", strtotime($to_date));
             $query->whereDate('feedback_date','<=',$to_date);
          } 
          if ($from_time != NULL) {
             $query->whereTime('feedback_date','>=',$from_time);
          } 
          if ($to_time != NULL) {
             $query->whereTime('feedback_date','<=',$to_time);
          } 
          $report_data = $query->get();
          $stores = Store::where('client_id', $client_id)->get();

      }else{

          $query = Feedback::all();

          if ($store_id != NULL) {
           $query->whereIn('store_id', $store_id);
          } 
          if ($from_date != NULL) {
            $from_date = date("Y-m-d", strtotime($from_date));
             $query->whereDate('feedback_date','>=', $from_date);
          } 
          if ($to_date != NULL) {
             $to_date  = date("Y-m-d", strtotime($to_date));
             $query->whereDate('feedback_date','<=',$to_date);
          } 
          if ($from_time != NULL) {
             $query->whereTime('feedback_date','>=',$from_time);
          } 
          if ($to_time != NULL) {
             $query->whereTime('feedback_date','<=',$to_time);
          }

          $report_data = $query->get();
          $stores = Store::all();
      }
      return View('pages.client.feedbacks.list',compact('report_data','stores','json_fiter_param'));
    }

    public function save(Request $request){

         //insert into database
        $report = Report::create([  
            'report_name' => $request->input('report_name'),
            'filter_param' => $request->input('filter_param'),
            'added_by' => Auth::user()->id
                      ]);
        $report->save(); 
              
        return redirect('/reports')->with('success', "Location Added successfully");
       
    }

    public function report_list($report_id){
      $client_id =  Auth::user()->id;
      $query = Report::where('id',$report_id)->where('added_by',$client_id)->get(); 
      print_r($query);
      //return View('pages.client.feedbacks.save_report',compact('query'));
    }

    //For dummy data
   /* public function create()
   {
    return view('pages.client.feedbacks.create'); 
   }

   public function save(Request $request)
   {
        $feedback = Feedback::create([  
            'name' => $request->input('radio'),
            'store_id' => 4,
            'unit_id' => 7,
            'client_id'    => Auth::user()->id,

            'feedback_date' =>  now(),
          ]);
        $feedback->save();
      return view('pages.client.feedbacks.create');
   }*/

   //Check user role
    public function role_report(){
      $client_id =  Auth::user()->id;
      $user_role_id = UserRole::where('user_id',$client_id)->pluck('role_id');
      $role = Role::where('id',$user_role_id)->pluck('slug');
      $user_role = $role[0];
      return $user_role;
    }
}
