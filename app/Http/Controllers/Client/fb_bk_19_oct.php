<?php

namespace App\Http\Controllers\Client;

use Auth;
use Session;
use App\Traits\CaptureIpTrait;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Pagination\Paginator;
use App\Models\User;
use App\Models\Store;
use App\Models\Feedback;
use App\Models\Role;
use App\Models\UserRole;
use App\Models\Report;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;

class FeedbackController extends Controller 
{
   //For dummy data
    public function test (){
      $arr_fbDate = array();
      $weekDays = array('sunday','Monday' );
    
       $userfb = DB::table('feedbacks')->select(DB::raw('feedback_date as fbDate, DAYNAME(feedback_date) as nameOfDay'))->get(); 
        //print_r($userfb); dd('fg');
      for ($i=0; $i < count($userfb); $i++) { 

        $fbDate = $userfb[$i]->fbDate;
        $nameOfDay = $userfb[$i]->nameOfDay;

        if (in_array($nameOfDay, $weekDays)){
           array_push($arr_fbDate,$fbDate);    
        }
      } //for loop end
      $query = Feedback::whereIn('feedback_date', $arr_fbDate);
     $fb_data = $query->get();
     
    }
   //dummy
    public function create()
   {
    return view('pages.client.feedbacks.create'); 
   }

   public function save_feedback(Request $request)
   {
        $feedback = Feedback::create([  
            'name' => $request->input('radio'),
            'store_id' => 4,
            'unit_id' => 7,
            'client_id'    => Auth::user()->id,

            'feedback_date' =>  now(),
          ]);
        $feedback->save();
      return view('pages.client.feedbacks.create');
   }
}


//<?php

namespace App\Http\Controllers\Client;

use Auth;
use Session;
use Carbon\Carbon;
use App\Traits\CaptureIpTrait;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Pagination\Paginator;
use App\Models\User;
use App\Models\Store;
use App\Models\Feedback;
use App\Models\Role;
use App\Models\UserRole;
use App\Models\Report;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;

use Illuminate\Support\Facades\DB;

class FeedbackController extends Controller 
{   private $user;
{   private $client_id;
  public function __construct()
  {
   // parent::__construct();
    $this->middleware('auth');
  }
    public function index(Request $request){ 
      
      //if (Auth::check()){
        $client_id =  Auth::user()->id; 
        $user = Auth::user();

        if($request->has('fiter_report_btn')){
          $data = $this->filter_param_btn($request);
          $data['json_fiter_param']= json_encode($data['fiter_param']);
        } 
        else{

          if($user->hasRole(Role::CLIENT)){ 

            $report_data = Feedback::where('client_id', $client_id)->get();
            $stores = Store::where('client_id', $client_id)->get();
            $data = array(
              'report_data' => $report_data,
              'stores' => $stores,
              'weekDays' => 'false',
            );
          }elseif($user->hasRole(Role::ADMIN)){

            $users = User::all();
            $report_data = Feedback::all();
            $stores = Store::all();
            $data = array(
              'report_data' => $report_data,
              'stores' => $stores,
              'users' => $users,
              'weekDays' => 'false',
            );
           
            if($request->ajax()){
              if(!empty($request->client_id)){ 
                $stores_list = Store::whereIn('client_id',$request->client_id)->get();

                return response()->json(['success'=>$stores_list]);
              } else{
                $stores_list = Store::get();
                return response()->json(['success'=>$stores_list]);
              }
            } 
          } 
        }   
        return View('pages.client.feedbacks.list')->with($data);
      /*} //Auth check if end
      else{
        return redirect('/login');
      } */  
    }

    // filter report save into databse
    public function save(Request $request){

      $report_id = $request->input('report_id');
      $filter_param= $request->input('filter_param');
      $report_name = $request->input('report_name');
      $string = implode(',', $filter_param); //array data convert into string
     //udate into database 
      if ($report_id) {

        $report = Report::find($report_id);
        $report->report_name = $report_name;
        $report->filter_param = $string;
        $report->added_by = Auth::user()->id;
        $report->save(); 
              
        return redirect('/reports')->with('success', "Location Updated successfully");
      } else{   
        $report = Report::create([  
          'report_name' => $report_name,
          'filter_param' => $string,
          'added_by' => Auth::user()->id
        ]);
        $report->save();   
        return redirect('/reports')->with('success', "Location Added successfully");
      }
    }

    //save report page
    public function report_list(Request $request,$report_id){

      $client_id =  Auth::user()->id;
      $query = Report::where('id',$report_id)->where('added_by',$client_id)->get();
      if($request->has('fiter_report_btn')){

        foreach ($query as $row) {
           $report_name =  $row->report_name;
        }
        $data = $this->filter_param_btn($request);
        $data['update_btn_set'] = $report_id;
        $data['report_name'] = $report_name;
        $data['fiter_param'] = json_encode($data['fiter_param']);

        return View('pages.client.feedbacks.list')->with($data);
      }else{
        if (count($query)) {
          foreach ($query as $row) {
             $filter_param =  json_decode($row->filter_param,TRUE);

            $store_id = $filter_param['store_id'];
            $from_date = $filter_param['from_date'];
            $to_date = $filter_param['to_date'];
            $from_time = $filter_param['from_time'];
            $to_time = $filter_param['to_time'];
            $users = $filter_param['users'];
            $weekDays = $filter_param['weekDays'];
            
            $data = $this->filter_paramater($users,$store_id,$from_date,$to_date,$from_time,$to_time,$weekDays);
            $data['saved_users'] = $users;
            $data['saved_store_id'] = $store_id;
            $data['saved_from_date'] = $from_date;
            $data['saved_to_date'] = $to_date;
            $data['saved_from_time'] = $from_time;
            $data['saved_to_time'] = $to_time;
            $data['saved_weekDays'] = $weekDays;
            return View('pages.client.feedbacks.list')->with($data);
          }
        }else{
          return abort(404);
        }
      }
    }

     //get report list by ajax
    public function ajax_report_list(Request $request){

      $client_id =  Auth::user()->id; 
      $query = Report::where('added_by',$client_id)->get();
      return response()->json(['success'=>$query]);
    }

    //Retrive data from databse based on filter param
    public function filter_paramater($users = null,$store_id = null,$from_date= null,$to_date= null,$from_time= null,$to_time= null,$weekDays = null){

      $client_id =  Auth::user()->id;
      $user = Auth::user();
      //$user_role = $this->role_report(); //check user role

      if($user->hasRole(Role::CLIENT)){ 
        $query = Feedback::where('client_id', $client_id);
        if ($store_id != NULL) {
          $query->whereIn('store_id', $store_id);
        } 
        if ($from_date != NULL) {
          $from_date = date("Y-m-d", strtotime($from_date));
          $query->whereDate('feedback_date','>=', $from_date); 
        } 
        if ($to_date != NULL) {
          $to_date  = date("Y-m-d", strtotime($to_date));
          $query->whereDate('feedback_date','<=',$to_date);
        } 
        if ($from_time != NULL) {
          $query->whereTime('feedback_date','>=',$from_time);
        } 
        if ($to_time != NULL) {
          $query->whereTime('feedback_date','<=',$to_time);
         } 
        if ($weekDays != NULL) {
          $userfb = $query->select(DB::raw('feedback_date as fbDate, DAYNAME(feedback_date) as nameOfDay'))->get(); 
          $arr_fbDate = array();
          for ($i=0; $i < count($userfb); $i++) { 

            $fbDate = $userfb[$i]->fbDate;
            $nameOfDay = $userfb[$i]->nameOfDay;

            if (in_array($nameOfDay, $weekDays)){
              array_push($arr_fbDate,$fbDate);    
            }
          } //for loop end
          $query = Feedback::whereIn('feedback_date', $arr_fbDate);  
        }

        $report_data = $query->get();
        $stores = Store::where('client_id', $client_id)->get();
        $users = User::where('id', $client_id)->get();
        $data = array(
          'report_data' => $report_data,
          'stores' => $stores,
          'users' => $users,
          'weekDays' => $weekDays,
        );
      }elseif($user->hasRole(Role::ADMIN)){
        $query =  Feedback::query();
        if ($users != NULL) {
          $query->whereIn('client_id', $users);
        } 
        if ($store_id != NULL) {
          $query->whereIn('store_id', $store_id);
        } 
        if ($from_date != NULL) {
          $from_date = date("Y-m-d", strtotime($from_date));
          $query->whereDate('feedback_date','>=', $from_date);
        } 
        if ($to_date != NULL) {
          $to_date  = date("Y-m-d", strtotime($to_date));
          $query->whereDate('feedback_date','<=',$to_date);
        } 
        if ($from_time != NULL) {
          $query->whereTime('feedback_date','>=',$from_time);
        } 
        if ($to_time != NULL) {
          $query->whereTime('feedback_date','<=',$to_time);
        }
        if ($weekDays != NULL) {
          $userfb = $query->select(DB::raw('feedback_date as fbDate, DAYNAME(feedback_date) as nameOfDay'))->get(); 
          $arr_fbDate = array();
          for ($i=0; $i < count($userfb); $i++) { 
            $fbDate = $userfb[$i]->fbDate;
            $nameOfDay = $userfb[$i]->nameOfDay;
            if (in_array($nameOfDay, $weekDays)){
              array_push($arr_fbDate,$fbDate);    
            }
          } //for loop end
          $query = Feedback::whereIn('feedback_date', $arr_fbDate); 
        }
          
        $report_data = $query->get();
        $users = User::all();
        $stores = Store::all();
        $data = array(
          'report_data' => $report_data,
          'stores' => $stores,
          'users' => $users,
          'weekDays' => $weekDays,
        );
      }
      return $data;
    }

    //Check user role
   /* public function role_report(){
      $client_id =  Auth::user()->id;
      $user_role_id = UserRole::where('user_id',$client_id)->pluck('role_id');
      $role = Role::where('id',$user_role_id)->pluck('slug');
      $user_role = $role[0];
      return $user_role;
    }*/

    // filter data input request
    public function filter_param_btn(Request $request){
      $client_id =  Auth::user()->id;
      $store_id = $request->input('stores');
      $from_date = $request->input('from_date');
      $to_date  = $request->input('to_date');
      $from_time = $request->input('from_time');
      $to_time = $request->input('to_time');
      $weekDays = $request->input('weekDays');
            
      //$user_role = $this->role_report(); //check user role
       $user = Auth::user();
      if($user->hasRole(Role::CLIENT)){
        $users = $client_id; 
      }elseif($user->hasRole(Role::ADMIN)){
        $users = $request->input('users');
      }     
      $fiter_param = array(
        'users'=>$users,
        'store_id'=> $store_id,
        'from_date'=>$from_date,
        'to_date'=>$to_date,
        'from_time'=>$from_time,
        'to_time'=>$to_time,
        'weekDays'=>$weekDays,                      
      );

      $data = $this->filter_paramater($users,$store_id,$from_date,$to_date,$from_time,$to_time,$weekDays);
      $data['fiter_param'] = $fiter_param;
      $data['selectedUserPriority'] = $users;
      $data['selectedstorePriority'] = $store_id;
      $data['selectedDay'] = $weekDays;
      return $data;
    } 
}
