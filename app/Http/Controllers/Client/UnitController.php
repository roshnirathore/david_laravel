<?php

namespace App\Http\Controllers\Client;

use Auth;
use Session;
use App\Traits\CaptureIpTrait;
use App\Models\Store;
use App\Models\User;
use App\Models\Unit;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;

class UnitController extends Controller
{
    public function index($store_id){ 
        $client_id =  Auth::user()->id;
    	//$store = Store::findOrFail($store_id);
        $store = Store::where('client_id', $client_id)->findOrFail($store_id);
           
             $units = Unit::with('user')->where('store_id',$store_id)->where('client_id', $client_id)->paginate(20);
        return view('pages.client.units.list',compact('store','units'));

    	
    }

    public function timezone_list(){
       $timezone = array();
         $timestamp = time();
            foreach(timezone_identifiers_list(\DateTimeZone::PER_COUNTRY, 'US') as $key => $t) {
                date_default_timezone_set($t);
                if($t == 'America/New_York' || $t == 'America/Chicago' || $t == 'America/Denver' || $t == 'America/Phoenix' || $t == 'America/Los_Angeles'){
                  $timezone[$key]['zone'] = $t;
                  if($t == 'America/New_York'){
                    $show_zone = substr_replace("America/New_York","EST New York",0);
                  }else if($t == 'America/Chicago'){
                    $show_zone = substr_replace("America/Chicago","CST Chicago",0);
                  }else if($t == 'America/Denver'){
                    $show_zone = substr_replace("America/Denver","MST Denver",0);
                  }else if($t == 'America/Phoenix'){
                    $show_zone = substr_replace("America/Phoenix","MST Phoenix",0);
                  }else if($t == 'America/Los_Angeles'){
                    $show_zone = substr_replace("America/Los_Angeles","PST Los Angeles",0);
                  }
                   $timezone[$key]['show_zone'] = $show_zone;
                  $timezone[$key]['GMT_difference'] =  date('P', $timestamp);
                }
            }
         $timezone = collect($timezone)->sortBy('GMT_difference');
         return $timezone; 
    }

    public function edit($store_id ,$unit_id){ 
        $client_id =  Auth::user()->id;
    	$unit = Unit::where('client_id', $client_id)->where('store_id',$store_id)->findOrFail($unit_id);
        $timezone = $this->timezone_list();
    	$data = [
            'unit' => $unit,
            'timezone_list' =>$timezone,
        ];
        return view('pages.client.units.edit')->with($data);
    }

    public function update(Request $request,$store_id,$unit_id)
    {
        $unit = Unit::findOrFail($unit_id);
       // dd($unit);
        $validator = Validator::make($request->all(),  
        [
            'name'            => 'required',
            'timezone'        =>'required',
        ],
        [
            'name.required' => "Name is required",
            'timezone.required' => "Timezone is required"
        ])->validate();

        $unit->name = $request->input('name');
        $unit->timezone     = $request->input('timezone');
        $unit->modified_by = Auth::user()->id;
        $unit->save();
       // return back()->with('success',"Unit has been updated");
        return redirect('/store/'.$store_id.'/units')->with('success', "$unit->name Unit has been updated"); 
        
    }
}
