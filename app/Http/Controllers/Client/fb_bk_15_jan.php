<?php

namespace App\Http\Controllers\Client;

use Auth;
use Session;
use Carbon\Carbon;
use App\Traits\CaptureIpTrait;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Pagination\Paginator;
use App\Models\User;
use App\Models\Store;
use App\Models\Feedback;
use App\Models\Role;
use App\Models\UserRole;
use App\Models\Report;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;

class FeedbackController extends Controller  
{  
  public function __construct()
  { 
    $this->middleware('auth');

    $this->middleware(function ($request, $next) {
      $this->client_id = Auth::user()->id;
      $this->user = Auth::user();
      return $next($request);
    }); 
  }
    public function index(Request $request){ 
      $timezone = Auth::user()->timezone;
        if($request->has('fiter_report_btn')){

          $data = $this->filter_form($request);
          $data['json_fiter_param']= json_encode($data['fiter_param']);
        } 
        else{

          if($this->user->hasRole(Role::CLIENT)){ 

            $report_data = Feedback::where('client_id', $this->client_id)->get();
            $data = $this->canvas_report_data($report_data);
            $stores = Store::where('client_id', $this->client_id)->get();
            $data = array(
              'report_data' => $report_data,
              'likeArray' => $data['likeArray'],
              'unLikeArray' => $data['unLikeArray'],
              'dateArray' => $data['dateArray'],
              'stores' => $stores,
              'weekDays' => 'false',
              'report_name' => '',
              'selectTimezone' => $timezone,
            );
          }elseif($this->user->hasRole(Role::ADMIN)){

            $users = User::all();
            $report_data = Feedback::all();
            $stores = Store::all();
           // print_r($report_data); dd();
            $data = $this->canvas_report_data($report_data);
           
            $data = array(
              'report_data' => $report_data,
              'likeArray' => $data['likeArray'],
              'unLikeArray' => $data['unLikeArray'],
              'dateArray' => $data['dateArray'],
              'stores' => $stores,
              'users' => $users,
              'weekDays' => 'false',
              'report_name' => '',
              'selectTimezone' => $timezone,
            );
           
            if($request->ajax()){
              if(!empty($request->client_id)){ 
                $stores_list = Store::whereIn('client_id',$request->client_id)->get();

                return response()->json(['success'=>$stores_list]);
              } else{
                $stores_list = Store::get();
                return response()->json(['success'=>$stores_list]);
              }
            }
          } 
        }   
        $data['timezone']= $this->timezone_list();
      return View('pages.client.feedbacks.list')->with($data);  
    }

    // filter report save into databse
    public function save(Request $request){

      $report_id = $request->input('report_id');
      $filter_param= $request->input('filter_param');
      $report_name = $request->input('report_name');
      $string = implode(',', $filter_param); //array data convert into string
     //udate into database 
      if ($report_id) {

        $report = Report::findOrFail($report_id);
        $report->report_name = $report_name;
        $report->filter_param = $string;
        $report->added_by = Auth::user()->id;
        $report->save(); 
              
        return redirect('/reports')->with('success', "Report Updated successfully");
      } else{   
        $report = Report::create([  
          'report_name' => $report_name,
          'filter_param' => $string,
          'added_by' => Auth::user()->id
        ]);
        $report->save();   
        return redirect('/reports')->with('success', "Report Added successfully");
      }
    }

    //save report page
    public function report_list(Request $request,$report_id){

      $query = Report::where('id',$report_id)->where('added_by',$this->client_id)->get();

      if($request->has('fiter_report_btn')){

        foreach ($query as $row) {
           $report_name =  $row->report_name;
        }
        $data = $this->filter_form($request);
        $data['update_btn_set'] = $report_id;
        $data['report_name'] = $report_name;
        $data['timezone']= $this->timezone_list();
        $data['fiter_param'] = json_encode($data['fiter_param']);

        return View('pages.client.feedbacks.list')->with($data);
      }else{ 
        if (count($query)) {
          foreach ($query as $row) {
             $report_name =  $row->report_name;
             $filter_param =  json_decode($row->filter_param,TRUE);

            $store_id = $filter_param['store_id'];
            $from_date = $filter_param['from_date'];
            $to_date = $filter_param['to_date'];
            $from_time = $filter_param['from_time'];
            $to_time = $filter_param['to_time'];
            $users = $filter_param['users'];
            $weekDays = $filter_param['weekDays'];
            $timezone = $filter_param['timezone'];

            $data = $this->filter_paramater($users,$store_id,$from_date,$to_date,$from_time,$to_time,$weekDays,$timezone);
            $data['saved_users'] = $users;
            $data['saved_store_id'] = $store_id;
            $data['saved_from_date'] = $from_date;
            $data['saved_to_date'] = $to_date;
            $data['saved_from_time'] = $from_time;
            $data['saved_to_time'] = $to_time;
            $data['saved_weekDays'] = $weekDays;
            $data['report_name'] = $report_name;
            $data['saved_time_zone'] = $timezone;
            $data['timezone']= $this->timezone_list();
            return View('pages.client.feedbacks.list')->with($data);
          }
        }else{
          return abort(404);
        }
      }
    }

     //get report list by ajax
    public function ajax_report_list(Request $request){

      $query = Report::where('added_by',$this->client_id)->get();
      return response()->json(['success'=>$query]);
    }

    //Retrive data from databse based on filter param
    public function filter_paramater($users = null,$store_id = null,$from_date= null,$to_date= null,$from_time= null,$to_time= null,$weekDays = null,$timezone=null){

      if($this->user->hasRole(Role::CLIENT)){ 

        $query = Feedback::where('client_id', $this->client_id);
        $stores = Store::where('client_id', $this->client_id)->get();
        $users = User::where('id', $this->client_id)->get();

      }elseif($this->user->hasRole(Role::ADMIN)){

        $query =  Feedback::query();
        
        if ($users != NULL) {
          $query->whereIn('client_id', $users);
          $stores = Store::whereIn('client_id', $users)->get();
        }else{
          $stores = Store::all();
        }
        $users = User::all();
      }

        if ($store_id != NULL) {
          $query->whereIn('store_id', $store_id);
        } 
        if($timezone != NULL){
         // $date_time = Carbon::now('UTC')->toDateTimeString();
          if($from_date != NULL && $from_time != NULL){

            $utc_date = \DateTime::createFromFormat(
              'm-d-Y H:i',
              $from_date.' '.$from_time,
              new \DateTimeZone($timezone)
            );
            $acst_date = clone $utc_date; 
            $acst_date->setTimeZone(new \DateTimeZone('UTC'));
            $global_date_time = $acst_date->format('Y-m-d H:i');

            $query->where('global_feedback_date','>=', $global_date_time); 

          }else{
          if ($from_date != NULL) {
            $utc_date = \DateTime::createFromFormat(
              'm-d-Y',
              $from_date,
              new \DateTimeZone($timezone)
            );
            $acst_date = clone $utc_date; 
            $acst_date->setTimeZone(new \DateTimeZone('UTC'));
            $global_from_date = $acst_date->format('Y-m-d');

            $query->whereDate('global_feedback_date','>=', $global_from_date); 
          } 
          if ($from_time != NULL) {

            $utc_date = \DateTime::createFromFormat(
              'H:i',
              $from_time,
              new \DateTimeZone($timezone)
            );
            $acst_date = clone $utc_date; 
            $acst_date->setTimeZone(new \DateTimeZone('UTC'));
            $global_from_time = $acst_date->format('H:i');


            $query->whereTime('global_feedback_date','>=',$global_from_time);
          }
        }
          if($to_date != NULL && $to_time != NULL){

            $utc_date = \DateTime::createFromFormat(
              'm-d-Y H:i',
              $to_date.' '.$to_time,
              new \DateTimeZone($timezone)
            );
            $acst_date = clone $utc_date; 
            $acst_date->setTimeZone(new \DateTimeZone('UTC'));
            $global_to_date_time = $acst_date->format('Y-m-d H:i');

            $query->where('global_feedback_date','<=', $global_to_date_time); 

          }else{
            if ($to_date != NULL) {
              $utc_date = \DateTime::createFromFormat(
                'm-d-Y',
                $to_date,
                new \DateTimeZone($timezone)
              );
              $acst_date = clone $utc_date; 
              $acst_date->setTimeZone(new \DateTimeZone('UTC'));
              $global_to_date = $acst_date->format('Y-m-d');

              $query->whereDate('global_feedback_date','<=',$global_to_date);
            }  
            if ($to_time != NULL) {

              $utc_date = \DateTime::createFromFormat(
                'H:i',
                $to_time,
                new \DateTimeZone($timezone)
              );
              $acst_date = clone $utc_date; 
              $acst_date->setTimeZone(new \DateTimeZone('UTC'));
              $global_to_time = $acst_date->format('H:i');


              $query->whereTime('global_feedback_date','<=',$global_to_time);
             } 
          }
          if ($weekDays != NULL) {
            $weekDaysClause = implode('","',$weekDays);
            $userfb = $query->select(DB::raw('global_feedback_date as fbDate, DAYNAME(global_feedback_date) as nameOfDay'))->get(); 
            $arr_fbDate = array();
            for ($i=0; $i < count($userfb); $i++) { 
              $fbDate = $userfb[$i]->fbDate;
              $nameOfDay = $userfb[$i]->nameOfDay;
              if (in_array($nameOfDay, $weekDays)){
                array_push($arr_fbDate,$fbDate);    
              }
            }    
           $query = Feedback::select(DB::raw('id,DAYNAME(DATE(global_feedback_date)) as feedback_date, name'))->whereIn('global_feedback_date', $arr_fbDate);
          }
         
        }else{
          if ($from_date != NULL) {
           //$from_date = date("Y-m-d", strtotime($from_date));
            $old_date = explode('-', $from_date); 
            $from_date = $old_date[2].'-'.$old_date[0].'-'.$old_date[1];
            
            $query->whereDate('feedback_date','>=', $from_date); 
          } 
          if ($to_date != NULL) {
            //$to_date  = date("Y-m-d", strtotime($to_date));
            $old_date = explode('-', $to_date); 
            $to_date = $old_date[2].'-'.$old_date[0].'-'.$old_date[1];
            $query->whereDate('feedback_date','<=',$to_date);
          } 
          if ($from_time != NULL) {
            $query->whereTime('feedback_date','>=',$from_time);
          } 
          if ($to_time != NULL) {
            $query->whereTime('feedback_date','<=',$to_time);
           } 
          if ($weekDays != NULL) {
            $weekDaysClause = implode('","',$weekDays);
            $userfb = $query->select(DB::raw('feedback_date as fbDate, DAYNAME(feedback_date) as nameOfDay'))->get(); 
            $arr_fbDate = array();
            for ($i=0; $i < count($userfb); $i++) { 
              $fbDate = $userfb[$i]->fbDate;
              $nameOfDay = $userfb[$i]->nameOfDay;
              if (in_array($nameOfDay, $weekDays)){
                array_push($arr_fbDate,$fbDate);    
              }
            }    
           $query = Feedback::select(DB::raw('id,DAYNAME(DATE(feedback_date)) as feedback_date, name'))->whereIn('feedback_date', $arr_fbDate);
          }
        }
        $report_data = $query->get();
        $canvas_report_data = $this->canvas_report_data($report_data);
        $data = array(
          'report_data' => $report_data,
          'likeArray' => $canvas_report_data['likeArray'],
          'unLikeArray' => $canvas_report_data['unLikeArray'],
          'dateArray' => $canvas_report_data['dateArray'],
          'stores' => $stores,
          'users' => $users,
          'weekDays' => $weekDays,
        );
        //die("Gaurav");
      return $data;
    }

    // filter data input request
    public function filter_form(Request $request){

      $store_id = $request->input('stores');
      $from_date = $request->input('from_date');
      $to_date  = $request->input('to_date');
      $from_time = $request->input('from_time');
      $to_time = $request->input('to_time');
      $weekDays = $request->input('weekDays');
      $timezone = $request->input('timezone');
        
      if($this->user->hasRole(Role::CLIENT)){
        $users = $this->client_id; 
      }elseif($this->user->hasRole(Role::ADMIN)){
        $users = $request->input('users');
      }     
      $fiter_param = array(
        'users'=>$users,
        'store_id'=> $store_id,
        'from_date'=>$from_date,
        'to_date'=>$to_date,
        'from_time'=>$from_time,
        'to_time'=>$to_time,
        'weekDays'=>$weekDays,                      
        'timezone'=>$timezone,                      
      );

      $data = $this->filter_paramater($users,$store_id,$from_date,$to_date,$from_time,$to_time,$weekDays,$timezone);

      $data['fiter_param'] = $fiter_param;
      $data['selectedUserPriority'] = $users;
      $data['selectedstorePriority'] = $store_id;
      $data['selectedDay'] = $weekDays;
      $data['selectTimezone'] = $timezone;
      $data['report_name'] = '';
      return $data;
    } 

    public function canvas_report_data($report_data){

            $newReportData = $report_data->toArray();
            $finalReportedData = array();
            foreach ($newReportData as $value) {
              $key = explode(" ", $value['feedback_date']);
              if (isset($finalReportedData[$key[0]])) {
                if ($value['name']) {
                  $finalReportedData[$key[0]]['like'] += 1;
                }else{
                  $finalReportedData[$key[0]]['unlike'] += 1;
                }
              }else{
                if ($value['name']) {
                  $finalReportedData[$key[0]]['like'] = 1;
                  $finalReportedData[$key[0]]['unlike'] = 0;
                }else{
                  $finalReportedData[$key[0]]['unlike'] = 1;
                  $finalReportedData[$key[0]]['like'] = 0;
                }
              }
            }

            $likeArray = array();
            $unLikeArray = array();
            $dateArray = array();
            $j = 0;

            foreach ($finalReportedData as $key => $value) {
              $likeArray[$j] = $value['like'];
              $unLikeArray[$j] = $value['unlike'];
              $dateArray[$j] = $key;
              $old_date = explode('-', $key);
                if(isset($old_date[1])){
                  $dateArray[$j]= $old_date[1].'-'.$old_date[2].'-'.$old_date[0];
                }else{
                  $dateArray[$j] = $key;
                }
                
              $j++;
            }
           //print_r($dateArray); dd('test');
            $data = array(
            'likeArray' => json_encode($likeArray),
              'unLikeArray' => json_encode($unLikeArray),
              'dateArray' => json_encode($dateArray),
              );
            return $data;
    }

    public function timezone_list(){
     $timezone = array();
      $timestamp = time();
      foreach(timezone_identifiers_list(\DateTimeZone::PER_COUNTRY, 'US') as $key => $t) {
          date_default_timezone_set($t);
              $timezone[$key]['zone'] = $t;
              $timezone[$key]['GMT_difference'] =  date('P', $timestamp);
      }
      $timezone = collect($timezone)->sortBy('GMT_difference');
      return $timezone; 
    }

}
          