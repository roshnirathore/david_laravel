<?php

namespace App\Http\Controllers\Profile;

use Auth;
use Session;
use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;

class ChangePasswordController extends Controller
{
    public function index(){
    	return view('pages.profile.change-password');
    }

    public function update(Request $request){
    	$user = Auth::user();
        $request->validate([
            'old_password' => [
                'required',
                function ($attribute, $value, $fail) use ($user)
                {
                    if (!Hash::check($value, $user->password)) {
                        return $fail(__('The current password is incorrect.'));
                    }
                }
            ], 
            'new_password' => 'required|min:8',
            'confirm_new_password' => 'required|same:new_password'
        ], [
            'old_password.required' => 'Please enter your current password',
            'new_password.required' => 'Please enter a new password',
            'confirm_new_password.same' => 'The Confirm Password do not match'
        ]);
        $user->password = Hash::make($request->input('new_password'));
        $user->save();

        $request->session()->flash('success', 'Your password has been changed');

        return view('pages.profile.change-password');
    }
}
