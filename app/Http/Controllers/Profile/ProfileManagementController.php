<?php

namespace App\Http\Controllers\Profile;
 
use Auth;
use Session;
use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Hash; 

class ProfileManagementController extends Controller
{
    public function __construct()
  { 
    $this->middleware('auth');

    $this->middleware(function ($request, $next) {
      $this->client_id = Auth::user()->id;
      $this->user = Auth::user();
      return $next($request);
    }); 
  }
  
    public function index(){
    	$user = Auth::user();
        $timezone = $this->timezone_list();
    	return view('pages.profile.profile', [
            'first_name' => $user->first_name,
            'last_name' => $user->last_name,
            'email' => $user->email,
            'company_name' => $user->company_name,
            'address_line_1' => $user->address_line_1,
            'address_line_2' => $user->address_line_2,
            'city' => $user->city,
            'state' => $user->state,
            'country' => $user->country,
            'zip_code' => $user->zip_code,
            'phone_no' => $user->phone_no,
            'phone_number_extension' => $user->phone_number_extension,
            'alter_phone_no' => $user->alter_phone_no,
            'user_timezone' => $user->timezone,
            'timezone_list' =>$timezone,
        ]); 
    }  

    public function timezone_list(){
       $timezone = array();
         $timestamp = time();
            foreach(timezone_identifiers_list(\DateTimeZone::PER_COUNTRY, 'US') as $key => $t) {
                date_default_timezone_set($t);
                if($t == 'America/New_York' || $t == 'America/Chicago' || $t == 'America/Denver' || $t == 'America/Phoenix' || $t == 'America/Los_Angeles'){
                  $timezone[$key]['zone'] = $t;
                  if($t == 'America/New_York'){
                    $show_zone = substr_replace("America/New_York","EST New York",0);
                  }else if($t == 'America/Chicago'){
                    $show_zone = substr_replace("America/Chicago","CST Chicago",0);
                  }else if($t == 'America/Denver'){
                    $show_zone = substr_replace("America/Denver","MST Denver",0);
                  }else if($t == 'America/Phoenix'){
                    $show_zone = substr_replace("America/Phoenix","MST Phoenix",0);
                  }else if($t == 'America/Los_Angeles'){
                    $show_zone = substr_replace("America/Los_Angeles","PST Los Angeles",0);
                  }
                  $timezone[$key]['show_zone'] = $show_zone;
                  $timezone[$key]['GMT_difference'] =  date('P', $timestamp);
                }
            }
         $timezone = collect($timezone)->sortBy('GMT_difference');
         return $timezone; 
    }
    public function update(Request $request){
        
        $request->validate([ 
            'first_name' => 'required',
            'last_name' => 'required|',
            'company_name' => 'required|',
            'address_line_1' => 'required|',
            'city' => 'required|',
            'state' => 'required|',
            'country' => 'required|',
            'zip_code' => 'required|',
            'phone_no' => 'required|',
            'timezone' => 'required|',
            /*'current_password' => 'required',
            'password' => 'required|same:password',
            'confirm_password' => 'required|same:password',  */

        ], [
            'first_name.required' => 'Please enter First Name',
            'last_name.required' => 'Please enter a Last Name',
            'company_name.required' => 'Please enter a Company Name',
            'address_line_1.required' => 'Please enter a Address line 1',
            'city.required' => 'Please enter a City',
            'state.required' => 'Please enter a State',
            'zip_code.required' => 'Please enter a Zip Code',
            'phone_no.required' => 'Please enter a Phone Number',
            'timezone.required' => 'Please select your timezone',
            /*'current_password.required' => 'Please enter current password',
            'password.required' => 'Please enter password',*/
        ]);
        $user_id = Auth::User()->id;                       
        $user = User::find($user_id);

        $user->first_name = $request->input('first_name');
        $user->last_name = $request->input('last_name');
        $user->company_name = $request->input('company_name');
        $user->address_line_1 = $request->input('address_line_1');
        $user->address_line_2 = $request->input('address_line_2');
        $user->city = $request->input('city');
        $user->state = $request->input('state');
        $user->country = $request->input('country');
        $user->zip_code = $request->input('zip_code');
        $user->phone_no = $request->input('phone_no');
        $user->phone_number_extension = $request->input('phone_number_extension');
        $user->alter_phone_no = $request->input('alter_phone_no');
        $user->timezone = $request->input('timezone');
       // $user->profile_image =$fileName;
        $user->save();
        $request->session()->flash('success', 'Your profile has been updated');
         $timezone = $this->timezone_list();
        return view('pages.profile.profile', [
            'first_name' => $user->first_name, 
            'last_name' => $user->last_name,
            'company_name' => $user->company_name,
            'address_line_1' => $user->address_line_1,
            'address_line_2' => $user->address_line_2,
            'city' => $user->city,
            'state' => $user->state,
            'country' => $user->country,
            'zip_code' => $user->zip_code,
            'phone_no' => $user->phone_no,
            'phone_number_extension' => $user->phone_number_extension,
            'alter_phone_no' => $user->alter_phone_no,
            'email' => $user->email,
            'user_timezone' => $user->timezone,
            'timezone_list' =>$timezone,
        ]);


        //Image upload
        /*$image = $request->file('profile_image');
        if($image){
            $extension = $image->getClientOriginalExtension();
            $fileName  = $image->getFilename().'.'.$extension;
            Storage::disk('public')->put($fileName ,  File::get($image));
        }else{
            $fileName = '';
        }
*/
    	
        /*$current_password = Auth::User()->password;  
         if(Hash::check($request->input('current_password'), $current_password))
          {           
            $user_id = Auth::User()->id;                       
            $user = User::find($user_id);
            $user->password = Hash::make($request->input('password'));

            $user->first_name = $request->input('first_name');
            $user->last_name = $request->input('last_name');
           // $user->profile_image =$fileName;
            $user->save();
            $request->session()->flash('success', 'Your profile has been updated');

            return view('pages.profile.profile', [
                'first_name' => $user->first_name,
                'last_name' => $user->last_name,
                'email' => $user->email,
                'profile_image' => $user->profile_image
            ]);
          }else
          {           
          
            return redirect()->back()->with('error', "Please enter correct current password");
          }*/
          

        
    }
}
