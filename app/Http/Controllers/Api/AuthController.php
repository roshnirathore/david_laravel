<?php

namespace App\Http\Controllers\Api;

use Auth;
use Session;
use Carbon\Carbon; 
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;
use App\Http\Controllers\Controller;
use App\Models\User;
use App\Models\Store;
use App\Models\Unit;
use App\Models\Feedback;
use App\Models\App;
use App\Models\Installation;
use Illuminate\Support\Facades\DB;


class AuthController extends Controller
{
   
    public function login(Request $request){ 

      $data = json_decode(file_get_contents("php://input"));

       if(!empty($data->email) && !empty($data->password)){ 

        if(Auth::attempt(['email' => $data->email, 'password' => $data->password])){ 

            $user = Auth::user();
            $user_id = Auth::user()->id; 

            if(Auth::user()->roles[0]->slug == 'client'){

              $success['token'] =  $user->createToken('MyApp')->accessToken; 
           	
           	  $success['user_info'] = User::where('id',$user_id)->first();

           	  //$store_id = Store::where('client_id',$user_id)->pluck('id')->toArray();

              $store_id = Unit::where('client_id',$user_id)->pluck('store_id')->toArray();

           	  $success['stores'] = Store::with('units')->where('client_id', $user_id)->whereIn('id',$store_id)->get();

              if(!empty($success['stores']->toArray()) && !empty($success['stores'][0]['units']->toArray())){

               if(isset($data->device_id)){
                $install= DB::table('installations')->where('device_id',$data->device_id)->update(['client_id'=> $user_id]);
                }else{
                  $device_id = '';
                  $install= DB::table('installations')->where('device_id',$device_id)->update(['client_id'=> $user_id]);
                }

                return response()->json([
                  'success' => true,
                  'data' => $success,
                ],200);
              }
              else{
                return response()->json([
                  'success' => false,
                  'error' =>'Store and Unit is required for login. Please contact your admin.',
                ],401);
              }
          }else{ //admin can't login

            return response()->json([
              'success' => false,
              'error' =>'Only client can login.'
            ],401);
          }
        } 
        else{   //username password are not match

            return response()->json([
		        'success' => false,
		        'error' =>'Username and password are Invalid'
		     ],401);
        } 
      }else{ //username or password are missing

        return response()->json([
            'success' => false,
            'error' =>'Username and password both are Required'
         ],400);
      }
    } 

    public function submitFeedback(){ 
    	$user = Auth::user(); 
      $data = json_decode(file_get_contents("php://input"));

      //if(!empty($data->rating) && !empty($data->store_id) && !empty($data->unit_id) && !empty($data->client_id) && !empty($data->feedback_date)){

        for ($i=0; $i < count($data); $i++) { 

          $timezone = Unit::where('id',$data[$i]->unit_id)->get(['timezone']);

          $local_feedback_date = $data[$i]->feedback_date;

          $utc_date = \DateTime::createFromFormat(
              'Y-m-d H:i:s',
              $local_feedback_date,
              new \DateTimeZone($timezone[0]->timezone)
          );

          $acst_date = clone $utc_date; 
          $acst_date->setTimeZone(new \DateTimeZone('UTC'));
          $global_feedback_date = $acst_date->format('Y-m-d H:i:s');

          $feedback = new Feedback;
          $feedback->name = $data[$i]->rating;
          $feedback->store_id = $data[$i]->store_id;
          $feedback->unit_id = $data[$i]->unit_id;
          $feedback->client_id = $data[$i]->client_id;
          $feedback->feedback_date = $local_feedback_date;
          $feedback->global_feedback_date = $global_feedback_date;
          $feedback->save();
        }
      	 return response()->json([
  		        'success' => true,
  		        'message' => 'Feedback submitted',
  		     ],200);
     /* }else{

        return response()->json([
          'success' => false,
          'error' =>'All Fields are required'
        ]);
      }*/
    }

    public function checkUpdate(){ //app version check and update

      $data = json_decode(file_get_contents("php://input"));

      if(!empty($data->version)){
        $app = App::where('version',$data->version)->where('is_live','true')->get(['file']);
        
        if($app->toArray()){
          $url = $app->toArray();
          $filename = $url[0]['file'];
          return response()->json([
            'success' => false,
           'url' => 'http://dashboard.focustrax.com/uploads/'.$filename,
            'version' => $data->version,
                
          ]);
          
        }else{
          $result = App::where('is_live','true')->get();
          if($result->toArray()){
            return response()->json([
            'success' => true,
            'url' => 'http://dashboard.focustrax.com/uploads/'.$result[0]->file,
            'version' =>$result[0]->version
          ]);
          }else{
            return response()->json([
              'success' => false,
              'message' => 'App is not available'
            ]);
          }
          
        }
      } else{
        return response()->json([
            'success' => false,
            'error' =>'App Version is required'
          ]);
      }
    }

    //app install api endpoint
    public function installation(Request $request){

      $data = json_decode(file_get_contents("php://input"));

      if(!empty($data->device_id)){ 
         $device_id_check = Installation::where('device_id',$data->device_id)->get();

         if(empty($device_id_check)){
          DB::table('installations')->where('device_id',$data->device_id)->update(['client_id'=> 0]);
         
         }else{
           $install = new Installation;
           $install->device_id = $data->device_id;
           $install->client_id = 0;
           $install->app_version = $data->app_version;
           $install->save();
         }
         return response()->json([
              'success' => true
           ],200);
      }else{

        return response()->json([
            'success' => false,
            'error' =>'Device Id is required'
          ]);
      }
    }
}
