<?php

namespace App\Http\Controllers;


use Auth;
use Session;
use App\Traits\CaptureIpTrait;
use App\Models\Store;
use App\Models\User;
use App\Models\Unit;
use App\Models\Feedback;
use Illuminate\Http\Request;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;

class UnitsManagementController extends Controller 
{
    //
    public function index($client_id,$store_id) 
    {
      
    	$store = Store::where('client_id',$client_id)->findOrFail($store_id);
    	$units = Unit::with('user')->where('store_id',$store_id)->where('client_id', $client_id)->paginate(20);
        return view('pages.admin.units.list',compact('store','units','client_id'));	
    }

    public function timezone_list(){
       $timezone = array();
         $timestamp = time();
            foreach(timezone_identifiers_list(\DateTimeZone::PER_COUNTRY, 'US') as $key => $t) {
                date_default_timezone_set($t);
                if($t == 'America/New_York' || $t == 'America/Chicago' || $t == 'America/Denver' || $t == 'America/Phoenix' || $t == 'America/Los_Angeles'){
                  $timezone[$key]['zone'] = $t;
                  if($t == 'America/New_York'){
                    $show_zone = substr_replace("America/New_York","EST New York",0);
                  }else if($t == 'America/Chicago'){
                    $show_zone = substr_replace("America/Chicago","CST Chicago",0);
                  }else if($t == 'America/Denver'){
                    $show_zone = substr_replace("America/Denver","MST Denver",0);
                  }else if($t == 'America/Phoenix'){
                    $show_zone = substr_replace("America/Phoenix","MST Phoenix",0);
                  }else if($t == 'America/Los_Angeles'){
                    $show_zone = substr_replace("America/Los_Angeles","PST Los Angeles",0);
                  }
                   $timezone[$key]['show_zone'] = $show_zone;
                  $timezone[$key]['GMT_difference'] =  date('P', $timestamp);
                }
            }
         $timezone = collect($timezone)->sortBy('GMT_difference');
         return $timezone; 
    }

    public function create($client_id, $store_id)
    { 
        $timezone = $this->timezone_list();
    	$user_timezone = User::where('id', $client_id)->pluck('timezone'); 
        $user_timezone = $user_timezone->toArray();
        $data = [
            'store_id' => $store_id,
            'client_id' => $client_id,
            'timezone' =>$timezone,
            'user_timezone' =>$user_timezone[0],
        ];
        //dd($store_id);
    	return view('pages.admin.units.create')->with($data);
    }

    public function save(Request $request, $client_id,$store_id)
    {  
    	$validator = Validator::make($request->all(),
            [
                'name'            => 'required',
                'timezone'        =>'required',
            ],
            [
                'name.required' => "Name is required",
                'timezone.required' => "Timezone is required"
            ]
        )->validate();

        $unit = Unit::create([
        		'name'         => $request->input('name'),
                'timezone'     => $request->input('timezone'),
        		'store_id'     => $store_id,
                'client_id'    => $client_id,
        		'added_by'     => Auth::user()->id,
                'modified_by'  => Auth::user()->id,
        	]);
        $unit->save();

        return redirect('client/'.$client_id.'/store/'.$store_id.'/units')->with('success', 'Unit saved correctly!!!');
    }

    public function edit($client_id,$store_id ,$id)
    {
    	//dd($store_id);
    	$unit = Unit::where('store_id',$store_id)->where('client_id', $client_id)->findOrFail($id);
        $timezone = $this->timezone_list();
    	$data = [
            'unit' => $unit,
            'client_id' =>$client_id,
            'timezone_list'=>$timezone
        ];
        return view('pages.admin.units.edit')->with($data);
    }

    public function update(Request $request, $client_id,$store_id,$id)
    {
        $unit = Unit::find($id);
       // dd($unit);
        $validator = Validator::make($request->all(),  
            [
                'name'            => 'required',
                'timezone'        =>'required',
            ],
            [
                'name.required' => "Name is required",
                'timezone.required' => "Timezone is required"
            ])->validate();

        $unit->name = $request->input('name');
        $unit->timezone    = $request->input('timezone');
        $unit->modified_by = Auth::user()->id;

        $unit->save();
        //return back()->with('success',"Unit has been updated");
        return redirect('client/'.$client_id.'/store/'.$store_id.'/units')->with('success',"Unit has been updated");
        
    }

      /**
     * Delete the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function delete($client_id,$store_id, $id)
    {
        $unit = Unit::findOrFail($id);

        
            Unit::destroy($unit->id);
             return redirect('client/'.$client_id.'/store/'.$store_id.'/units')->with('success', 'Unit has been deleted!!!');  
        
    } 

    public function deleteFeedback($client_id,$store_id,$unit_id){
        $feedback = Feedback::where('client_id',$client_id)->where('store_id',$store_id)->where('unit_id',$unit_id)->get(['id']);

        $result = $feedback->toArray();
        if(empty($result)){
           return redirect('client/'.$client_id.'/store/'.$store_id.'/units')->with('error', 'There are not feedback available!!!');
        }else{
            foreach ($result as $key => $value) {
               Feedback::destroy($value['id']); 
            }
           return redirect('client/'.$client_id.'/store/'.$store_id.'/units')->with('success', 'Feedbacks have been deleted!!!'); 
        }
    }

}
