<?php

namespace App\Http\Controllers;

use Auth;
use Session;
use App\Traits\CaptureIpTrait;
use App\Models\Role;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;

class StoreUserManagementController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
        $storeUsers = User::whereHas('roles', function($role) {
            $role->where('name', '=', Role::STORE);
        })->paginate(20);

        return View('pages.client.store-user.list', compact('storeUsers'));
    }

      /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $roles = Role::all();
        $data = [
            'roles' => $roles,
        ];

        return view('pages.client.store-user.create')->with($data);
    }

     /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function save(Request $request)
    {
        $validator = Validator::make($request->all(),
            [
                'first_name'            => 'required',
                'last_name'             => 'required',
                'email'                 => 'required|email|max:255|unique:users',
                'password'              => 'required|min:6|max:20|confirmed',
                'password_confirmation' => 'required|same:password',
            ],
            [
                'first_name.required' => "First name is required",
                'last_name.required'  => "Last name is required",
                'email.required'      => "Email is required",
                'email.email'         => "Please enter a valid email address",
                'password.required'   => "Please enter a password",
                'password.min'        => "minimum password length is  6",
                'password.max'        => "maximum password length is 20"
            ]
        )->validate();

       
        $ipAddress = new CaptureIpTrait();
        $user = User::create([
                'name'             => $request->input('email'),
                'first_name'       => $request->input('first_name'),
                'last_name'        => $request->input('last_name'),
                'email'            => $request->input('email'),
                'password'         => Hash::make($request->input('password')),
                'admin_ip_address' => $ipAddress->getClientIp(),
                'activated'        => 1,
                'token'            => str_random(64)
            ]);            
        $user->save();
        $user->addRole(Role::STORE);
        return redirect('store-users')->with('success', "Store user has been added!");        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::findOrFail($id);
        $data = [
            'user'        => $user
        ];
        return view('pages.client.store-user.edit')->with($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int                      $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $currentUser = Auth::user();
        $user = User::find($id);
        $ipAddress = new CaptureIpTrait();
        $validator = Validator::make($request->all(), [
                'first_name'     => 'required|max:255',
                'last_name'     => 'required|max:255',

            ])->validate();

        $user->first_name = $request->input('first_name');
        $user->last_name = $request->input('last_name');
        $user->updated_ip_address = $ipAddress->getClientIp();
        $user->save();
        // Session::push('success', 'Client has been updated');
        return back()->with('success',"Store User has been updated");
        
    }

    /**
     * Delete the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function delete($id)
    {
        $user = User::findOrFail($id);
        if (!$user->hasRole(Role::ADMIN))
        {
            User::destroy($user->id);
            return redirect()->back()->with('success', "$user->first_name has been deleted");
        }
        return back()->with('error',"You can not delete admin user");
    }
}
/*
public function feedback_api(Request $request){
       $request->session()->token();
      

    $feedback = new Feedback;
    $feedback->name = $request->rating;
    $feedback->store_id = $request->store_id;
    $feedback->unit_id = $request->unit_id;
    $feedback->client_id = $request->client_id;
    $feedback->feedback_date = $request->feedback_date;
    
    $feedback->save();


        return response()->json([
        "message" => "Your Feedback Submited"
    ], 201);
    }*/