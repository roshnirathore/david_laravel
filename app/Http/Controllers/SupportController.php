<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Mail;
use Auth; 
use App\Mail\Support;
use App\Mail\UserSupport;
class SupportController extends Controller
{ 
    public function __construct()
  {
    $this->middleware('auth');
  } 

    public function index(){
        $email = Auth::user()->email;
        $first_name = Auth::user()->first_name;
        $last_name = Auth::user()->last_name;
    	return View('support',compact('email','first_name','last_name'));
    }
    public function mail(Request $request){
        
    	$name = $request->input('name');
    	$email = $request->input('email');
    	//$subject = $request->input('subject');
        $subject = 'test';
    	$comment = $request->input('comment');
    	$email_content = array(
                        'name'=>$name,
                        'email'=>$email,
                        'subject'=>$subject,
                        'comment'=>$comment, 
                    );
    	 Mail::to('support@focustrax.com','Question from FocusTrax user')->send(new Support($email_content)); //user to admin
        // Mail::to($email,$name)->send(new UserSupport($email_content)); //admin to user

        return back()->with('success',"Thank you! Your message has been sent successfully");
    }
}
