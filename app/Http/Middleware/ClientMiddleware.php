<?php
 
namespace App\Http\Middleware;

use Closure;
use App\Models\Role;

class ClientMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
     public function handle($request, Closure $next)
    {
        if ($request->user() && $request->user()->hasRole(Role::CLIENT)){
           return $next($request);
        }
        return redirect('/')->with('forbidden', 'You are not permitted to access this page');
        
    }
}
