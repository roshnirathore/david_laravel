<?php

use App\Models\Profile;
use App\Models\User;
use App\Models\Role;
use Illuminate\Database\Seeder;


class UsersTableSeeder extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();
        $adminUser = [
                        'name'                           => $faker->userName,
                        'first_name'                     => $faker->firstName,
                        'last_name'                      => $faker->lastName,
                        'email'                          => "admin@admin.com",
                        'password'                       => Hash::make('password'),
                        'token'                          => str_random(64),
                        'activated'                      => true,
                        'signup_confirmation_ip_address' => $faker->ipv4,
                        'admin_ip_address'               => $faker->ipv4,
                    ];
        $clientUser = [
                        'name'                           => $faker->userName,
                        'first_name'                     => $faker->firstName,
                        'last_name'                      => $faker->lastName,
                        'email'                          => 'client@client.com',
                        'password'                       => Hash::make('password'),
                        'token'                          => str_random(64),
                        'activated'                      => true,
                        'signup_confirmation_ip_address' => $faker->ipv4,
                        'admin_ip_address'               => $faker->ipv4,
                    ];
        $storeUser = [
                        'name'                           => $faker->userName,
                        'first_name'                     => $faker->firstName,
                        'last_name'                      => $faker->lastName,
                        'email'                          => 'store@store.com',
                        'password'                       => Hash::make('password'),
                        'token'                          => str_random(64),
                        'activated'                      => true,
                        'signup_confirmation_ip_address' => $faker->ipv4,
                        'admin_ip_address'               => $faker->ipv4,
                    ];

        $this->createAdmin($adminUser,'Admin');
        $this->createAdmin($clientUser,'Client');
        $this->createAdmin($storeUser,'Store');

    }

    private function createAdmin($newUser,$role)
    {
        $user = App\Models\User::where([
            'email' => $newUser['email']
        ])->first();

        if (!$user)
        {
            $user = new App\Models\User();
            $user->email = $newUser['email'];
            $user->name = $newUser['email'];            
            $user->first_name = $newUser['first_name'];
            $user->last_name = $newUser['last_name'];            
            $user->password = $newUser['password'];
            $user->token = $newUser['token'];
            $user->activated = $newUser['activated'];
            $user->signup_confirmation_ip_address = $newUser['signup_confirmation_ip_address'];
            $user->admin_ip_address = $newUser['admin_ip_address'];    
            $user->save();
        }
        
        if($role == 'Admin' && !$user->hasRole(App\Models\Role::ADMIN)){
            $user->addRole(App\Models\Role::ADMIN);
        }
        if($role == 'Client' && !$user->hasRole(App\Models\Role::CLIENT)){
            $user->addRole(App\Models\Role::CLIENT);
        }
        if($role == 'Store' && !$user->hasRole(App\Models\Role::STORE)){
            $user->addRole(App\Models\Role::STORE);
        }
        return $user;
    }
}
