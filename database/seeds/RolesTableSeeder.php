<?php

use Illuminate\Database\Seeder;

class RolesTableSeeder extends Seeder
{

       /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {   
        $adminRole = [
                        'name'        => 'Admin',
                        'slug'        => 'admin',
                        'description' => 'Admin Role',
                        'level'       => 5,
                    ];
        $clientRole = [
                        'name'        => 'Client',
                        'slug'        => 'client',
                        'description' => 'Client Role',
                        'level'       => 1,
                    ];
        $storeRole = [
                        'name'        => 'Store',
                        'slug'        => 'store',
                        'description' => 'Store Role',
                        'level'       => 2,
                    ];
        $this->createRole($adminRole);
        $this->createRole($clientRole);
        $this->createRole($storeRole);

    }

    private function createRole($newRole)
    {
        
         if (App\Models\Role::where('name', '=', $newRole['name'])->first() === null) {
            $adminRole = App\Models\Role::create($newRole);
        }

    }
}
