<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/*Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});*/
 
Route::post('login', 'Api\AuthController@login');

Route::group(['middleware' => 'auth:api'], function(){

Route::post('submit_feedback', 'Api\AuthController@submitFeedback');
Route::post('check_update', 'Api\AuthController@checkUpdate');
Route::post('/app/version/check', 'Api\AuthController@checkUpdate');
});

Route::post('/app/install/log', 'Api\AuthController@installation');