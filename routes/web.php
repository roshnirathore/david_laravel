<?php  

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect()->guest('login');
});
 
Auth::routes([ 'register' => false ]);
Route::get('logout', 'Auth\LoginController@logout'); 

 //Route for Client 
Route::middleware(['auth','middleware' => 'App\Http\Middleware\ClientMiddleware'])->group(function () {

	// Store Module
    Route::get('/stores', 'Client\StoreController@index')->name('client_store.index');
	Route::get('/stores/edit/{id}', 'Client\StoreController@edit')->name('client_store.edit');
	Route::post('/stores/update/{id}', 'Client\StoreController@update')->name('client_store.update');

	//Units Module
	Route::get('/store/{store_id}/units', 'Client\UnitController@index')->name('client_unit.index');

	Route::get('/store/{store_id}/units/edit/{id}', 'Client\UnitController@edit')->name('client_unit.edit');

	Route::post('/store/{store_id}/units/update/{id}', 'Client\UnitController@update')->name('client_unit.update');

	//Client's feedback

	/*Route::get('/reports/create', 'Client\FeedbackController@create')->name('client_feedback.create');
	Route::post('/reports/save_feedback', 'Client\FeedbackController@save_feedback')->name('client_feedback.save_feedback');*/

});

// Registered, activated, and is admin routes.
Route::group(['auth','middleware' => 'App\Http\Middleware\AdminMiddleware'], function () {    
    Route::get('php', 'UtilityController@listPHPInfo');    
    
    // Client Module
    Route::get('/clients', 'ClientManagementController@index')->name('client.index');
	Route::get('/clients/create', 'ClientManagementController@create')->name('client.create');
	Route::post('/clients/save', 'ClientManagementController@save')->name('client.save');
	Route::get('/clients/edit/{id}', 'ClientManagementController@edit')->name('client.edit');
	Route::post('/clients/update/{id}', 'ClientManagementController@update')->name('client.update');
	Route::get('/clients/delete/{id}', 'ClientManagementController@delete')->name('client.delete');
	Route::get('/search', 'ClientManagementController@search')->name('client.search');
	

	// App Module
	Route::get('/apps','AppController@index')->name('app.index');
	Route::get('/app/create','AppController@create')->name('app.create');
	Route::post('/app/save','AppController@save')->name('app.save');
	Route::get('/app/edit/{id}','AppController@edit')->name('app.edit');
	Route::post('/app/update/{id}','AppController@update')->name('app.update');
	Route::get('/app/delete/{id}','AppController@delete')->name('app.delete');

	// Store Module
    Route::get('/client/{client_id}/stores', 'StoreManagementController@index')->name('store.index');
	Route::get('/client/{client_id}/stores/create', 'StoreManagementController@create')->name('store.create');
	Route::post('/client/{client_id}/stores/save', 'StoreManagementController@save')->name('store.save');
	Route::get('/client/{client_id}/stores/edit/{id}', 'StoreManagementController@edit')->name('store.edit');
	Route::post('/client/{client_id}/stores/update/{id}', 'StoreManagementController@update')->name('store.update');
	Route::get('/client/{client_id}/stores/delete/{id}', 'StoreManagementController@delete')->name('store.delete');

	//Units Module
	Route::get('/client/{client_id}/store/{store_id}/units', 'UnitsManagementController@index')->name('unit.index');

	Route::get('/client/{client_id}/store/{store_id}/units/create' , 'UnitsManagementController@create')->name('unit.create');

	Route::post('/client/{client_id}/store/{store_id}/units/save' ,'UnitsManagementController@save')->name('unit.save');

	Route::get('/client/{client_id}/store/{store_id}/units/edit/{id}', 'UnitsManagementController@edit')->name('unit.edit');

	Route::post('/client/{client_id}/store/{store_id}/units/update/{id}', 'UnitsManagementController@update')->name('unit.update');
	
	Route::get('/client/{client_id}/store/{store_id}/units/delete/{id}', 'UnitsManagementController@delete')->name('unit.delete');

	Route::get('/client/{client_id}/store/{store_id}/units/deleteFeedback/{id}', 'UnitsManagementController@deleteFeedback')->name('feedback.delete');	

});
 
Route::group(['auth'], function () {
//Route::get('/home', 'ClientManagementController@index')->name('client.home');

Route::any('/home', 'Client\FeedbackController@index')->name('client.home');

 Route::any('/reports', 'Client\FeedbackController@index')->name('client_report.index');

	Route::any('/reports/save', 'Client\FeedbackController@save')->name('client_report.save');

	//Route::post('/reports/filter', 'Client\FeedbackController@filter_data')->name('client_report.filter_data');

	Route::any('/report/{report_id}','Client\FeedbackController@report_list')->name('client_report.report_list'); 
	Route::any('/report/delete/{report_id}','Client\FeedbackController@report_delete')->name('report.delete'); 

	Route::any('/reports/report_list','Client\FeedbackController@ajax_report_list')->name('client_report.ajax_report_list');

	Route::any('/support','SupportController@index')->name('support.index');
	Route::any('/support_mail','SupportController@mail')->name('support.mail');
	//Route::any('/faq','FaqController@index')->name('faq.index');
	Route::any('/profile', 'Profile\ProfileManagementController@index')->name('profile.index');
	Route::any('/update', 'Profile\ProfileManagementController@update')->name('profile.update');

	Route::any('/password_edit', 'Profile\ChangePasswordController@index')->name('password.index');
	Route::any('/password_update', 'Profile\ChangePasswordController@update')->name('password.update');
});

