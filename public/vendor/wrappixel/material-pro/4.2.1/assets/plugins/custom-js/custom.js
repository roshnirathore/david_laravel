   
                          $(document).ready(function(){
                            var graph_data = {!! $report_data->toJson() !!};
                            
                            var title_report_name  = '<?php if($report_name) echo $report_name; else  echo $report_name; ?>';
                            
                           var weekDays  = <?php if( $weekDays == "false") echo $weekDays; else  echo json_encode($weekDays); ?>;

                           var weekDay_len;
                            if (weekDays) {
                               weekDay_len = weekDays.length;
                             }else{
                              weekDay_len=0;
                             }
                        
                          var likeDate = new Array();
                          var likeDateValue = new Array();
                          var unlikeDate = new Array();
                          var unlikeDateValue = new Array();
                          var likeFinalData = new Array();
                          var unlikeFinalData = new Array();
                         
                            var check_data = graph_data.length;
                            if(check_data == 0){
                              $("#chartContainer").hide();
                              $("#not_data").show();
                            }else{
                              $("#not_data").hide();

                          for (var i = graph_data.length - 1; i >= 0; i--) {

                            var mydate=graph_data[i]['feedback_date'];
                            //date format change
                            var fbDate = new Date(mydate);
                            var dd = fbDate.getDate(); 
                            var mm = fbDate.getMonth() + 1; 
                            var yyyy = fbDate.getFullYear(); 
                            if (dd < 10) { 
                                dd = '0' + dd; 
                            } 
                            if (mm < 10) { 
                                mm = '0' + mm; 
                            } 
                            var fbDate = mm + '-' + dd + '-' + yyyy;

                            var fbValue = graph_data[i]['name'];
                           // var fbValue = graph_data[i]['name'].count;
                            if(fbValue == 1){
                                var found = $.inArray(fbDate, likeDate);
                                if(found == -1){
                                    likeDate.push(fbDate);
                                    likeDateValue.push(1);
                                }else{
                                    likeDateValue[found] += 1;
                                }
                            }else{
                                var found = $.inArray(fbDate, unlikeDate);
                                if(found == -1){
                                    unlikeDate.push(fbDate);
                                    unlikeDateValue.push(1);
                                }else{
                                    unlikeDateValue[found] += 1;
                                }
                            }
                          }

                          var length = likeDate.length; 
                          var i = 0;
                          if(length> 0){
                              while(i < length){
                                if(weekDay_len > 0){
                                  var data = {  y: likeDateValue[i] , label: weekDays[i]};
                                }else{
                                  var data = {  y: likeDateValue[i] , label: likeDate[i]};
                                 }
                                likeFinalData.push(data);
                                i++;
                              }
                          }

                          var length = unlikeDate.length;
                          var i = 0;
                          if(length> 0){
                              while(i < length){
                                if(weekDay_len > 0){
                                var data = {  y: unlikeDateValue[i] , label: weekDays[i]};
                              } else{
                                var data = {  y: unlikeDateValue[i] , label: unlikeDate[i]};
                              }
                                unlikeFinalData.push(data);
                                i++;
                              }
                          }

                          var chart = new CanvasJS.Chart("chartContainer",{
                            zoomEnabled: true,
                            animationEnabled: true,
                            exportFileName: "Feedback Report",
                            exportEnabled: true,
                            title:{ text: title_report_name },
                             axisY:{
                                interval: 5,
                              },
                              dataPointMaxWidth: 80,
                            data: [
                                    {
                                      type: "stackedColumn",
                                      indexLabel: "{y}",
                                      color: "red",
                                      dataPoints: unlikeFinalData
                                    }, 
                                     
                                    {
                                      type: "stackedColumn",
                                      indexLabel: "{y}",
                                      color: "#8dc642",
                                      dataPoints: likeFinalData
                                    }, 
                                    {
                                      type: "line",  //Adding a hidden series to display the total
                                      color: "transparent",
                                      indexLabel: "{y}",
                                      toolTipContent: null,
                                      dataPoints: []
                                    }
                                  ]
                          });

                          //Show total number like and unlike 
                          for(var i = 0; i < chart.options.data[0].dataPoints.length; i++) {
                            
                            var likeTotal = 0;
                            var unlikeTotal = 0;
                            if (chart.options.data[0].dataPoints[i] == undefined) {
                                unlikeTotal = 0;
                            }else{
                                unlikeTotal = chart.options.data[0].dataPoints[i].y;
                            }

                            if (chart.options.data[1].dataPoints[i] == undefined) {
                                likeTotal = 0;
                            }else{
                                likeTotal = chart.options.data[1].dataPoints[i].y;
                            }

                            chart.options.data[2].dataPoints.push({
                              label: chart.options.data[0].dataPoints[i].label, 
                              y: likeTotal + unlikeTotal
                            });
                          }
                        //  
                          chart.render();

                          }
                          
                            $('#user_id').multiselect({
                              nonSelectedText: 'Select Client',
                              enableFiltering: true,
                              includeSelectAllOption: true,
                              enableCaseInsensitiveFiltering: true
                            });

                           $('#store_id').multiselect({
                            nonSelectedText: 'Select Store',
                            enableFiltering: true,
                            includeSelectAllOption: true,
                            enableCaseInsensitiveFiltering: true
                           });

                            // Get Report list from report table by ajax 
                            $.ajaxSetup({
                              headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                              }
                            });
                            //get stores list by client id
                            $('#user_id').change(function(){ 
                              var userId = $(this).val();
                              if(userId){
                                $.ajax({
                                  url:'/reports',
                                  type:'GET',
                                  data: {
                                        "client_id": userId
                                       },
                                  success:function(data){
                                  var store_list = data.success;
                                  var store_option = "";
                                  $(store_list).each(function(index,item){
                                    var store_name = item.name;
                                    var store_id = item.id;

                                    store_option += "<option value ='"+store_id+"'>"+store_name+"</option>";
                                  });

                                  $('#store_id').html(store_option).multiselect("destroy").multiselect({
                                      nonSelectedText: 'Select Store',
                                      enableFiltering: true,
                                      includeSelectAllOption: true,
                                      enableCaseInsensitiveFiltering: true
                                    });

                                  },
                                 error : function(res){
                                  console.log(res);
                                 }
                                });
                              }
                            });
                                //Get Saved Report list
                                $.ajax({
                                  url: '/reports/report_list', 
                                  type: "GET",
                                  success: function(data){
                                    var report_data = data.success;
                                  var route = window.location.origin;
                                  $(report_data).each(function (index, item) {
                                    var report_name = item.report_name;
                                    var id = item.id;

                                    var div_data="<li class='listItem'><a href ="+route+"/report/"+id+">"+report_name+"</a></li>";
                                    $(div_data).appendTo('#user_report');
                                  }); 
                                  paginationReinitialize(); //report list pagination 
                                 
                                  }
                                });
                          });
                       