$(document).ready(function(){
    $('#user_id').multiselect({
        nonSelectedText: 'Select Client',
        enableFiltering: true,
         nableCaseInsensitiveFiltering: true
    });

    $('#store_id').multiselect({
     	nonSelectedText: 'Select Store',
     	enableFiltering: true,
     	enableCaseInsensitiveFiltering: true
    });

    // Get Report list from report table by ajax 
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
         }
    });

    //get stores list by client id
    $('#user_id').change(function(){ 
        var userId = $(this).val();
         if(userId){
            $.ajax({
	            url:'/reports',
	            type:'GET',
	            data: {
	                "client_id": userId
	            },
	            success:function(data){
	                var store_list = data.success;
	                var store_option = "";
	                $(store_list).each(function(index,item){
	                   var store_name = item.name;
	                   var store_id = item.id;
	                    store_option += "<option value ='"+store_id+"'>"+store_name+"</option>";
	                });

	                $('#store_id').html(store_option).multiselect("destroy").multiselect({
	                    nonSelectedText: 'Select Store',
	                    enableFiltering: true,
	                    enableCaseInsensitiveFiltering: true
	                });
	            },
	            error : function(res){
	                console.log(res);
	            }
            });
        }
    });

    //Get Saved Report list
    $.ajax({
        url: '/reports/report_list', 
        type: "GET",
        success: function(data){
            var report_data = data.success;
            var route = window.location.origin;
            $(report_data).each(function (index, item) {
                var report_name = item.report_name;
                var id = item.id;
                var div_data="<li class='listItem'><a href ="+route+"/report/"+id+">"+report_name+"</a></li>";
                $(div_data).appendTo('#user_report');
            }); 
            paginationReinitialize(); //report list pagination 
        }
    });
});