 
<?php $__env->startSection('content'); ?>
<script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>

<div class="row">
     <!-- column -->
    <div class="col-12"> 
        <div class="card">
            <div class="card-body"> 
                <!-- row -->  
                <div class="row">
                    <div class="col-2">       
                        <form method="POST" action="<?php echo e(route('client_report.index')); ?>" name="filter_form">
                            <?php echo e(csrf_field()); ?>

                            <div class="create-report">
                              <h4 class="card-title">CREATE REPORT</h4>
                              <?php if(isset($users)): ?>
                               <?php if(Auth::user()->roles[0]->slug == 'admin'): ?>
                              <select  name="users[]" id="user_id" multiple class="form-control" >
                                
                                  <?php $__currentLoopData = $users; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $user): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                    <?php if(Auth::user()->id != $user->id): ?>
                                      <option value="<?php echo e($user->id); ?>"  
                                       <?php if(isset(saved_users)): ?>
 <?php $__currentLoopData = $saved_users; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $saved_user): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
    <?php echo e($saved_user == $user->id ? 'selected' : ''); ?>

   <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
  <?php elseif(isset($selectedUserPriority)): ?> 
    <?php $__currentLoopData = $selectedUserPriority; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $selectUser): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
       <?php echo e($selectUser == $user->id ? 'selected' : ''); ?>

     <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
  <?php endif; ?>
> 
                                        <?php echo e(ucfirst(trans($user->first_name))); ?></option> 
                                    <?php endif; ?>
                                  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>  
                                <?php endif; ?>
                                <?php endif; ?>
                              </select>

                              <select  name="stores[]" id="store_id" multiple class="form-control" >
                                  <?php $__currentLoopData = $stores; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $store): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                      <option value="<?php echo e($store->id); ?>"
                                        <?php if(isset($selectedstorePriority)): ?> 
                                          <?php $__currentLoopData = $selectedstorePriority; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $selectstore): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                            <?php echo e($selectstore == $store->id ? 'selected' : ''); ?>

                                          <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                                        <?php endif; ?>
                                      ><?php echo e(ucfirst(trans($store->name))); ?></option>
                                  <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                              </select>
                            </div>
                            <div class="date-picker pt-15">
                              <h5>Date Range</h5>
                              <div class="date-range">
                                <span class="date-from">
                                  <input type="date" name="from_date" id="from_date" 
                                  value="<?php if(isset($saved_from_date)): ?><?php echo e($saved_from_date); ?><?php else: ?><?php echo e(request()->input('from_date')); ?><?php endif; ?>">
                                  <i class="fa fa-calendar"></i> 
                                  <span></span>
                                </span> 
                                
                                <span class="date-to">
                                  <input type="date" name="to_date" id="to_date" value="<?php if(isset($saved_to_date)): ?><?php echo e($saved_to_date); ?><?php else: ?><?php echo e(request()->input('to_date')); ?><?php endif; ?>">
                                  <i class="fa fa-calendar"></i>
                                </span>
                              </div>
                            </div>
                            <div class="time-picker-section pt-15">
                              <h5>Time Range</h5>
                              <div class="time-picker">
                                <span class="time-from">
                                  <input type="time" name="from_time" id="from_time" value="<?php if(isset($saved_from_time)): ?><?php echo e($saved_from_time); ?><?php else: ?><?php echo e(request()->input('from_time')); ?><?php endif; ?>">
                                  <i class='far fa-clock'></i>
                                </span>
                                <span class="time-to">
                                  <input type="time" name="to_time" id="to_time" value="<?php if(isset($saved_to_time)): ?><?php echo e($saved_to_time); ?><?php else: ?><?php echo e(request()->input('to_time')); ?><?php endif; ?>">
                                  <i class='far fa-clock'></i>
                                </span>
                              </div>
                            </div>
                            <!-- <div class="day-week pt-15">
                                <label>Day of Week</label>
                            </div> -->
                            <div class="report-filter-btn">
                              <button class="" type="submit" name="fiter_report_btn">  <?php echo e(__('Filter Report')); ?></button>
                            </div>
                        </form>
                        <div class="create-report">
                          <h4 class="card-title">SAVED REPORT</h4>
                             
                            <ul class="paginationList" id="user_report"></ul>
                            <div id="pagination-container">
                              <p class='paginacaoCursor' id="beforePagination"><</p>
                              <p class='paginacaoCursor' id="afterPagination">></p>
                            </div>  
                        </div>
                        <div class="return_page">
                          <p>Go To:</p>
                          <a href="<?php echo e(route ('client.home')); ?>">Home</a>
                        </div>
                    </div>

                    <div class="col-10">
                        <div>
                          <!-- Update report button -->
                          <?php if(isset($update_report_btn)): ?>
                          <button type="button" class="btn btn-info btn-lg">Update Report</button>
                        <?php endif; ?>
                          <!-- save report button -->
                          <?php if(isset($json_fiter_param)): ?>
                          <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#reportModal" id="save_report">Save This Report</button>
                          <div id="reportModal" class="modal fade" role="dialog">
                            <div class="modal-dialog">

                              <!-- Modal content-->
                              <div class="modal-content">
                                <div class="modal-header">
                                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                                  <h4 class="modal-title"></h4>
                                </div>
                                <div class="modal-body">
                                   <form method="POST" action="<?php echo e(route('client_report.save')); ?>">
                                  <!--  <form> -->
                                    <?php echo csrf_field(); ?>
                                      <div class="report-name pt-15">
                                        <div>
                                          <?php if(isset($json_fiter_param)): ?>
                                          <input type="hidden" name="filter_param[]" value="<?php echo e($json_fiter_param); ?>">
                                          <?php endif; ?>
                                        </div>
                                            <label>Report Name</label>
                                            <input type="text" name="report_name" id="report_name">
                                        </div>
                                        <div class="report-filter-btn">
                                            <button type="submit">  <?php echo e(__('Save')); ?></button>
                                        </div>
                                    </form>
                                </div>
                                <div class="modal-footer">
                                  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                </div>
                              </div>
                            </div>
                          </div>
                          <?php endif; ?>
                            <!-- Chart Report -->
                            <script src="https://canvasjs.com/assets/script/canvasjs.min.js"></script>
                            <div class = "remove_text" id="chartContainer" style="height: 360px; width: 90%;"></div>
                            <script type="text/javascript">
                          var graph_data = <?php echo $report_data->toJson(); ?>;

                          var likeDate = new Array();
                          var likeDateValue = new Array();
                          var unlikeDate = new Array();
                          var unlikeDateValue = new Array();
                          var likeFinalData = new Array();
                          var unlikeFinalData = new Array();
                          for (var i = graph_data.length - 1; i >= 0; i--) {

                            var mydate=graph_data[i]['feedback_date'];
                            //date format change
                            var fbDate = new Date(mydate);
                            var dd = fbDate.getDate(); 
                            var mm = fbDate.getMonth() + 1; 
                            var yyyy = fbDate.getFullYear(); 
                            if (dd < 10) { 
                                dd = '0' + dd; 
                            } 
                            if (mm < 10) { 
                                mm = '0' + mm; 
                            } 
                            var fbDate = dd + '-' + mm + '-' + yyyy;

                            var fbValue = graph_data[i]['name'];
                           // var fbValue = graph_data[i]['name'].count;
                            if(fbValue == 1){
                                var found = $.inArray(fbDate, likeDate);
                                if(found == -1){
                                    likeDate.push(fbDate);
                                    likeDateValue.push(1);
                                }else{
                                    likeDateValue[found] += 1;
                                }
                            }else{
                                var found = $.inArray(fbDate, unlikeDate);
                                if(found == -1){
                                    unlikeDate.push(fbDate);
                                    unlikeDateValue.push(1);
                                }else{
                                    unlikeDateValue[found] += 1;
                                }
                            }
                          }

                          var length = likeDate.length;
                          var i = 0;
                          if(length> 0){
                              while(i < length){
                                var data = {  y: likeDateValue[i] , label: likeDate[i]};
                                likeFinalData.push(data);
                                i++;
                              }
                          }

                          var length = unlikeDate.length;
                          var i = 0;
                          if(length> 0){
                              while(i < length){
                                var data = {  y: unlikeDateValue[i] , label: unlikeDate[i]};
                                unlikeFinalData.push(data);
                                i++;
                              }
                          }

                          var chart = new CanvasJS.Chart("chartContainer",{
                            data: [
                                    {
                                      type: "stackedColumn",
                                      indexLabel: "{y}",
                                      color: "red",
                                      dataPoints: unlikeFinalData
                                    }, 
                                     
                                    {
                                      type: "stackedColumn",
                                      indexLabel: "{y}",
                                      color: "#8dc642",
                                      dataPoints: likeFinalData
                                    }, 
                                    {
                                      type: "line",  //Adding a hidden series to display the total
                                      color: "transparent",
                                      indexLabel: "{y}",
                                      toolTipContent: null,
                                      dataPoints: []
                                    }
                                  ]
                          });

                          //Show total number like and unlike 
                          for(var i = 0; i < chart.options.data[0].dataPoints.length; i++) {
                            
                            var likeTotal = 0;
                            var unlikeTotal = 0;
                            if (chart.options.data[0].dataPoints[i] == undefined) {
                                unlikeTotal = 0;
                            }else{
                                unlikeTotal = chart.options.data[0].dataPoints[i].y;
                            }

                            if (chart.options.data[1].dataPoints[i] == undefined) {
                                likeTotal = 0;
                            }else{
                                likeTotal = chart.options.data[1].dataPoints[i].y;
                            }

                            chart.options.data[2].dataPoints.push({
                              label: chart.options.data[0].dataPoints[i].label, 
                              y: likeTotal + unlikeTotal
                            });
                          }
                        //  
                          chart.render();
                        </script>
                        <!-- Multiselect dropdown list -->
                        <script type="text/javascript">
                          $(document).ready(function(){
                            $('#user_id').multiselect({
                              nonSelectedText: 'Select Client',
                              enableFiltering: true,
                              enableCaseInsensitiveFiltering: true
                            });

                           $('#store_id').multiselect({
                            nonSelectedText: 'Select Store',
                            enableFiltering: true,
                            enableCaseInsensitiveFiltering: true
                           });

                         // Get Report list from report table by ajax 
                         $.ajaxSetup({
                                  headers: {
                                      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                                  }
                              });
                         //get stores list by client id
                        $('#user_id').change(function(){
                            var userId = $(this).val(); 
                            //console.log(userId);
                            if(userId){
                              $.ajax({
                                url:'/reports',
                                type:'GET',
                                 data: {
                                        "client_id": userId
                                       },
                                success:function(data){
                                  var store_list = data.success;
                                  var store_option = "";
                                  $(store_list).each(function(index,item){
                                    var store_name = item.name;
                                    var store_id = item.id;

                                    store_option += "<option value ='"+store_id+"'>"+store_name+"</option>";
                                  });

                                  $('#store_id').html(store_option).multiselect("destroy").multiselect({
                                      nonSelectedText: 'Select Store',
                                      enableFiltering: true,
                                      enableCaseInsensitiveFiltering: true
                                    });

                                },
                                 error : function(res){
                                  console.log(res);
                                 }

                              });
                            }
                        });
                                //Get Saved Report list
                                $.ajax({
                                  url: '/reports/report_list', 
                                  type: "GET",
                                  success: function(data){
                                    var report_data = data.success;
                                  // console.log(report_data[0].report_name);
                                  var route = window.location.origin;
                                  $(report_data).each(function (index, item) {
                                    var report_name = item.report_name;
                                    var id = item.id;

                                    var div_data="<li class='listItem'><a href ="+route+"/report/"+id+">"+report_name+"</a></li>";
                                   // console.log(div_data);
                                    $(div_data).appendTo('#user_report');
                                  }); 
                                  paginationReinitialize(); //report list pagination 
                                 
                                  }
                                });

                         });
                        </script>
                        </div>
                    </div>
                </div>

              </div>  
            </div>
        </div>
    </div>
</div>
<?php $__env->stopSection(); ?> 



<?php echo $__env->make('templates.main',['pageTitle'=>'Clients','rootPage'=>''], \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH /var/www/html/dashboard.focustrax.com/resources/views/pages/client/feedbacks/list.blade.php ENDPATH**/ ?>